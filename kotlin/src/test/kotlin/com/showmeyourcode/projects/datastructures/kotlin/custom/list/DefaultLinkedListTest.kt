package com.showmeyourcode.projects.datastructures.kotlin.custom.list

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.forAll
import io.kotest.data.headers
import io.kotest.data.row
import io.kotest.data.table
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldContainInOrder
import io.kotest.matchers.shouldBe

class DefaultLinkedListTest : AnnotationSpec() {

    @Test
    fun `should get the first element`() {
        forAll(
            table(
                headers("list", "expected element"),
                row(listOf(1), 1),
                row(listOf(1, 2), 1),
                row(listOf(1, 2, 3), 1),
                row(listOf(1, 2, 3, 4, 5), 1)
            )
        ) { list, expectedElement ->
            convertToLinkedList(list).getFirst() shouldBe expectedElement
        }
    }

    @Test
    fun `should throw an exception when trying to get the first element`() {
        shouldThrow<NoSuchElementException> {
            convertToLinkedList(emptyList()).getFirst()
        }
    }

    @Test
    fun `should get the last element`() {
        forAll(
            table(
                headers("list", "expected element"),
                row(listOf(1), 1),
                row(listOf(1, 2), 2),
                row(listOf(1, 2, 3), 3),
                row(listOf(1, 2, 3, 4, 5), 5)
            )
        ) { list, expectedElement ->
            convertToLinkedList(list).getLast() shouldBe expectedElement
        }
    }

    @Test
    fun `should throw an exception when trying to get the last element`() {
        shouldThrow<NoSuchElementException> {
            convertToLinkedList(emptyList()).getLast()
        }
    }

    @Test
    fun `should remove the first element`() {
        val list = DefaultLinkedList<String>()

        list.add("1")
        list.add("2")
        list.add("3")
        list.add("4")
        list.add("5")
        list.toList() shouldContainInOrder listOf("1", "2", "3", "4", "5")
        list.size() shouldBe 5
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "5"

        list.removeFirst()
        list.toList() shouldContainInOrder listOf("2", "3", "4", "5")
        list.size() shouldBe 4
        list.getFirst() shouldBe "2"
        list.getLast() shouldBe "5"

        list.removeFirst()
        list.toList() shouldContainInOrder listOf("3", "4", "5")
        list.size() shouldBe 3
        list.getFirst() shouldBe "3"
        list.getLast() shouldBe "5"

        list.removeFirst()
        list.toList() shouldContainInOrder listOf("4", "5")
        list.size() shouldBe 2
        list.getFirst() shouldBe "4"
        list.getLast() shouldBe "5"

        list.removeFirst()
        list.toList() shouldContainInOrder listOf("5")
        list.size() shouldBe 1
        list.getFirst() shouldBe "5"
        list.getLast() shouldBe "5"

        list.removeFirst()
        list.toList() shouldBe emptyList()
        list.size() shouldBe 0

        shouldThrow<NoSuchElementException> {
            list.removeFirst()
        }
    }

    @Test
    fun `should remove the last element`() {
        val list = DefaultLinkedList<String>()

        list.add("1")
        list.add("2")
        list.add("3")
        list.add("4")
        list.add("5")
        list.toList() shouldContainExactly listOf("1", "2", "3", "4", "5")
        list.size() shouldBe 5
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "5"

        list.removeLast()
        list.toList() shouldContainExactly listOf("1", "2", "3", "4")
        list.size() shouldBe 4
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "4"

        list.removeLast()
        list.toList() shouldContainExactly listOf("1", "2", "3")
        list.size() shouldBe 3
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "3"

        list.removeLast()
        list.toList() shouldContainExactly listOf("1", "2")
        list.size() shouldBe 2
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "2"

        list.removeLast()
        list.toList() shouldContainExactly listOf("1")
        list.size() shouldBe 1
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "1"

        list.removeLast()
        list.toList() shouldBe emptyList()
        list.size() shouldBe 0

        shouldThrow<NoSuchElementException> {
            list.removeLast()
        }
    }

    @Test
    fun `should add first`() {
        val list = DefaultLinkedList<Int>()

        list.addFirst(1)
        list.toList() shouldContainExactly listOf(1)
        list.size() shouldBe 1
        list.getFirst() shouldBe 1
        list.getLast() shouldBe 1

        list.addFirst(2)
        list.toList() shouldContainExactly listOf(2, 1)
        list.size() shouldBe 2
        list.getFirst() shouldBe 2
        list.getLast() shouldBe 1

        list.addFirst(3)
        list.toList() shouldContainExactly listOf(3, 2, 1)
        list.size() shouldBe 3
        list.getFirst() shouldBe 3
        list.getLast() shouldBe 1

        list.addFirst(4)
        list.toList() shouldContainExactly listOf(4, 3, 2, 1)
        list.size() shouldBe 4
        list.getFirst() shouldBe 4
        list.getLast() shouldBe 1
    }

    @Test
    fun `should add last`() {
        val list = DefaultLinkedList<Int>()

        list.addLast(1)
        list.toList() shouldContainExactly listOf(1)
        list.size() shouldBe 1
        list.getFirst() shouldBe 1
        list.getLast() shouldBe 1

        list.addLast(2)
        list.toList() shouldContainExactly listOf(1, 2)
        list.size() shouldBe 2
        list.getFirst() shouldBe 1
        list.getLast() shouldBe 2

        list.addLast(3)
        list.toList() shouldContainExactly listOf(1, 2, 3)
        list.size() shouldBe 3
        list.getFirst() shouldBe 1
        list.getLast() shouldBe 3

        list.addLast(4)
        list.toList() shouldContainExactly listOf(1, 2, 3, 4)
        list.size() shouldBe 4
        list.getFirst() shouldBe 1
        list.getLast() shouldBe 4
    }

    @Test
    fun `should contain a particular element`() {
        val list = DefaultLinkedList<Int>()

        list.add(1)
        list.add(2)

        list.contains(1) shouldBe true
        list.contains(2) shouldBe true
    }

    @Test
    fun `should not contain a particular element`() {
        val list = DefaultLinkedList<Int>()

        list.add(1)
        list.add(2)

        list.contains(12) shouldBe false
        list.contains(9) shouldBe false
        list.contains(0) shouldBe false
    }

    @Test
    fun `should add an element`() {
        val list = DefaultLinkedList<Int>()

        list.add(1)
        list.toList() shouldContainExactly listOf(1)
        list.size() shouldBe 1
        list.getFirst() shouldBe 1
        list.getLast() shouldBe 1

        list.add(2)
        list.toList() shouldContainExactly listOf(1, 2)
        list.size() shouldBe 2
        list.getFirst() shouldBe 1
        list.getLast() shouldBe 2

        list.add(3)
        list.toList() shouldContainExactly listOf(1, 2, 3)
        list.size() shouldBe 3
        list.getFirst() shouldBe 1
        list.getLast() shouldBe 3

        list.add(4)
        list.toList() shouldContainExactly listOf(1, 2, 3, 4)
        list.size() shouldBe 4
        list.getFirst() shouldBe 1
        list.getLast() shouldBe 4
    }

    @Test
    fun `should remove an element if exists`() {
        val list = DefaultLinkedList<String>()

        list.add("1")
        list.add("2")
        list.add("3")
        list.add("4")
        list.add("5")
        list.toList() shouldContainExactly listOf("1", "2", "3", "4", "5")
        list.size() shouldBe 5
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "5"

        list.remove("5") shouldBe true
        list.toList() shouldContainExactly listOf("1", "2", "3", "4")
        list.size() shouldBe 4
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "4"

        list.remove("1") shouldBe true
        list.toList() shouldContainExactly listOf("2", "3", "4")
        list.size() shouldBe 3
        list.getFirst() shouldBe "2"
        list.getLast() shouldBe "4"

        list.remove("3") shouldBe true
        list.toList() shouldContainExactly listOf("2", "4")
        list.size() shouldBe 2
        list.getFirst() shouldBe "2"
        list.getLast() shouldBe "4"

        list.remove("2") shouldBe true
        list.toList() shouldContainExactly listOf("4")
        list.size() shouldBe 1
        list.getFirst() shouldBe "4"
        list.getLast() shouldBe "4"

        list.remove("4") shouldBe true
        list.toList() shouldBe emptyList()
        list.size() shouldBe 0
    }

    @Test
    fun `should remove an element if not exists`() {
        val list = DefaultLinkedList<String>()

        list.add("1")
        list.add("2")
        list.add("3")
        list.add("4")
        list.add("5")
        list.toList() shouldContainExactly listOf("1", "2", "3", "4", "5")
        list.size() shouldBe 5
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "5"

        list.remove("12345678") shouldBe false
        list.remove("11") shouldBe false
    }

    @Test
    fun `should add a collection`() {
        val list = DefaultLinkedList<String>()

        list.add("1")
        list.add("2")
        list.add("3")
        list.toList() shouldContainExactly listOf("1", "2", "3")
        list.size() shouldBe 3
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "3"

        list.addAll(listOf("4", "5")) shouldBe true
        list.toList() shouldContainExactly listOf("1", "2", "3", "4", "5")
        list.size() shouldBe 5
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "5"

        list.addAll(listOf("6", "7")) shouldBe true
        list.toList() shouldContainExactly listOf("1", "2", "3", "4", "5", "6", "7")
        list.size() shouldBe 7
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "7"

        list.addAll(emptyList()) shouldBe true
        list.toList() shouldContainExactly listOf("1", "2", "3", "4", "5", "6", "7")
        list.size() shouldBe 7
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "7"
    }

    @Test
    fun `should add a collection at index`() {
        val list = DefaultLinkedList<String>()

        list.add("1")
        list.add("2")
        list.add("3")
        list.toList() shouldContainExactly listOf("1", "2", "3")
        list.size() shouldBe 3
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "3"

        list.addAll(1, listOf("4", "5")) shouldBe true
        list.toList() shouldContainExactly listOf("1", "4", "5", "2", "3")
        list.size() shouldBe 5
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "3"

        list.addAll(0, listOf("0", "0")) shouldBe true
        list.toList() shouldContainExactly listOf("0", "0", "1", "4", "5", "2", "3")
        list.size() shouldBe 7
        list.getFirst() shouldBe "0"
        list.getLast() shouldBe "3"

        list.addAll(6, listOf("9", "9")) shouldBe true
        list.toList() shouldContainExactly listOf("0", "0", "1", "4", "5", "2", "3", "9", "9")
        list.size() shouldBe 9
        list.getFirst() shouldBe "0"
        list.getLast() shouldBe "9"

        list.addAll(2, emptyList()) shouldBe true
        list.toList() shouldContainExactly listOf("0", "0", "1", "4", "5", "2", "3", "9", "9")
        list.size() shouldBe 9
        list.getFirst() shouldBe "0"
        list.getLast() shouldBe "9"

        list.addAll(0, listOf("11")) shouldBe true
        list.toList() shouldContainExactly listOf("11", "0", "0", "1", "4", "5", "2", "3", "9", "9")
        list.size() shouldBe 10
        list.getFirst() shouldBe "11"
        list.getLast() shouldBe "9"

        shouldThrow<java.lang.IndexOutOfBoundsException> {
            list.addAll(16, listOf("11"))
        }
    }

    @Test
    fun `should clear a list`() {
        val list = DefaultLinkedList<String>()

        list.add("1")
        list.add("2")
        list.add("3")
        list.add("4")
        list.add("5")
        list.toList() shouldContainExactly listOf("1", "2", "3", "4", "5")
        list.size() shouldBe 5

        list.clear()
        list.toList() shouldContainExactly emptyList()
        list.size() shouldBe 0
        shouldThrow<NoSuchElementException> {
            list.getLast()
        }
        shouldThrow<NoSuchElementException> {
            list.getFirst()
        }
    }

    @Test
    fun `should get an element at index`() {
        val list = DefaultLinkedList<String>()

        list.add("1")
        list.add("2")
        list.add("3")
        list.toList() shouldContainExactly listOf("1", "2", "3")
        list.size() shouldBe 3
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "3"

        list[0] shouldBe "1"
        list[1] shouldBe "2"
        list[2] shouldBe "3"

        shouldThrow<IndexOutOfBoundsException> {
            list[12]
        }
    }

    @Test
    fun `should set a new value at index`() {
        val list = DefaultLinkedList<String>()

        list.add("1")
        list.toList() shouldContainExactly listOf("1")
        list.size() shouldBe 1
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "1"
        list.add("2")
        list.toList() shouldContainExactly listOf("1", "2")
        list.size() shouldBe 2
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "2"
        list.add("3")
        list.toList() shouldContainExactly listOf("1", "2", "3")
        list.size() shouldBe 3
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "3"

        list[0] = "6"
        list.toList() shouldContainExactly listOf("6", "2", "3")
        list.size() shouldBe 3
        list.getFirst() shouldBe "6"
        list.getLast() shouldBe "3"
        list[1] = "5"
        list.toList() shouldContainExactly listOf("6", "5", "3")
        list.size() shouldBe 3
        list.getFirst() shouldBe "6"
        list.getLast() shouldBe "3"
        list[2] = "4"
        list.toList() shouldContainExactly listOf("6", "5", "4")
        list.size() shouldBe 3
        list.getFirst() shouldBe "6"
        list.getLast() shouldBe "4"

        shouldThrow<IndexOutOfBoundsException> {
            list[12] = "12"
            Unit
        }
    }

    @Test
    fun `should add an element at index`() {
        val list = DefaultLinkedList<String>()

        list.add(0, "1")
        list.add(1, "2")
        list.add(2, "3")
        list.toList() shouldContainExactly listOf("1", "2", "3")
        list.size() shouldBe 3
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "3"

        list.add(1, "12")
        list.toList() shouldContainExactly listOf("1", "12", "2", "3")
        list.size() shouldBe 4
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "3"

        list.add(0, "0")
        list.toList() shouldContainExactly listOf("0", "1", "12", "2", "3")
        list.size() shouldBe 5
        list.getFirst() shouldBe "0"
        list.getLast() shouldBe "3"

        list.add(4, "4")
        list.toList() shouldContainExactly listOf("0", "1", "12", "2", "4", "3")
        list.size() shouldBe 6
        list.getFirst() shouldBe "0"
        list.getLast() shouldBe "3"

        list.add(6, "77")
        list.toList() shouldContainExactly listOf("0", "1", "12", "2", "4", "3", "77")
        list.size() shouldBe 7
        list.getFirst() shouldBe "0"
        list.getLast() shouldBe "77"

        shouldThrow<java.lang.IndexOutOfBoundsException> {
            list.add(33, "4")
        }
    }

    @Test
    fun `should remove at index`() {
        val list = DefaultLinkedList<String>()

        list.add("1")
        list.add("2")
        list.add("3")
        list.add("4")
        list.toList() shouldContainExactly listOf("1", "2", "3", "4")
        list.size() shouldBe 4
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "4"

        list.remove(2)
        list.toList() shouldContainExactly listOf("1", "2", "4")
        list.size() shouldBe 3
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "4"
        list.remove(2)
        list.toList() shouldContainExactly listOf("1", "2")
        list.size() shouldBe 2
        list.getFirst() shouldBe "1"
        list.getLast() shouldBe "2"
        list.remove(0)
        list.toList() shouldContainExactly listOf("2")
        list.size() shouldBe 1
        list.getFirst() shouldBe "2"
        list.getLast() shouldBe "2"
        list.remove(0)
        list.toList() shouldContainExactly emptyList()
        list.size() shouldBe 0
        shouldThrow<NoSuchElementException> {
            list.getFirst()
        }
        shouldThrow<NoSuchElementException> {
            list.getLast()
        }

        shouldThrow<IndexOutOfBoundsException> {
            list.remove(10)
        }
    }

    @Test
    fun `should reverse the list`() {
        forAll(
            table(
                headers("list", "expected reversed list"),
                row(listOf(), listOf()),
                row(listOf(1), listOf(1)),
                row(listOf(1, 2), listOf(2, 1)),
                row(listOf(1, 2, 3), listOf(3, 2, 1)),
                row(listOf(1, 2, 3, 4, 5), listOf(5, 4, 3, 2, 1))
            )
        ) { list, expectedReversedList ->
            val linkedList = convertToLinkedList(list)

            linkedList.toList() shouldContainExactly list

            linkedList.reverse()
            linkedList.toList() shouldContainExactly expectedReversedList
        }
    }

    @Test
    fun `should convert it to a list`() {
        forAll(
            table(
                headers("list"),
                row(listOf()),
                row(listOf(1)),
                row(listOf(1, 2)),
                row(listOf(1, 2, 3)),
                row(listOf(1, 2, 3, 4, 5))
            )
        ) { list ->
            convertToLinkedList(list).toList() shouldContainExactly list
        }
    }

    @Test
    fun `should override toString method`() {
        forAll(
            table(
                headers("list", "expected toString output"),
                row(listOf(), "[]"),
                row(listOf(1), "[1]"),
                row(listOf(1, 2), "[1, 2]"),
                row(listOf(1, 2, 3, 4, 5), "[1, 2, 3, 4, 5]")
            )
        ) { list, toString ->
            convertToLinkedList(list).toString() shouldBe toString
        }
    }

    private fun convertToLinkedList(list: List<Int>): DefaultLinkedList<Int> {
        val linkedList = DefaultLinkedList<Int>()
        list.forEach { linkedList.add(it) }
        return linkedList
    }
}
