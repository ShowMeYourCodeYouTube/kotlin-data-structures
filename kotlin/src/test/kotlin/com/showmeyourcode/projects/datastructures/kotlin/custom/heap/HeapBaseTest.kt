package com.showmeyourcode.projects.datastructures.kotlin.custom.heap

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.forAll
import io.kotest.data.headers
import io.kotest.data.row
import io.kotest.data.table
import io.kotest.matchers.shouldBe

abstract class HeapBaseTest : AnnotationSpec() {

    abstract fun constructMaxHeap(): Heap

    abstract fun constructMinHeap(): Heap

    @Test
    fun `should get the max element`() {
        forAll(
            table(
                headers("elements to insert", "expected max", "expected heap"),
                row(listOf(2, 12, 3, 1, 123, 54, 444, 555, 1, 223), 555, listOf(555, 444, 123, 12, 223, 3, 54, 1, 1, 2)),
                row(listOf(1, 2, 3, 4, 5, 6), 6, listOf(6, 4, 5, 1, 3, 2)),
                row(listOf(1, 11, 2, 44, 98, 6, 4, 33678, 90, 11, 46, 123), 33678, listOf(33678, 98, 123, 90, 46, 6, 4, 1, 44, 11, 11, 2)),
                row(listOf(7, 4, 8, 7, 6, 9, 3, 234, 231, 5, 3, 2, 23), 234, listOf(234, 231, 23, 9, 6, 8, 3, 4, 7, 5, 3, 2, 7))
            )
        ) { list, expectedMax, expectedHeapAsArray ->
            val heap = constructMaxHeap()
            list.forEach {
                heap += it
            }

            heap.peek() shouldBe expectedMax
            heap.size() shouldBe list.size
            heap.toList() shouldBe expectedHeapAsArray
        }
    }

    @Test
    fun `should get the min element`() {
        forAll(
            table(
                headers("elements to insert", "expected max", "expected heap"),
                row(listOf(2, 12, 3, 1, 123, 54, 444, 555, 1, 223), 1, listOf(1, 1, 3, 2, 123, 54, 444, 555, 12, 223)),
                row(listOf(1, 2, 3, 4, 5, 6), 1, listOf(1, 2, 3, 4, 5, 6)),
                row(listOf(3, 11, 4, 44, 98, 6, 4, 33678, 90, 1, 46, 123), 1, listOf(1, 3, 4, 44, 11, 6, 4, 33678, 90, 98, 46, 123)),
                row(listOf(7, 4, 8, 7, 6, 9, 3, 234, 231, 5, 3, 2, 23), 2, listOf(2, 3, 3, 7, 5, 4, 8, 234, 231, 7, 6, 9, 23))
            )
        ) { list, expectedMin, expectedHeapAsArray ->
            val heap = constructMinHeap()
            list.forEach {
                heap += it
            }

            heap.peek() shouldBe expectedMin
            heap.size() shouldBe list.size
            heap.toList() shouldBe expectedHeapAsArray
        }
    }

    @Test
    fun `should remove the top min element`() {
        forAll(
            table(
                headers("elements to insert", "expected element"),
                row(listOf(2, 12, 3, 1, 123, 54, 444, 555, 1, 223), listOf(1, 1, 2, 3, 12)),
                row(listOf(1, 2, 3, 4, 5, 6), listOf(1, 2, 3)),
                row(listOf(3, 11, 4, 44, 98, 6, 4, 33678, 90, 1, 46, 123), listOf(1, 3, 4))
            )
        ) { list, elementsToRemove ->
            val heap = constructMinHeap()
            list.forEach {
                heap += it
            }

            var currentSize = heap.size()
            elementsToRemove.forEach {
                heap.pop() shouldBe it
                heap.size() shouldBe --currentSize
            }
        }
    }

    @Test
    fun `should remove the top max element`() {
        forAll(
            table(
                headers("elements to insert", "expected element"),
                row(listOf(2, 12, 3, 1, 123, 54, 444, 555, 1, 223), listOf(555, 444, 223, 123, 54)),
                row(listOf(1, 2, 3, 4, 5, 6), listOf(6, 5, 4)),
                row(listOf(3, 11, 4, 44, 98, 6, 4, 33678, 90, 1, 46, 123), listOf(33678, 123, 98))
            )
        ) { list, elementsToRemove ->
            val heap = constructMaxHeap()
            list.forEach {
                heap += it
            }

            var currentSize = heap.size()
            elementsToRemove.forEach {
                heap.pop() shouldBe it
                heap.size() shouldBe --currentSize
            }
        }
    }

    @Test
    fun `should not allow to pop or peek operations on an empty heap`() {
        val emptyHeap = constructMaxHeap()
        shouldThrow<NoSuchElementException> {
            emptyHeap.peek()
        }
        shouldThrow<NoSuchElementException> {
            emptyHeap.pop()
        }
    }
}
