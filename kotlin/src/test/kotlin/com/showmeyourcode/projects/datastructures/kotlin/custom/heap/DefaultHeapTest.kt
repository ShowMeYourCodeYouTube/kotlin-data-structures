package com.showmeyourcode.projects.datastructures.kotlin.custom.heap

class DefaultHeapTest : HeapBaseTest() {
    override fun constructMaxHeap(): Heap = DefaultHeap.maxHeap()

    override fun constructMinHeap(): Heap = DefaultHeap.minHeap()
}
