package com.showmeyourcode.projects.datastructures.kotlin.exercise.arrays

import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.forAll
import io.kotest.data.headers
import io.kotest.data.row
import io.kotest.data.table
import io.kotest.matchers.shouldBe

class ArraysContentAnalysisExerciseTest : AnnotationSpec() {
    @Test
    fun `countWords should correctly count specific words`() {
        forAll(
            table(
                headers("input sentences", "word to count", "expected output"),
                row(
                    arrayOf("Kotlin is a modern programming language."),
                    arrayOf("kotlin"),
                    mapOf("kotlin" to 1)
                ),
                row(
                    arrayOf(
                        "The language is concise and powerful.",
                        "Many developers enjoy using Kotlin."
                    ),
                    arrayOf("kotlin", "language"),
                    mapOf("kotlin" to 1, "language" to 1)
                ),
                row(
                    arrayOf(
                        "Kotlin and Java are both used for Android development."
                    ),
                    arrayOf("kotlin", "java", "android"),
                    mapOf("kotlin" to 1, "java" to 1, "android" to 1)
                )
            )
        ) { sentences, wordsToCount, expectedCounts ->
            val actualCounts = countWords(sentences, wordsToCount)
            actualCounts shouldBe expectedCounts
        }
    }

    @Test
    fun `countWords should handle empty sentences array`() {
        val sentences = emptyArray<String>()
        val wordsToCount = arrayOf("kotlin", "language", "and")

        val wordCounts = countWords(
            sentences,
            wordsToCount
        )

        wordCounts.size shouldBe 3
        wordCounts.forEach { (key, value) ->
            wordsToCount.contains(key) shouldBe true
            value shouldBe 0
        }
    }

    @Test
    fun `countWords should handle empty wordsToCount array`() {
        val sentences = arrayOf(
            "Kotlin is a modern programming language.",
            "The language is concise and powerful."
        )

        val wordCounts = countWords(
            sentences,
            emptyArray()
        )

        wordCounts.isEmpty() shouldBe true
    }

    @Test
    fun `countWords should handle sentences with special characters and casing`() {
        val sentences = arrayOf(
            "Kotlin is a modern programming language, @Kotlin@ is powerful.",
            "Kotlin and Java are used for Android development!!!!!"
        )

        val wordCounts = countWords(
            sentences,
            arrayOf("kotlin", "language")
        )

        wordCounts["kotlin"] shouldBe 3
        wordCounts["language"] shouldBe 1
    }
}
