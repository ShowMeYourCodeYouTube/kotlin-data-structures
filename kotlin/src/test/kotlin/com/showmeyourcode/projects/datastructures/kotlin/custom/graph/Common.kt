package com.showmeyourcode.projects.datastructures.kotlin.custom.graph

import kotlin.random.Random

fun createGraph(adjacencyList: List<List<Int>>, representation: GraphRepresentation): Graph {
    representation.addEdges(adjacencyList)
    return Graph(representation)
}

fun createGraphWithAdjacencyList(adjacencyList: List<MutableList<Int>>, type: GraphType): Graph {
    val representation = AdjacencyList(adjacencyList.size, type)
    return createGraph(adjacencyList, representation)
}

fun createGraphWithAdjacencyMatrix(adjacencyList: List<List<Int>>, type: GraphType): Graph {
    val representation = AdjacencyMatrix(adjacencyList.size, type)
    return createGraph(adjacencyList, representation)
}

fun createWeightedGraph(adjacencyList: List<List<Pair<Int, Double>>>, representation: WeightedGraphRepresentation): WeightedGraph {
    representation.addWeightedEdges(adjacencyList)
    return WeightedGraph(representation)
}

fun createWeightedGraphWithAdjacencyList(adjacencyList: List<List<Pair<Int, Double>>>, type: GraphType): WeightedGraph {
    val representation = WeightedAdjacencyList(adjacencyList.size, type)
    return createWeightedGraph(adjacencyList, representation)
}

fun createWeightedGraphWithAdjacencyMatrix(adjacencyList: List<List<Pair<Int, Double>>>, type: GraphType): WeightedGraph {
    val representation = WeightedAdjacencyMatrix(adjacencyList.size, type)
    return createWeightedGraph(adjacencyList, representation)
}

val addRandomWeights = { l: List<List<Int>> -> l.map { neighbours -> neighbours.map { Pair(it, Random.nextDouble()) } } }
