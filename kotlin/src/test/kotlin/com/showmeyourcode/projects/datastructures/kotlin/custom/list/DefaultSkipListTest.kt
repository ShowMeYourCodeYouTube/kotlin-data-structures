package com.showmeyourcode.projects.datastructures.kotlin.custom.list

import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.forAll
import io.kotest.data.headers
import io.kotest.data.row
import io.kotest.data.table
import io.kotest.matchers.shouldBe

class DefaultSkipListTest : AnnotationSpec() {

    @Test
    fun `should insert elements`() {
        forAll(
            table(
                headers("elements to insert", "expected list"),
                row(listOf(1), listOf(1)),
                row(listOf(1, 2, 3, 4, 5, 6), listOf(1, 2, 3, 4, 5, 6)),
                row(listOf(1, 11, 2, 44, 98, 6, 4, 33678, 90, 11, 46, 123), listOf(1, 2, 4, 6, 11, 11, 44, 46, 90, 98, 123, 33678)),
                row(listOf(7, 4, 8, 7, 6, 9, 3, 234, 231, 5, 3, 2, 23), listOf(2, 3, 3, 4, 5, 6, 7, 7, 8, 9, 23, 231, 234))
            )
        ) { list, expectedList ->
            val skipList = convertToSkipList(list)
            skipList.getElementsAsList() shouldBe expectedList
            skipList.size() shouldBe list.size
        }
    }

    @Test
    fun `should contain elements`() {
        forAll(
            table(
                headers("elements"),
                row(listOf(1)),
                row(listOf(1, 2, 3, 4, 5, 6)),
                row(listOf(1, 11, 2, 44, 98, 6, 4, 33678, 90, 11, 46, 123)),
                row(listOf(7, 4, 8, 7, 6, 9, 3, 234, 231, 5, 3, 2, 23))
            )
        ) { elements ->
            val skipList = convertToSkipList(elements)
            elements.forEach {
                skipList.contains(it) shouldBe true
            }
        }
    }

    @Test
    fun `should not contain elements`() {
        forAll(
            table(
                headers("elements", "element not present"),
                row(listOf(1), listOf(2, 6, 5, 43, 11, 0)),
                row(listOf(1, 2, 3, 4, 5, 6), listOf(43, 11, 0)),
                row(listOf(1, 11, 2, 44, 98, 6, 4, 33678, 90, 11, 46, 123), listOf(43, 0)),
                row(listOf(7, 4, 8, 7, 6, 9, 3, 234, 231, 5, 3, 2, 23), listOf(43, 0, 3456789, 987, 42))
            )
        ) { elements, elementNotPresent ->
            val skipList = convertToSkipList(elements)
            elementNotPresent.forEach {
                skipList.contains(it) shouldBe false
            }
        }
    }

    @Test
    fun `should delete elements`() {
        forAll(
            table(
                headers("elements", "element to delete", "expected elements"),
                row(listOf(1), listOf(1), emptyList()),
                row(listOf(1), listOf(11), listOf(1)),
                row(listOf(1, 2, 3, 4, 5, 6), listOf(3, 6, 1, 2), listOf(4, 5)),
                row(listOf(1, 11, 2, 44, 98, 6, 4, 33678, 90, 11, 46, 123), listOf(98, 6, 999, 888), listOf(1, 2, 4, 11, 11, 44, 46, 90, 123, 33678))
            )
        ) { elements, elementToDelete, expectedList ->
            val skipList = convertToSkipList(elements)
            elementToDelete.forEach {
                skipList.delete(it)
            }
            skipList.getElementsAsList() shouldBe expectedList
        }
    }

    private fun convertToSkipList(list: List<Int>): DefaultSkipList {
        val skipList = DefaultSkipList(5)
        list.forEach { skipList.insert(it) }
        return skipList
    }
}
