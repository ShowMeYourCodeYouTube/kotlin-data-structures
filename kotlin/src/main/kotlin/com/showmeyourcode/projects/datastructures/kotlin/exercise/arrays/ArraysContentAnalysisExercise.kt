package com.showmeyourcode.projects.datastructures.kotlin.exercise.arrays

/**
 * A Kotlin function that performs text analysis on an array of sentences and count specific words.
 *
 */
fun countWords(sentences: Array<String>, wordsToCount: Array<String>): Map<String, Int> {
    val counts = mutableMapOf(*wordsToCount.map { it to 0 }.toTypedArray())

    for (s in sentences) {
        s.replace("[^a-zA-Z\\s]".toRegex(), "")
            .split("\\s+".toRegex())
            .forEach { word ->
                val lowerCaseWord = word.lowercase()
                if (counts.contains(lowerCaseWord)) {
                    counts[lowerCaseWord] = counts.getOrDefault(lowerCaseWord, 0).inc()
                }
            }
    }

    return counts
}
