package com.showmeyourcode.projects.datastructures.kotlin.standard.concurrency

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withLock

object ConcurrencyOverview {

    fun main() {
        LOGGER.info(
            """
            Concurrency is the execution of multiple instruction sequences at the same time.
            
            In computer programming, a mutual exclusion (mutex) is a program object 
            that prevents multiple threads from accessing the same shared resource simultaneously.
            It's a property of concurrency control, which is instituted for the purpose of preventing race conditions.
            A race condition is a semantic error. It is a flaw that occurs in the timing or the ordering of events 
            that leads to erroneous program behavior and occurs when a device 
            or system attempts to perform two or more operations at the same time
            
            Concurrent collection refers to a set of classes that allow multiple threads to access and modify a collection concurrently, 
            without the need for explicit synchronization.
            
            Kotlin's standard library does not include specific concurrent collections.
            However, Kotlin has interoperability with Java, and you can use Java's concurrent collections seamlessly in Kotlin.
            """.trimIndent()
        )
        concurrent()
    }

    private fun concurrent() {
        LOGGER.info(
            """
             The kotlinx.coroutines.sync.Mutex class provides a mutual exclusion lock, allowing only one thread to access a critical section of code at a time. 
             This is useful for protecting shared resources from concurrent access.
             Mutex is a specific kind of binary semaphore that is used to provide a locking mechanism.
            """.trimIndent()
        )

        val mutex = Mutex()

        runBlocking {
            launch {
                mutex.withLock {
                    LOGGER.info("Accessing a critical section with Mutex...\n")
                }
            }
        }

        LOGGER.info(
            """
             The Semaphore class is used to control access to a shared resource by multiple threads (does the same as a mutex but allows x number of threads to enter). 
             It maintains a set of permits that determine how many threads can access the resource simultaneously.
            """.trimIndent()
        )

        val semaphore = Semaphore(permits = 2)

        runBlocking {
            launch {
                semaphore.acquire()
                LOGGER.info("Accessing a critical section with Semaphore...\n")
                semaphore.release()
            }
        }

        LOGGER.info(
            """
             The Channel class in Kotlin provides a way for coroutines to communicate with each other in a concurrent setting. 
             It's often used for producer-consumer scenarios.
            """.trimIndent()
        )

        val channel = Channel<Int>()

        runBlocking {
            launch {
                repeat(3) {
                    channel.send(it)
                    LOGGER.info("Channel | Sending $it")
                }
                channel.close()
            }

            launch {
                for (value in channel) {
                    LOGGER.info("Channel | Receiving $value")
                }
            }
        }

        LOGGER.info("\n===\n")
    }
}
