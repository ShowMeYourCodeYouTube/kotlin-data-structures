package com.showmeyourcode.projects.datastructures.kotlin.custom

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import com.showmeyourcode.projects.datastructures.kotlin.custom.list.DefaultSkipList

object SkipListOverview {

    fun main() {
        LOGGER.info(
            """
                The skip list is a probabilistic data structure that is built upon the general idea of a linked list.
                The skip list uses probability to build subsequent layers of linked lists upon an original linked list.
                Each additional layer of links contains fewer elements, but no new elements.
                It allows O(log n) average complexity for search as well as O(log n)average complexity for insertion within
                an ordered  sequence of n elements.
            """.trimIndent()
        )
        val skipList = DefaultSkipList(4)

        skipList.insert(5)
        skipList.insert(2)
        skipList.insert(8)
        skipList.insert(3)
        skipList.insert(10)

        LOGGER.info("Skip List after insertions:")
        skipList.print()

        skipList.delete(8)

        LOGGER.info("Skip List after deletion of 8:")
        skipList.print()

        LOGGER.info("Search for 5: " + skipList.contains(5))
        LOGGER.info("Search for 8: " + skipList.contains(8))
    }
}
