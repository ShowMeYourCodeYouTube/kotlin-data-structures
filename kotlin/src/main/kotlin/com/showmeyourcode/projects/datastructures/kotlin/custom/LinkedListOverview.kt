package com.showmeyourcode.projects.datastructures.kotlin.custom

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging
import com.showmeyourcode.projects.datastructures.kotlin.custom.list.DefaultLinkedList

object LinkedListOverview {
    fun main() {
        Logging.LOGGER.info(
            """
                A linked list stores a collection of items in a linear order.
                Each element, or node, in a linked list contains a data item, as well as a reference, or link, to the next item in the list.
            """.trimIndent()
        )

        val linkedList = DefaultLinkedList<Int>()

        linkedList.add(1)
        linkedList.add(2)
        linkedList.add(3)
        Logging.LOGGER.info("\nLinkedList: ${linkedList}\n")

        linkedList.addFirst(0)
        Logging.LOGGER.info("\nLinkedList: ${linkedList}\n")
    }
}
