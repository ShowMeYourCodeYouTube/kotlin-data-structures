package com.showmeyourcode.projects.datastructures.kotlin.custom.tree

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import java.util.*

enum class Color {
    RED,
    BLACK
}

class RedBlackTreeNode<T : Comparable<T>>(
    var value: T,
    var color: Color = Color.RED,
    var left: RedBlackTreeNode<T>? = null,
    var right: RedBlackTreeNode<T>? = null,
    var parent: RedBlackTreeNode<T>? = null
)

class RedBlackTree<T : Comparable<T>> {
    var root: RedBlackTreeNode<T>? = null

    fun insert(value: T) {
        insertAndFixup(value)
    }

    fun inOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        inOrderTraversalRecursive(root, result)
        return result
    }

    private fun inOrderTraversalRecursive(node: RedBlackTreeNode<T>?, result: MutableList<T>) {
        if (node != null) {
            inOrderTraversalRecursive(node.left, result)
            result.add(node.value)
            inOrderTraversalRecursive(node.right, result)
        }
    }

    fun preOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        preOrderTraversalRecursive(root, result)
        return result
    }

    private fun preOrderTraversalRecursive(node: RedBlackTreeNode<T>?, result: MutableList<T>) {
        if (node != null) {
            result.add(node.value)
            preOrderTraversalRecursive(node.left, result)
            preOrderTraversalRecursive(node.right, result)
        }
    }

    fun postOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        postOrderTraversalRecursive(root, result)
        return result
    }

    private fun postOrderTraversalRecursive(node: RedBlackTreeNode<T>?, result: MutableList<T>) {
        if (node != null) {
            postOrderTraversalRecursive(node.left, result)
            postOrderTraversalRecursive(node.right, result)
            result.add(node.value)
        }
    }

    fun levelOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        val queue: Queue<RedBlackTreeNode<T>> = LinkedList()

        root?.let { queue.add(it) }

        while (queue.isNotEmpty()) {
            val current = queue.poll()
            result.add(current.value)

            current.left?.let { queue.add(it) }
            current.right?.let { queue.add(it) }
        }

        return result
    }

    private fun leftRotate(x: RedBlackTreeNode<T>) {
        val y = x.right ?: return
        x.right = y.left
        y.left?.parent = x
        y.parent = x.parent
        if (x.parent == null) {
            root = y
        } else if (x == x.parent?.left) {
            x.parent?.left = y
        } else {
            x.parent?.right = y
        }
        y.left = x
        x.parent = y
    }

    private fun rightRotate(y: RedBlackTreeNode<T>) {
        val x = y.left ?: return
        y.left = x.right
        x.right?.parent = y
        x.parent = y.parent
        if (y.parent == null) {
            root = x
        } else if (y == y.parent?.left) {
            y.parent?.left = x
        } else {
            y.parent?.right = x
        }
        x.right = y
        y.parent = x
    }

    private fun insertFixup(z: RedBlackTreeNode<T>) {
        var y: RedBlackTreeNode<T>?
        var x = z
        while (x.parent?.color == Color.RED) {
            if (x.parent == x.parent?.parent?.left) {
                y = x.parent?.parent?.right
                if (y?.color == Color.RED) {
                    x.parent?.color = Color.BLACK
                    y.color = Color.BLACK
                    x.parent?.parent?.color = Color.RED
                    x = x.parent?.parent ?: return
                } else {
                    if (x == x.parent?.right) {
                        x = x.parent ?: return
                        leftRotate(x)
                    }
                    x.parent?.color = Color.BLACK
                    x.parent?.parent?.color = Color.RED
                    rightRotate(x.parent?.parent ?: return)
                }
            } else {
                y = x.parent?.parent?.left
                if (y?.color == Color.RED) {
                    x.parent?.color = Color.BLACK
                    y.color = Color.BLACK
                    x.parent?.parent?.color = Color.RED
                    x = x.parent?.parent ?: return
                } else {
                    if (x == x.parent?.left) {
                        x = x.parent ?: return
                        rightRotate(x)
                    }
                    x.parent?.color = Color.BLACK
                    x.parent?.parent?.color = Color.RED
                    leftRotate(x.parent?.parent ?: return)
                }
            }
        }
        root?.color = Color.BLACK
    }

    private fun insertAndFixup(value: T) {
        val z = RedBlackTreeNode(value)
        var y: RedBlackTreeNode<T>? = null
        var x = root

        while (x != null) {
            y = x
            if (z.value < x.value) {
                x = x.left
            } else {
                x = x.right
            }
        }

        z.parent = y

        if (y == null) {
            root = z
        } else if (z.value < y.value) {
            y.left = z
        } else {
            y.right = z
        }

        insertFixup(z)
    }

    fun delete(value: T) {
        throw NotImplementedError()
    }
}

object RedBlackTreeOverview {

    fun main() {
        val redBlackTree = RedBlackTree<Int>()

        // Example: Insert nodes into the Red-Black tree
        redBlackTree.insert(10)
        redBlackTree.insert(5)
        redBlackTree.insert(15)
        redBlackTree.insert(3)
        redBlackTree.insert(7)

        // Example: Perform in-order traversal
        val inOrderResult = redBlackTree.inOrderTraversal()
        LOGGER.info("In-order traversal: $inOrderResult")

        // Example: Perform pre-order traversal
        val preOrderResult = redBlackTree.preOrderTraversal()
        LOGGER.info("Pre-order traversal: $preOrderResult")

        // Example: Perform post-order traversal
        val postOrderResult = redBlackTree.postOrderTraversal()
        LOGGER.info("Post-order traversal: $postOrderResult")

        // Example: Perform level-order traversal
        val levelOrderResult = redBlackTree.levelOrderTraversal()
        LOGGER.info("Level-order traversal: $levelOrderResult")
    }
}
