package com.showmeyourcode.projects.datastructures.kotlin.custom.list

class DefaultLinkedList<E> : LinkedList<E> {
    private var first: Node<E>? = null
    private var last: Node<E>? = null
    private var size: Int = 0

    private inner class Node<E>(val value: E, var next: Node<E>?)

    override fun getFirst(): E = first?.value ?: throw NoSuchElementException()

    override fun getLast(): E = last?.value ?: first?.value ?: throw NoSuchElementException()

    override fun removeFirst(): E = when {
        first == null -> throw NoSuchElementException()
        last == null -> {
            val returnValue = first!!.value
            first = null
            size--
            returnValue
        }
        else -> if (first!!.next == last) {
            val returnValue = first!!.value
            first = last
            last = null
            size--
            returnValue
        } else {
            val returnValue = first!!.value
            first = first!!.next
            size--
            returnValue
        }
    }

    override fun removeLast(): E = when {
        first == null -> throw NoSuchElementException()
        last == null -> {
            val returnValue = first!!.value
            first = null
            size--
            returnValue
        }
        else -> if (first!!.next == last) {
            val returnValue = last!!.value
            last = null
            first?.next = null
            size--
            returnValue
        } else {
            val returnValue = last!!.value
            var newLastNode = first
            while (newLastNode?.next != last) {
                newLastNode = newLastNode?.next
            }
            last = newLastNode
            newLastNode?.next = null
            size--
            returnValue
        }
    }

    override fun addFirst(e: E) {
        first = Node(e, first)

        if (last == null) {
            last = first?.next
        }
        size++
    }

    override fun addLast(e: E) {
        if (first == null) {
            first = Node(e, last)
        } else if (last == null) {
            last = Node(e, null)
            first?.next = last
        } else {
            val previousLast = last
            last = Node(e, null)
            previousLast?.next = last
        }
        size++
    }

    override fun contains(o: E): Boolean {
        var isFound = false
        var tmpNode = first
        while (tmpNode != null && !isFound) {
            isFound = tmpNode.value == o
            tmpNode = tmpNode.next
        }
        return isFound
    }

    override fun size(): Int = size

    override fun add(e: E): Boolean {
        addLast(e)
        return true
    }

    override fun remove(o: Any): Boolean {
        var isDeleted = false
        var previousNode: Node<E>? = null
        var currentNode = first
        while (currentNode != null && !isDeleted) {
            if (currentNode.value == o) {
                isDeleted = true
                size--
                // if the next element is the last one, update the last node reference
                if (currentNode.next == null) {
                    previousNode?.next = null
                    when (size) {
                        0 -> {
                            last = null
                            first = null
                        }
                        1 -> {
                            last = null
                        }
                        else -> {
                            last = previousNode
                        }
                    }
                } else if (currentNode == first) {
                    first = currentNode.next
                } else {
                    previousNode?.next = currentNode.next
                }
            }
            previousNode = currentNode
            currentNode = currentNode.next
        }
        return isDeleted
    }

    override fun addAll(c: Collection<E>): Boolean {
        c.forEach { addLast(it) }
        return true
    }

    override fun addAll(index: Int, c: Collection<E>): Boolean {
        if (!isPositionIndex(index)) {
            throw IndexOutOfBoundsException()
        } else if (c.isNotEmpty()) {
            val iterator = c.iterator()

            val firstNew = Node(iterator.next(), null)
            var nextNew: Node<E>? = firstNew

            while (iterator.hasNext()) {
                nextNew?.next = Node(iterator.next(), null)
                nextNew = nextNew?.next
            }

            when (index) {
                0 -> {
                    nextNew?.next = first
                    first = firstNew
                }
                size - 1 -> {
                    last?.next = firstNew
                    last = nextNew
                }
                else -> {
                    var nodeAtIndexBefore: Node<E>? = null
                    for (i in 0..index - 1) {
                        nodeAtIndexBefore = if (nodeAtIndexBefore == null) {
                            first
                        } else {
                            nodeAtIndexBefore.next
                        }
                    }
                    val nodeAtCurrentIndex = nodeAtIndexBefore?.next
                    nodeAtIndexBefore?.next = firstNew
                    nextNew?.next = nodeAtCurrentIndex
                }
            }
            size += c.size
        }

        return true
    }

    override fun clear() {
        last = null
        first = null
        size = 0
    }

    override fun get(index: Int): E = if (!isElementIndex(index)) {
        throw IndexOutOfBoundsException()
    } else {
        var currentNode = first
        var currentIdx = 0
        while (currentIdx != index) {
            currentNode = currentNode?.next
            currentIdx++
        }
        currentNode!!.value
    }

    override fun set(index: Int, element: E): E = if (!isElementIndex(index)) {
        throw IndexOutOfBoundsException()
    } else {
        var previousNode: Node<E>? = null
        var currentNode = first
        var currentIdx = 0
        while (currentIdx != index) {
            previousNode = currentNode
            currentNode = currentNode?.next
            currentIdx++
        }

        val previousValue = currentNode!!.value
        val newNode = Node(element, currentNode.next)
        previousNode?.next = newNode

        if (index == 0) {
            first = newNode
        } else if (index == size - 1) {
            last = newNode
        }
        previousValue
    }

    override fun add(index: Int, element: E) = if (!isPositionIndex(index)) {
        throw IndexOutOfBoundsException()
    } else {
        if (index == 0) {
            val newNode = Node(element, first)
            first = newNode
        } else if (index == size) {
            val newNode = Node(element, null)
            if (last == null) {
                first?.next = newNode
                last = newNode
            } else {
                last?.next = newNode
                last = newNode
            }
        } else {
            var previousNode: Node<E>? = null
            var currentNode = first
            var currentIdx = 0
            while (currentIdx != index) {
                previousNode = currentNode
                currentNode = currentNode?.next
                currentIdx++
            }

            val newNode = Node(element, currentNode)
            previousNode?.next = newNode
        }

        size++
        Unit
    }

    override fun remove(index: Int): E = if (!isElementIndex(index)) {
        throw IndexOutOfBoundsException()
    } else {
        var previousNode: Node<E>? = null
        var currentNode = first
        var currentIdx = 0
        while (currentIdx != index) {
            previousNode = currentNode
            currentNode = currentNode?.next
            currentIdx++
        }

        previousNode?.next = currentNode?.next
        size--

        when {
            size == 0 -> {
                first = null
            }
            size == 1 -> {
                first = currentNode?.next
                last = null
            }
            // -1 to match the last index +1 to restore the deleted one and compare with the initial index
            index == size - 1 + 1 -> {
                last = previousNode
            }
            index == 0 -> {
                first = currentNode?.next
            }
        }

        currentNode!!.value
    }

    override fun reverse() {
        var previousNode: Node<E>? = null
        var current = first
        while (current != null) {
            val next = current.next
            current.next = previousNode
            previousNode = current
            current = next
        }

        // If the list contains 2+ elements,
        // change the first & last elements as they are already reversed.
        if (last != null) {
            val tmp = first
            first = last
            last = tmp
        }
    }

    override fun toList(): List<E> {
        val result = mutableListOf<E>()
        var node = first
        while (node?.value != null) {
            result.add(node.value)
            node = node.next
        }
        return result
    }

    override fun toString(): String {
        return toList().joinToString(prefix = "[", postfix = "]")
    }

    /**
     * Tells if the argument is the index of an existing element.
     */
    private fun isElementIndex(index: Int): Boolean {
        return index in 0 until size
    }

    /**
     * Tells if the argument is the index of a valid position for an
     * iterator or an add operation.
     */
    private fun isPositionIndex(index: Int): Boolean {
        return index in 0..size
    }
}
