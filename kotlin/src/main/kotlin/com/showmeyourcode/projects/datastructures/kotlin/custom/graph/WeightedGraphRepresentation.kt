package com.showmeyourcode.projects.datastructures.kotlin.custom.graph

interface WeightedGraphRepresentation : GraphRepresentation {
    fun addEdge(vertex1: Int, vertex2: Int, weight: Double)
    fun getNeighborsWithWeights(vertex: Int): List<Pair<Int, Double>>

    fun addWeightedEdges(adjacencyList: List<List<Pair<Int, Double>>>) {
        adjacencyList.forEachIndexed { index, neighbors ->
            neighbors.forEach { (neighbour, weight) ->
                addEdge(index, neighbour, weight)
            }
        }
    }
}

class WeightedAdjacencyList(private val vertices: Int, private val graphType: GraphType) : WeightedGraphRepresentation {
    private val adjacencyList: MutableList<MutableList<Pair<Int, Double>>> = MutableList(vertices) { mutableListOf() }

    override fun addEdge(vertex1: Int, vertex2: Int) {
        throw UnsupportedOperationException("Cannot add an unweighted edge to the weighted graph.")
    }

    override fun getNeighborsWithWeights(vertex: Int): List<Pair<Int, Double>> {
        return adjacencyList[vertex]
    }

    override fun addEdge(vertex1: Int, vertex2: Int, weight: Double) {
        adjacencyList[vertex1].add(Pair(vertex2, weight))
        //  This ensures the edge is bidirectional, meaning there is also an edge from vertex2 to vertex1.
        if (graphType == GraphType.UNDIRECTED) {
            adjacencyList[vertex2].add(Pair(vertex1, weight))
        }
    }

    override fun getNeighbors(vertex: Int): List<Int> {
        return adjacencyList[vertex].map { it.first }
    }

    override fun toList(): List<List<Int>> {
        return adjacencyList.map { it.map { pair -> pair.first }.toList() }
    }

    override fun vertices(): Int {
        return vertices
    }

    override fun graphType(): GraphType {
        return graphType
    }
}

class WeightedAdjacencyMatrix(private val vertices: Int, private val graphType: GraphType) : WeightedGraphRepresentation {
    private val adjacencyMatrix: Array<Array<Double>> = Array(vertices) { Array(vertices) { Double.POSITIVE_INFINITY } }

    override fun addEdge(vertex1: Int, vertex2: Int) {
        throw UnsupportedOperationException("Cannot add an unweighted edge to the weighted graph.")
    }

    override fun getNeighborsWithWeights(vertex: Int): List<Pair<Int, Double>> {
        return adjacencyMatrix[vertex]
            .mapIndexedNotNull { index, weight ->
                if (weight < Double.POSITIVE_INFINITY) {
                    Pair(index, weight)
                } else {
                    null
                }
            }
    }

    override fun addEdge(vertex1: Int, vertex2: Int, weight: Double) {
        adjacencyMatrix[vertex1][vertex2] = weight
        //  This ensures the edge is bidirectional, meaning there is also an edge from vertex2 to vertex1.
        if (graphType == GraphType.UNDIRECTED) {
            adjacencyMatrix[vertex2][vertex1] = weight
        }
    }

    override fun getNeighbors(vertex: Int): List<Int> {
        return adjacencyMatrix[vertex].indices.filter { adjacencyMatrix[vertex][it] < Double.POSITIVE_INFINITY }
    }

    override fun toList(): List<List<Int>> {
        return adjacencyMatrix.map { row ->
            row.indices.filter { row[it] < Double.POSITIVE_INFINITY }.toList()
        }
    }

    override fun vertices(): Int {
        return vertices
    }

    override fun graphType(): GraphType {
        return graphType
    }
}
