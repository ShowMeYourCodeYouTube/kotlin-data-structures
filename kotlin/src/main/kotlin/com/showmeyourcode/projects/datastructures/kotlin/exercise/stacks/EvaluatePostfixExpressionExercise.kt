package com.showmeyourcode.projects.datastructures.kotlin.exercise.stacks

import kotlin.collections.ArrayDeque

fun evaluatePostfixExpression(expression: String): Int {
    val stack = ArrayDeque<Int>()

    for (token in expression.split(" ")) {
        if (token.isNumber()) {
            stack.addFirst(token.toInt())
        } else if (token.isOperator()) {
            val operand2 = stack.removeFirst()
            val operand1 = stack.removeFirst()
            val result = when (token) {
                "+" -> operand1 + operand2
                "-" -> operand1 - operand2
                "*" -> operand1 * operand2
                "/" -> operand1 / operand2
                else -> throw IllegalArgumentException("Invalid operator: $token")
            }
            stack.addFirst(result)
        } else {
            throw IllegalArgumentException("Invalid token in expression: $token")
        }
    }

    require(stack.size == 1) {
        throw IllegalArgumentException("Invalid postfix expression format")
    }

    return stack.removeFirst()
}

fun String.isNumber(): Boolean {
    return this.toIntOrNull() != null
}

fun String.isOperator(): Boolean {
    return this.matches(Regex("[+\\-*/]"))
}
