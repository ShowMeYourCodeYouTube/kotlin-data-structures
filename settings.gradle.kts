rootProject.name = "Data structures in Kotlin and Java"

include(":common")
include(":coverage-report")
include(":java")
include(":kotlin")
