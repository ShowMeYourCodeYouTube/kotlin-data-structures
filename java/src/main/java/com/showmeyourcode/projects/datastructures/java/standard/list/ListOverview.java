package com.showmeyourcode.projects.datastructures.java.standard.list;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.showmeyourcode.projects.datastructures.kotlin.common.CommonUtilsKt.word;

public class ListOverview {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListOverview.class);

    private ListOverview() {
    }

    public static void main(String[] args) {
        LOGGER.info(
                """
                A List is a data structure that stores elements in an ordered and sequential manner. 
                A list can store repetitive elements which means a single element can occur more than once in a list.
                
                """
        );
        immutableList();
        mutableList();
        sorting();
        compareLists();

        concurrentList();
    }

    private static void concurrentList() {
        // A thread-safe variant of ArrayList in which all mutative operations (add, set, and so on)
        // are implemented by making a fresh copy of the underlying array.
        // This is ordinarily too costly, but may be more efficient than alternatives
        // when traversal operations vastly outnumber mutations,
        // and is useful when you cannot or don't want to synchronize traversals,
        // yet need to preclude interference among concurrent threads.
        var copyOnWriteArray = new CopyOnWriteArrayList<Integer>();
        // If you need exclusive write access, then you should synchronize.
        // You don't need to use Collections.synchronizedList(), just make sure that you are always synchronizing on the same object.
        // Ref: https://stackoverflow.com/questions/41620543/is-synchronized-block-always-needed-with-synchronized-collections
        var synchronizedList = java.util.Collections.synchronizedList(new ArrayList<Integer>());

        copyOnWriteArray.add(1);
        copyOnWriteArray.add(2);

        synchronizedList.add(1);
        synchronizedList.add(2);

        LOGGER.info("Concurrent list (CopyOnWriteArrayList): {}", copyOnWriteArray);
        LOGGER.info("Concurrent list (Collections.synchronizedList*): {}", synchronizedList);
        LOGGER.info("*If you need exclusive write access, then you should synchronize.");
    }

    private static void immutableList() {
        var immutableList = Collections.unmodifiableList(Arrays.asList(word));
        var immutableList2 = List.of(word);
        var immutableList3 = Collections.emptyList();
        var immutableList4 = Collections.singletonList(1);

        LOGGER.info("Immutable list (List.of): {}", immutableList);
        LOGGER.info("Immutable list (Collections.unmodifiableList): {}", immutableList2);
        LOGGER.info("Collections.emptyList: {}", immutableList3);
        LOGGER.info("Collections.singletonList: {}", immutableList4);
    }

    private static void mutableList() {
        var arrayList = new ArrayList<Integer>();
        var linkedList = new LinkedList<Integer>();
        // Get/Set methods for Vector are synchronized, whereas they are not for arrayList/linkedList.
        // ArrayList (Java 1.2 Collections) is replacement for Vector
        @SuppressWarnings("java:S1149")
        var vector = new Vector<Integer>();

        arrayList.add(1);
        linkedList.add(1);
        vector.add(1);

        LOGGER.info("ArrayList: {}", arrayList);
        LOGGER.info("LinkedList: {}", linkedList);
        LOGGER.info("Vector: {}", vector);
    }

    private static void sorting() {
        List<Integer> intList = Arrays.asList(1, 5, 3, 4, 2);
        Collections.sort(intList);

        List<Map.Entry<String, Integer>> entryList = new ArrayList() {{
            add(Map.entry("LAST", 1));
            add(Map.entry("3MIDDLE", 3));
            add(Map.entry("2MIDDLE", 2));
            add(Map.entry("FIRST", 4));
        }};

        LOGGER.info("Before list sorting: {}", entryList);

        Collections.sort(entryList, Map.Entry.comparingByValue());

        LOGGER.info("After list sorting: {}", entryList);
    }

    private record ExampleExample(String id) { }

    private record ExampleAggregate (String attr1, List<ExampleExample> examples) { }

    private static void compareLists() {
        var strings1 = List.of("String1", "String2", "String3");
        var strings2 = List.of("String1", "String2", "String3");
        var strings3 = List.of("String1", "String3", "String2");

        LOGGER.info("Compare list order sensitive and insensitive.");
        LOGGER.info("Comparing list of string (equal), the same order: {}", strings1.equals(strings2));
        LOGGER.info("Comparing list of string (equal), different order: {}", strings1.equals(strings3));

        LOGGER.info("Comparing list of string (containsAll), different order, order insensitive: {}", strings1.containsAll(strings3));

        LOGGER.info("Compare objects which contain List of objects.");

        var obj1 = new ExampleAggregate("Example1", List.of(new ExampleExample("ID1"), new ExampleExample("ID2"), new ExampleExample("ID3")));
        var obj2 = new ExampleAggregate("Example2", List.of(new ExampleExample("ID2"), new ExampleExample("ID1"), new ExampleExample("ID3")));
        var obj3 = new ExampleAggregate("Example1", List.of(new ExampleExample("ID1"), new ExampleExample("ID2"), new ExampleExample("ID3")));

        LOGGER.info("WITHOUT OVERWRITING EQUALS METHOD, IT'S ALWAYS FALSE!!!!!!!");
        LOGGER.info("Comparing objects with a field as a list (equal), the same order: {}", obj1.equals(obj2));
        LOGGER.info("Comparing objects with a field as a list (equal), different order: {}", obj1.equals(obj3));

        LOGGER.info("Compare list order sensitive and insensitive.");

        var list1 = List.of(
                new ExampleAggregate("Example2", List.of(new ExampleExample("ID1"), new ExampleExample("ID1"), new ExampleExample("ID1"))),
                new ExampleAggregate("Example1", List.of(new ExampleExample("ID1"), new ExampleExample("ID1"), new ExampleExample("ID1")))
        );

        var list2 = List.of(
                new ExampleAggregate("Example1", List.of(new ExampleExample("ID1"), new ExampleExample("ID1"), new ExampleExample("ID1"))),
                new ExampleAggregate("Example2", List.of(new ExampleExample("ID1"), new ExampleExample("ID1"), new ExampleExample("ID1")))
        );

        LOGGER.info("Comparing lists (equal), different order / order matters!: {}", list1.equals(list2));
        LOGGER.info("Comparing lists (equal), different order / order matters!: {}", Objects.equals(list1, list2));
        LOGGER.info("Comparing lists (containsAll), different order / order matters!: {}", (list1.containsAll(list2) && list2.containsAll(list1)));

        Comparator<ExampleAggregate> comparatorExample = (o1, o2) -> String.CASE_INSENSITIVE_ORDER.compare(o1.attr1, o2.attr1);
        var mutableList1 = new ArrayList<>(list1);
        var mutableList2 = new ArrayList<>(list2);

        mutableList1.sort(comparatorExample);
        mutableList2.sort(comparatorExample);
        LOGGER.info("Comparing lists (sort), different order / order matters!: {}", (mutableList1.equals(mutableList2)));

        // Read more: https://stackoverflow.com/questions/1075656/simple-way-to-find-if-two-different-lists-contain-exactly-the-same-elements
        // ContainsAll(), even if called both ways,
        // still wouldn't give the right results if frequency of elements is significant:
        // Arrays.asList(1,2,3,3).containsAll(Arrays.asList(1,2,2,3)) && Arrays.asList(1,2,2,3).containsAll(Arrays.asList(1,2,3,3)) gives true instead of false.
    }
}
