package com.showmeyourcode.projects.datastructures.java.standard.stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class StackOverview {

    private static final Logger LOGGER = LoggerFactory.getLogger(StackOverview.class);

    private StackOverview() {
    }

    public static void main(String[] args) {
        LOGGER.info(
                """
                A stack is a linear data structure that follows the principle of Last In First Out (LIFO).
                This means the last element inserted inside the stack is removed first.
                
                """
        );
        stack();
        immutableStack();

        concurrent();
    }

    private static void immutableStack() {
        var immutableList = Collections.unmodifiableCollection(new LinkedList<>(List.of(1, 3, 5, 2, 4)));

        LOGGER.info("Immutable deck (Collections.unmodifiableList): {}", immutableList);
    }

    private static void stack() {
        Deque<Integer> arrayDequeue = new ArrayDeque<>();
        Deque<Integer> linkedList = new LinkedList<>();

        arrayDequeue.add(3);
        arrayDequeue.add(2);
        arrayDequeue.add(1);

        linkedList.add(3);
        linkedList.add(2);
        linkedList.add(1);

        LOGGER.info("""
                ArrayDequeue: {}
                LinkedList: {}
                """,
                arrayDequeue,
                linkedList
        );

        arrayDequeue.removeLast();
        linkedList.removeLast();

        LOGGER.info("""
                === After removal ===
                ArrayDequeue: {}
                LinkedList: {}
                """,
                arrayDequeue,
                linkedList
        );
    }

    private static void concurrent() {
        var linkedBlockingDeque = new LinkedBlockingDeque<Integer>();
        var concurrentLinkedDeque = new ConcurrentLinkedDeque<Integer>();

        linkedBlockingDeque.add(5);
        linkedBlockingDeque.add(1);
        linkedBlockingDeque.add(3);

        concurrentLinkedDeque.add(5);
        concurrentLinkedDeque.add(1);
        concurrentLinkedDeque.add(3);

        LOGGER.info("Concurrent stack/deque (ConcurrentLinkedDeque): {}", concurrentLinkedDeque);
        LOGGER.info("Concurrent stack/deque (LinkedBlockingDeque): {}", linkedBlockingDeque);
    }
}
