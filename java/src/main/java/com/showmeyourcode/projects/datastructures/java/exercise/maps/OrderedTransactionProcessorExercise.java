package com.showmeyourcode.projects.datastructures.java.exercise.maps;

import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * You are given a list of transaction records. Each record is a string in the format "id:type:amount".
 * Your task is to write a method that processes these transactions and returns a map that maintains the insertion order of transactions.
 * The map should store the transaction ID as the key and the net balance (income - expense) after each transaction as the value.
 */
@Slf4j
class OrderedTransactionProcessorExercise {

    private OrderedTransactionProcessorExercise() {
    }

    public static Map<Integer, Integer> processOrderedTransactions(List<String> transactions) {
        if (transactions == null) {
            throw new IllegalArgumentException("Input list cannot be null");
        }

        Map<Integer, Integer> transactionMap = new LinkedHashMap<>();
        int netBalance = 0;

        for (String transaction : transactions) {
            String[] parts = transaction.split(":");
            if (parts.length != 3) continue;

            try {
                int id = Integer.parseInt(parts[0]);
                String type = parts[1];
                int amount = Integer.parseInt(parts[2]);

                switch (type) {
                    case "income":
                        netBalance += amount;
                        break;
                    case "expense":
                        netBalance -= amount;
                        break;
                    default:
                        log.warn("Unknown type of transaction - {}", type);
                }

                transactionMap.put(id, netBalance);
            } catch (NumberFormatException e) {
                log.error("Cannot processed the record: {} ", transaction, e);
            }
        }

        return transactionMap;
    }

    public static <K, V> void printElements(Map<K, V> elements) {
        for (Map.Entry<K, V> entry : elements.entrySet()) {
            log.info("{}: {}", entry.getKey(), entry.getValue());
        }
    }
}
