package com.showmeyourcode.projects.datastructures.java.custom.graph;

import java.util.*;

public class WeightedGraph extends BaseGraph {

    protected final WeightedGraphRepresentation representation;

    public WeightedGraph(WeightedGraphRepresentation representation) {
        super(representation);
        this.representation = representation;
    }

    void addEdge(int vertex1, int vertex2, double weight) {
        representation.addEdge(vertex1, vertex2, weight);
    }

    /**
     * A minimum spanning tree (MST) is defined as a spanning tree that has the minimum weight among all the possible spanning trees.
     * <p>
     * A spanning tree is a connected graph using all vertices in which there are no circuits.
     * In other words, there is a path from any vertex to any other vertex, but no circuits.
     */
    List<Map.Entry<Integer, Integer>> primMST() {
        var mst = new ArrayList<Map.Entry<Integer, Integer>>();
        var pq = new PriorityQueue<Map.Entry<Integer, Map.Entry<Integer, Double>>>(Comparator.comparing(o -> o.getValue().getValue())
        );
        var visited = new boolean[representation.vertices()];
        // by default new boolean[] is filled with false
        Arrays.fill(visited, false);

        // initialize the algorithm
        int startVertex = 0;
        for (Map.Entry<Integer, Double> neighbour : representation.getNeighborsWithWeights(startVertex)) {
            pq.offer(Map.entry(startVertex, neighbour));
        }
        visited[startVertex] = true;

        while (!pq.isEmpty()) {
            var currentVertex = pq.poll();
            var currentId = currentVertex.getKey();
            var currentConnection = currentVertex.getValue();
            var destinationVertexId = currentConnection.getKey();
            if (!visited[destinationVertexId]) {
                mst.add(Map.entry(currentId, destinationVertexId));
                visited[destinationVertexId] = true;
                for (Map.Entry<Integer, Double> neighbour : representation.getNeighborsWithWeights(destinationVertexId)) {
                    pq.offer(Map.entry(destinationVertexId, neighbour));
                }
            }
        }

        return mst;
    }

    static class UnionFind {
        int[] parent;
        int[] rank;

        UnionFind(int size) {
            parent = new int[size];
            rank = new int[size];
            // Initially, each vertex is its own parent (disjoint sets)
            for (int i = 0; i < size; i++) {
                parent[i] = i;
                rank[i] = 0;
            }
        }

        // Find with path compression
        public int find(int x) {
            if (parent[x] != x) {
                parent[x] = find(parent[x]);  // Path compression
            }
            return parent[x];
        }

        // Union by rank
        public void union(int x, int y) {
            int rootX = find(x);
            int rootY = find(y);

            if (rootX != rootY) {
                // Union by rank: attach the smaller tree under the larger tree
                if (rank[rootX] > rank[rootY]) {
                    parent[rootY] = rootX;
                } else if (rank[rootX] < rank[rootY]) {
                    parent[rootX] = rootY;
                } else {
                    parent[rootY] = rootX;
                    rank[rootX]++;
                }
            }
        }
    }

    /**
     * A minimum spanning tree (MST) is defined as a spanning tree that has the minimum weight among all the possible spanning trees.
     * <p>
     * A spanning tree is a connected graph using all vertices in which there are no circuits.
     * In other words, there is a path from any vertex to any other vertex, but no circuits.
     */
    List<Map.Entry<Integer, Integer>> kruskalMST() {
        List<Map.Entry<Integer, Integer>> mst = new ArrayList<>();

        // Collect all edges and sort them by weight
        var edges = new ArrayList<Map.Entry<Map.Entry<Integer, Integer>, Double>>();
        for (int i = 0; i < representation.vertices(); i++) {
            for (Map.Entry<Integer, Double> neighbour : representation.getNeighborsWithWeights(i)) {
                edges.add(Map.entry(Map.entry(i, neighbour.getKey()), neighbour.getValue()));
            }
        }
        edges.sort(Comparator.comparing(Map.Entry::getValue));

        UnionFind uf = new UnionFind(representation.vertices());

        for (Map.Entry<Map.Entry<Integer, Integer>, Double> edge : edges) {
            int u = edge.getKey().getKey();
            int v = edge.getKey().getValue();

            if (uf.find(u) != uf.find(v)) {
                // No cycle, add edge to MST
                mst.add(Map.entry(u, v));
                uf.union(u, v);  // Union the two sets
            }

            // If we've added V-1 edges, we can stop (MST is complete)
            if (mst.size() == representation.vertices() - 1) {
                break;
            }
        }

        return mst;
    }

    /**
     * Dijkstra's algorithm is an algorithm for finding the shortest paths between nodes in a weighted graph,
     * which may represent, for example, road networks.
     */
    List<Double> dijkstra(int startingVertex) {
        var shortest = new ArrayList<Double>();
        for (int i = 0; i < representation.vertices(); i++) {
            shortest.add(Double.MAX_VALUE);
        }

        var pq = new PriorityQueue<Map.Entry<Integer, Double>>(Map.Entry.comparingByValue());
        pq.offer(Map.entry(startingVertex, 0.0));

        while (!pq.isEmpty()) {
            var node = pq.poll();
            var vertex = node.getKey();
            var weight = node.getValue();

            if (shortest.get(vertex) < weight) {
                continue;
            }

            shortest.set(vertex, weight);

            for (Map.Entry<Integer, Double> entry : representation.getNeighborsWithWeights(vertex)) {
                pq.offer(Map.entry(entry.getKey(), weight + entry.getValue()));
            }
        }

        for (int i = 0; i < representation.vertices(); i++) {
            if (shortest.get(i) == Double.MAX_VALUE) {
                shortest.set(i, -1.0);
            }
        }

        return shortest;
    }
}
