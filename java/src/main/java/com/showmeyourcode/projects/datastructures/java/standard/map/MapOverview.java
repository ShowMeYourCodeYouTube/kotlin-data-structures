package com.showmeyourcode.projects.datastructures.java.standard.map;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * In general, you should use a HashMap.
 * While both classes use keys to look up values, there are some important differences, including:
 * A HashTable doesn't allow null keys or values; a HashMap does.
 * A HashTable is synchronized to prevent multiple threads from accessing it at once; a HashMap isn't.
 *
 * https://www.interviewcake.com/concept/java/hash-map
 */
public class MapOverview {

    private static final Logger LOGGER = LoggerFactory.getLogger(MapOverview.class);

    private MapOverview() {
    }

    public static void main(String[] args) {
        LOGGER.info(
                """
                A map data structure (also known as a dictionary, associative array, or hash map) 
                is defined as a data structure that stores a collection of key-value pairs.
                    
                """
        );
        immutableMap();
        mutableMap();

        concurrent();

        treeMap();
        sortHashMap();

        LOGGER.info("parallelSum100: {}", parallelSum100(10));
    }

    private static void immutableMap() {
        var map = Map.of(1, "one", 5, "five", 8, "eight");
        var hashMap = Collections.unmodifiableMap(new HashMap<>(Map.of(2, "two", 1, "one", 3, "three")));


        LOGGER.info("Immutable map (Map.of): {}", map);
        LOGGER.info("Immutable hashMap (Collections.unmodifiableMap): {}", hashMap);
    }

    private static void mutableMap() {
        var hashMap = new HashMap<Integer, String>();
        var linkedHashMap = new LinkedHashMap<Integer, String>();
        var treeMap = new TreeMap<Integer, String>();

        hashMap.put(2, "two");
        hashMap.put(1, "one");
        linkedHashMap.put(2, "two");
        linkedHashMap.put(1, "one");
        treeMap.put(2, "two");
        treeMap.put(1, "one");

        LOGGER.info("""
                HashMap: {}
                LinkedHashMap: {}
                TreeMap: {}
                """,
                hashMap,
                linkedHashMap,
                treeMap
        );
    }

    private static void concurrent() {
        // In general, you will use a ConcurrentHashMap, if you must have O(1) for both get and put operations,
        // but do not care about the ordering in the collection.
        // You will use a ConcurrentSkipListMap if you need an ordered collection (sorted),
        // but can tolerate O(logn) performance for get and put.
        //
        // ConcurrentHashMap is generally faster for most operations, making it suitable for applications requiring high performance and throughput.
        // ConcurrentSkipListMap provides a sorted map, which is beneficial when you need to maintain key order or perform range queries.
        // ConcurrentHashMap uses lock-striping and atomic operations, whereas ConcurrentSkipListMap employs a skip list data structure for concurrent operations.
        // https://medium.com/@AlexanderObregon/concurrent-maps-in-java-a-guide-to-concurrenthashmap-and-concurrentskiplistmap-293898425176
        var concurrentHashMap = new ConcurrentHashMap<Integer, String>();
        var concurrentSkipList = new ConcurrentSkipListMap<Integer, String>();
        var synchronizedMap = Collections.synchronizedMap(new HashMap<Integer, String>());

        concurrentHashMap.put(5, "five");
        concurrentHashMap.put(1, "one");
        concurrentHashMap.put(2, "two");

        concurrentSkipList.put(5, "five");
        concurrentSkipList.put(1, "one");
        concurrentSkipList.put(4, "four");

        synchronizedMap.put(5, "five");
        synchronizedMap.put(1, "one");
        synchronizedMap.put(3, "three");

        LOGGER.info("Concurrent map (ConcurrentHashMap): {}", concurrentHashMap);
        LOGGER.info("Concurrent map (ConcurrentSkipListMap): {}", concurrentSkipList);
        LOGGER.info("ConcurrentSkipListMap.descendingKeySet: {}", concurrentSkipList.descendingKeySet());
        LOGGER.info("Concurrent map (Collections.synchronizedMap*): {}", synchronizedMap);
        LOGGER.info("*If you need exclusive write access, then you should synchronize.");
    }

    private static void treeMap() {
        SortedMap<String, String> codenames = new TreeMap<>();
        SortedMap<String, String> codenamesInsensitive = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        codenames.put("JDK 1.1.4", "Sparkler");
        codenames.put("J2SE 1.2", "Playground");
        codenames.put("J2SE 1.3", "Kestrel");
        codenames.put("J2SE 1.4", "Merlin");
        codenames.put("J2SE 5.0", "Tiger");
        codenames.put("Java SE 6", "Mustang");
        codenames.put("Java SE 7", "Dolphin");

        codenamesInsensitive.putAll(codenames);

        LOGGER.info("TreeMap: \n" + codenames);
    }

    /**
     * https://www.java67.com/2015/01/how-to-sort-hashmap-in-java-based-on.html
     * https://stackoverflow.com/questions/780541/how-to-sort-a-hashmap-in-java
     * https://www.digitalocean.com/community/tutorials/set-to-list-in-java
     */
    private static void sortHashMap() {
        // let's create a map with Java releases and their code names
        Map<String, String> codenames = new HashMap<>();
        codenames.put("JDK 1.1.4", "Sparkler");
        codenames.put("J2SE 1.2", "Playground");
        codenames.put("J2SE 1.3", "Kestrel");
        codenames.put("J2SE 1.4", "Merlin");
        codenames.put("J2SE 5.0", "Tiger");
        codenames.put("Java SE 6", "Mustang");
        codenames.put("Java SE 7", "Dolphin");

        LOGGER.info("HashMap before sorting, random order: \n{}", codenames);
        Set<Map.Entry<String, String>> entries = codenames.entrySet();

        List<Map.Entry<String, String>> listToSort = new ArrayList<>(entries);

        Collections.sort(listToSort, Map.Entry.<String, String>comparingByValue());
        LOGGER.info("Sort by Value: \n{}", listToSort);
    }

    @SneakyThrows
    private static List<Integer> parallelSum100(int executionTimes) {
        Map<String, Integer> map = new ConcurrentHashMap<>();
        List<Integer> sumList = new ArrayList<>(1000);
        for (int i = 0; i < executionTimes; i++) {
            map.put("test", 0);
            ExecutorService executorService =
                    Executors.newFixedThreadPool(4);
            for (int j = 0; j < 10; j++) {
                executorService.execute(() -> {
                    for (int k = 0; k < 10; k++)
                        map.computeIfPresent(
                                "test",
                                (key, value) -> value + 1
                        );
                });
            }
            executorService.shutdown();
            executorService.awaitTermination(5, TimeUnit.SECONDS);
            sumList.add(map.get("test"));
        }
        return sumList;
    }
}
