package com.showmeyourcode.projects.datastructures.java.standard;

import com.showmeyourcode.projects.datastructures.java.standard.array.ArrayOverview;
import com.showmeyourcode.projects.datastructures.java.standard.heap.HeapOverview;
import com.showmeyourcode.projects.datastructures.java.standard.list.ListOverview;
import com.showmeyourcode.projects.datastructures.java.standard.map.MapOverview;
import com.showmeyourcode.projects.datastructures.java.standard.queue.QueueOverview;
import com.showmeyourcode.projects.datastructures.java.standard.set.SetOverview;
import com.showmeyourcode.projects.datastructures.java.standard.stack.StackOverview;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.showmeyourcode.projects.datastructures.kotlin.common.CommonUtilsKt.word;

public class StandardStructuresOverview {

    private static final Logger LOGGER = LoggerFactory.getLogger(StandardStructuresOverview.class);

    private StandardStructuresOverview() {
    }

    public static void main(String[] args) {
        LOGGER.info("\nStarting Data Structure Overview Application - standard implementations (Java)...");
        LOGGER.info("Test data: '{}'\n", word);

        LOGGER.info("\n===> ARRAY");
        ArrayOverview.main(new String[0]);

        LOGGER.info("\n===> QUEUE");
        QueueOverview.main(new String[0]);

        LOGGER.info("\n===> STACK");
        StackOverview.main(new String[0]);

        LOGGER.info("\n===> LIST");
        ListOverview.main(new String[0]);

        LOGGER.info("\n===> SET");
        SetOverview.main(new String[0]);

        LOGGER.info("\n===> HEAP");
        HeapOverview.main(new String[0]);

        LOGGER.info("\n===> MAP");
        MapOverview.main(new String[0]);
    }
}
