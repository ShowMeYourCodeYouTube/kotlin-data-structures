package com.showmeyourcode.projects.datastructures.java.exercise.lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Write a Java method that merges two sorted lists of integers into a single sorted list.
 * Ensure that the input lists are already sorted in ascending order.
 * <br>
 * <br>
 * Alternative version of thi exercise is to merge two unsorted lists.
 * <br>
 * <br>
 * <br>
 * Merging Unsorted Lists and Sorting vs Merging Sorted Lists
 * <br>
 * Answer: Merging sorted lists directly (O(m + n)) is generally more efficient than merging unsorted lists and then sorting (O((m + n) log (m + n))).
 * <br>
 * Details:
 * <br>
 * Time Complexity: Sorting the merged list using Collections.sort typically has a time complexity of O(k log k), where k is the total number of elements in the merged list. Java's implementation of Collections.sort uses a tuned version of Merge Sort or Tim Sort, which are efficient sorting algorithms.
 * <br>
 * Overall Time Complexity:If you merge two lists of size m and n (totaling m + n elements) and then sort the resulting list, the overall time complexity would be O((m + n) log (m + n)).
 */
public class MergeSortedListsExercise {

    private MergeSortedListsExercise() {
    }

    public static List<Integer> mergeAndSortLists(List<Integer> list1, List<Integer> list2) {
        var merged = new ArrayList<>(list1);
        merged.addAll(list2);

        Collections.sort(merged);

        return merged;
    }

    public static List<Integer> mergeSortedLists(List<Integer> list1, List<Integer> list2) {
        var merged = new ArrayList<Integer>();

        int idx1 = 0;
        int idx2 = 0;

        while (idx1 < list1.size() && idx2 < list2.size()) {
            if (list1.get(idx1) <= list2.get(idx2)) {
                merged.add(list1.get(idx1));
                idx1++;
            } else {
                merged.add(list2.get(idx2));
                idx2++;
            }
        }

        // Add remaining elements from list1 (if any)
        while (idx1 < list1.size()) {
            merged.add(list1.get(idx1));
            idx1++;
        }

        // Add remaining elements from list2 (if any)
        while (idx2 < list2.size()) {
            merged.add(list2.get(idx2));
            idx2++;
        }

        return merged;
    }
}
