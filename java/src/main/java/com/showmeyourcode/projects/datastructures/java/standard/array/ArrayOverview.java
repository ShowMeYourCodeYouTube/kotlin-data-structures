package com.showmeyourcode.projects.datastructures.java.standard.array;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicIntegerArray;

import static com.showmeyourcode.projects.datastructures.kotlin.common.CommonUtilsKt.word;

public class ArrayOverview {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArrayOverview.class);

    private ArrayOverview() {
    }

    public static void main(String[] args) {
        LOGGER.info(
                """
                An array is a linear data structure that collects elements of the same data type 
                and stores them in contiguous and adjacent memory locations. 
                
                It's is mutable with fixed size by default, which means we can perform both read and writes operations on elements of array.
                
                """
        );
        arrays();

        concurrentArray();
    }

    private static void arrays() {
        String[] mutableArray = {word};
        String[] mutableArrayCopy = mutableArray.clone();

        mutableArray = addToArray(mutableArray, "TEST");
        LOGGER.info("Modification 1: {}", Arrays.toString(mutableArray));

        mutableArray[0] = "NEW STRING";
        LOGGER.info("Modification 2: {}", Arrays.toString(mutableArray));
        LOGGER.info("Copy of the array (shallow copy): {}", Arrays.toString(mutableArrayCopy));

        // You cannot make an immutable array.
        // You can only convert it to list and then use Collections.unmodifiableList method
        // to create an immutable collection.
    }

    private static String[] addToArray(String[] arr, String element) {
        String[] result = new String[arr.length + 1];

        // An alternative can be Arrays.copyOf(arr, arr.length+1) which will create a new array
        // using System.arraycopy (shallow copy) under the hood
        System.arraycopy(arr, 0, result, 0, arr.length);
        result[arr.length] = element;
        return result;
    }

    private static void concurrentArray() {
        // AtomicInteger[] is an array of thread safe integers.
        // AtomicIntegerArray is a thread-safe array of integers.
        var atomicIntArray = new AtomicIntegerArray(new int[]{1, 3, 5, 2, 4});

        // There is also AtomicReferenceArray.
        // If you had a shared array of object references,
        // then you would use an AtomicReferenceArray to ensure that
        // the array couldn't be updated simultaneously by different threads
        // i.e. only one element can be updated at a time.

        atomicIntArray.set(0, 100);
        LOGGER.info("Concurrent array (AtomicIntegerArray): {}", atomicIntArray);
    }
}
