package com.showmeyourcode.projects.datastructures.java.custom.tree.bst;

import java.util.Objects;

/**
 * Binary Search Tree (BST) implementation with duplicates as nodes and recursive tree operations.
 * <br>
 * See package-info.java for more details.
 */
class DuplicatesTreeNode<T extends Comparable<T>> extends TreeNode<T>{

    DuplicatesTreeNode<T> left;
    DuplicatesTreeNode<T> right;

    public DuplicatesTreeNode(T value) {
        super(value);
    }

    @Override
    DuplicatesTreeNode<T> getLeft() {
        return left;
    }

    @Override
    DuplicatesTreeNode<T> getRight() {
        return right;
    }

    // =======================
    // standard operations
    // =======================

    @Override
    public void insert(T element) {
        if (this.value.compareTo(element) > 0) {
            if (left == null) {
                left = new DuplicatesTreeNode<>(element);
            } else {
                left.insert(element);
            }
        } else {
            if (this.value.compareTo(element) <= 0) {
                if (right == null) {
                    right = new DuplicatesTreeNode<>(element);
                } else {
                    right.insert(element);
                }
            }
        }
    }

    @Override
    public DuplicatesTreeNode<T> delete(T element) {
        if (value.compareTo(element) == 0) {
            return removeRoot();
        } else if (value.compareTo(element) > 0 && left != null) {
            left = left.delete(element);
        } else if (right != null) {
            right = right.delete(element);
        }
        return this;
    }

    private DuplicatesTreeNode<T> removeRoot() {
        if (left == null && right == null) {
            return null;
        }
        if (right == null) {
            return left;
        } else {
            var greatestNode = findLowestValueNode(right);
            right = right.delete(greatestNode.value);
            var newRoot = new DuplicatesTreeNode<>(greatestNode.value);
            newRoot.right = right;
            newRoot.left = left;
            return newRoot;
        }
    }

    private DuplicatesTreeNode<T> findLowestValueNode(DuplicatesTreeNode<T> node) {
        if (node.left == null) {
            return node;
        } else {
            return findLowestValueNode(node.left);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var node = (DuplicatesTreeNode<T>) o;
        return value == node.value && Objects.equals(left, node.left) && Objects.equals(right, node.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right, value);
    }
}
