package com.showmeyourcode.projects.datastructures.java.exercise.maps;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * You are given a list of temperature readings for different cities over several days.
 * Each reading is a string in the format "city:timestamp:temperature".
 * The 'timestamp' is the date and time of the reading in the format yyyy-MM-ddTHH:mm:ss (ISO 8601 format).
 * Your task is to write a class TemperatureTracker with the following methods:
 * - addReading: Adds a new temperature reading to the tracker.
 * - getMaxTemperatureInRange: Returns the maximum temperature recorded for a given city within a specified date and time range.
 * - getAverageTemperatureInRange: Returns the average temperature recorded for a given city within a specified date and time range.
 */
interface TemperatureTracker {
    void addReading(String reading);

    int getMaxTemperatureInRange(String city, String startTimestamp, String endTimestamp);

    double getAverageTemperatureInRange(String city, String startTimestamp, String endTimestamp);
}

public class TemperatureTrackerExercise implements TemperatureTracker {
    private final Map<String, NavigableMap<LocalDateTime, Integer>> cityReadings = new ConcurrentHashMap<>();
    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

    @Override
    public void addReading(String reading) {
        String[] parts = reading.split(";");
        if (parts.length != 3) {
            throw new IllegalArgumentException("Malformed reading: " + reading);
        }
        String city = parts[0];
        LocalDateTime timestamp = LocalDateTime.parse(parts[1], formatter);
        int temperature = Integer.parseInt(parts[2]);

        cityReadings.putIfAbsent(city, new TreeMap<>());
        cityReadings.get(city).put(timestamp, temperature);
    }

    @Override
    public int getMaxTemperatureInRange(String city, String startTimestamp, String endTimestamp) {
        NavigableMap<LocalDateTime, Integer> readings = cityReadings.get(city);
        if (readings == null) {
            throw new IllegalArgumentException("City not found: " + city);
        }

        LocalDateTime start = LocalDateTime.parse(startTimestamp, formatter);
        LocalDateTime end = LocalDateTime.parse(endTimestamp, formatter);

        return readings.subMap(start, true, end, true).values().stream()
                .mapToInt(Integer::intValue)
                .max()
                .orElseThrow(() -> new IllegalArgumentException("No readings in range for city: " + city));
    }

    @Override
    public double getAverageTemperatureInRange(String city, String startTimestamp, String endTimestamp) {
        NavigableMap<LocalDateTime, Integer> readings = cityReadings.get(city);
        if (readings == null) {
            throw new IllegalArgumentException("City not found: " + city);
        }

        LocalDateTime start = LocalDateTime.parse(startTimestamp, formatter);
        LocalDateTime end = LocalDateTime.parse(endTimestamp, formatter);

        return readings.subMap(start, true, end, true).values().stream()
                .mapToInt(Integer::intValue)
                .average()
                .orElseThrow(() -> new IllegalArgumentException("No readings in range for city: " + city));
    }
}
