package com.showmeyourcode.projects.datastructures.java.custom.tree.avl;

import java.util.*;

class AVLTreeNode<T extends Comparable<T>> {
    T value;
    AVLTreeNode<T> left;
    AVLTreeNode<T> right;
    int height;

    AVLTreeNode(T value) {
        this.value = value;
        this.height = 1;
    }
}

class AVLTree<T extends Comparable<T>> {
    private AVLTreeNode<T> root;

    private int height(AVLTreeNode<T> node) {
        return node == null ? 0 : node.height;
    }

    private void updateHeight(AVLTreeNode<T> node) {
        if (node != null) {
            node.height = 1 + Math.max(height(node.left), height(node.right));
        }
    }

    private int balanceFactor(AVLTreeNode<T> node) {
        return node == null ? 0 : height(node.left) - height(node.right);
    }

    private AVLTreeNode<T> leftRotate(AVLTreeNode<T> y) {
        AVLTreeNode<T> x = y.right;
        AVLTreeNode<T> t2 = x.left;

        // Perform rotation
        x.left = y;
        y.right = t2;

        // Update heights
        updateHeight(y);
        updateHeight(x);

        return x;
    }

    private AVLTreeNode<T> rightRotate(AVLTreeNode<T> x) {
        AVLTreeNode<T> y = x.left;
        AVLTreeNode<T> t2 = y.right;

        // Perform rotation
        y.right = x;
        x.left = t2;

        // Update heights
        updateHeight(x);
        updateHeight(y);

        return y;
    }

    public void insert(T value) {
        root = insertRecursive(root, value);
    }

    private AVLTreeNode<T> insertRecursive(AVLTreeNode<T> node, T value) {
        if (node == null) {
            return new AVLTreeNode<>(value);
        }

        if (value.compareTo(node.value) < 0) {
            node.left = insertRecursive(node.left, value);
        } else if (value.compareTo(node.value) > 0) {
            node.right = insertRecursive(node.right, value);
        } else {
            return node; // Duplicate values are not allowed
        }

        updateHeight(node);

        int balance = balanceFactor(node);

        // Left Left Case
        if (balance > 1 && value.compareTo(node.left.value) < 0) {
            return rightRotate(node);
        }

        // Right Right Case
        if (balance < -1 && value.compareTo(node.right.value) > 0) {
            return leftRotate(node);
        }

        // Left Right Case
        if (balance > 1 && value.compareTo(node.left.value) > 0) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }

        // Right Left Case
        if (balance < -1 && value.compareTo(node.right.value) < 0) {
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }

        return node;
    }

    public void delete(T value) {
        root = deleteRecursive(root, value);
    }

    private AVLTreeNode<T> deleteRecursive(AVLTreeNode<T> node, T value) {
        if (node == null) {
            return null;
        }

        if (value.compareTo(node.value) < 0) {
            node.left = deleteRecursive(node.left, value);
        } else if (value.compareTo(node.value) > 0) {
            node.right = deleteRecursive(node.right, value);
        } else {
            if (node.left == null || node.right == null) {
                return (node.left != null) ? node.left : node.right;
            }

            AVLTreeNode<T> successor = minValueNode(node.right);
            node.value = successor.value;
            node.right = deleteRecursive(node.right, successor.value);
        }

        updateHeight(node);

        int balance = balanceFactor(node);

        // Left Left Case
        if (balance > 1 && balanceFactor(node.left) >= 0) {
            return rightRotate(node);
        }

        // Left Right Case
        if (balance > 1 && balanceFactor(node.left) < 0) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }

        // Right Right Case
        if (balance < -1 && balanceFactor(node.right) <= 0) {
            return leftRotate(node);
        }

        // Right Left Case
        if (balance < -1 && balanceFactor(node.right) > 0) {
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }

        return node;
    }

    private AVLTreeNode<T> minValueNode(AVLTreeNode<T> node) {
        AVLTreeNode<T> current = node;
        while (current.left != null) {
            current = current.left;
        }
        return current;
    }

    public List<T> inOrderTraversal() {
        List<T> result = new ArrayList<>();
        inOrderRecursive(root, result);
        return result;
    }

    private void inOrderRecursive(AVLTreeNode<T> node, List<T> result) {
        if (node != null) {
            inOrderRecursive(node.left, result);
            result.add(node.value);
            inOrderRecursive(node.right, result);
        }
    }

    public List<T> preOrderTraversal() {
        List<T> result = new ArrayList<>();
        preOrderRecursive(root, result);
        return result;
    }

    private void preOrderRecursive(AVLTreeNode<T> node, List<T> result) {
        if (node != null) {
            result.add(node.value);
            preOrderRecursive(node.left, result);
            preOrderRecursive(node.right, result);
        }
    }

    public List<T> postOrderTraversal() {
        List<T> result = new ArrayList<>();
        postOrderRecursive(root, result);
        return result;
    }

    private void postOrderRecursive(AVLTreeNode<T> node, List<T> result) {
        if (node != null) {
            postOrderRecursive(node.left, result);
            postOrderRecursive(node.right, result);
            result.add(node.value);
        }
    }

    public List<T> levelOrderTraversal() {
        List<T> result = new ArrayList<>();
        Queue<AVLTreeNode<T>> queue = new LinkedList<>();

        if (root != null) {
            queue.add(root);
        }

        while (!queue.isEmpty()) {
            AVLTreeNode<T> current = queue.poll();
            result.add(current.value);

            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }

        return result;
    }
}
