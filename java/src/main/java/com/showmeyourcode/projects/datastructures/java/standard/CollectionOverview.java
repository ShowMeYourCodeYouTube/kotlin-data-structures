package com.showmeyourcode.projects.datastructures.java.standard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.stream.Collectors;

public class CollectionOverview {

    public static void main(String[] args) {
        //
        // non-thread safe
        //
        String[][] input = {
                {"10", "Ten"},
                {"1", "One"},
                {"5", "Five"},
                {"15", "Fifteen"},
                {"2", "Two"}
        };


        // List
        //

        // ArrayList - Insertion order
        List<Map.Entry<Integer, String>> arrayList = new ArrayList<>();
        for(String[] element: input){
            arrayList.add(Map.entry(Integer.parseInt(element[0]), element[1]));
        }

        // LinkedList - InsertionOrder (better performance for insertion/delete)
        List<Map.Entry<Integer, String>> linkedList = new LinkedList<>(arrayList);

        // In order to sort, use
        var arrayListSorted = new ArrayList<>(arrayList);
        Collections.sort(arrayListSorted, Comparator.comparing(Map.Entry::getKey));

        Map<Object, List<Map.Entry<Integer, String>>> groupedBy = arrayList.stream()
                .collect(Collectors.groupingBy(e->e.getKey().toString().startsWith("1"), Collectors.toList()));

        System.out.println("ArrayList: " + arrayList);
        System.out.println("LinkedList: " + linkedList);
        System.out.println("Sorted ArrayList: " + arrayListSorted);
        System.out.println("GroupedBy: " + groupedBy + "\n");

        // Stack / Dequeue
        //

        Deque<Map.Entry<Integer, String>> stack = new LinkedList<>();
        for(Map.Entry<Integer, String> element: arrayList){
            stack.push(element);
        }
        // use stack.pop() to remove or stack.peek() to check the first element
        System.out.println("Stack: " + stack + "\n");

        // Queue / Dequeue
        //

        Deque<Map.Entry<Integer, String>> queue = new LinkedList<>();
        for(Map.Entry<Integer, String> element: arrayList){
            // add vs offer
            // offer(E e)	Returns true if successful, false if the queue is full (for bounded queues).
            // add(E e)	Throws an exception (IllegalStateException) if the queue is full (for bounded queues).
            queue.offer(element);
        }
        // use stack.remove() to remove or queue.element() to check the first element
        System.out.println("Queue: " + queue + "\n");

        // Set
        //

        Set<Map.Entry<Integer, String>> hashSet = new HashSet<>();
        for(Map.Entry<Integer, String> element: arrayList){
            hashSet.add(element);
        }
        for(Map.Entry<Integer, String> element: arrayList){
            hashSet.add(element);
        }

        // LinkedHashSet - insertion order
        Set<Map.Entry<Integer, String>> linkedHashSet = new LinkedHashSet<>();
        for(Map.Entry<Integer, String> element: arrayList){
            linkedHashSet.add(element);
        }

        SortedSet<Map.Entry<Integer, String>> sortedTreeSet = new TreeSet<>(Map.Entry.comparingByKey());
        for(Map.Entry<Integer, String> element: arrayList){
            sortedTreeSet.add(element);
        }

        NavigableSet<Map.Entry<Integer, String>> navigableTreeSet = new TreeSet<>(Map.Entry.comparingByKey());
        for(Map.Entry<Integer, String> element: arrayList){
            navigableTreeSet.add(element);
        }

        System.out.println("HashSet: " + hashSet);
        System.out.println("LinkedHashSet: " + linkedHashSet);
        System.out.println("TreeSet (SortedSet): " + sortedTreeSet);
        System.out.println("TreeSet (NavigableSet): " + navigableTreeSet + "\n");

        // Map
        //

        Map<Integer, String> hashMap = new HashMap<>();
        for(Map.Entry<Integer, String> element: arrayList){
            hashMap.put(element.getKey(), element.getValue());
        }

        // LinkedHashMap - insertion order
        Map<Integer, String> linkedHasMap = new LinkedHashMap<>();
        for(Map.Entry<Integer, String> element: arrayList){
            linkedHasMap.put(element.getKey(), element.getValue());
        }

        SortedMap<Integer, String>  sortedTreeMap = new TreeMap<>();
        for(Map.Entry<Integer, String> element: arrayList){
            sortedTreeMap.put(element.getKey(), element.getValue());
        }

        NavigableMap<Integer, String>  navigableTreeMapReverseOrder = new TreeMap<>(Comparator.reverseOrder());
        for(Map.Entry<Integer, String> element: arrayList){
            navigableTreeMapReverseOrder.put(element.getKey(), element.getValue());
        }

        System.out.println("HashMap: " + hashMap);
        System.out.println("LinkedHashMap: " + linkedHasMap);
        System.out.println("TreeMap (SortedMap): " + sortedTreeMap);
        System.out.println("TreeMap (NavigableMap): " + navigableTreeMapReverseOrder + "\n");

        //
        // thread safe
        //

        // List
        //
        List<Map.Entry<Integer, String>> synchronizedArrayList = Collections.synchronizedList(new ArrayList<>());
        for(String[] element: input){
            synchronizedArrayList.add(Map.entry(Integer.parseInt(element[0]), element[1]));
        }

        List<Map.Entry<Integer, String>> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
        for(String[] element: input){
            copyOnWriteArrayList.add(Map.entry(Integer.parseInt(element[0]), element[1]));
        }

        System.out.println("Collections.synchronizedList(ArrayList): " + synchronizedArrayList);
        System.out.println("CopyOnWriteArrayList: " + copyOnWriteArrayList + "\n");

        // Stack / Dequeue
        //

        BlockingDeque<Map.Entry<Integer, String>> linkedBlockingDeque = new LinkedBlockingDeque<>();
        for(Map.Entry<Integer, String> element: arrayList){
            linkedBlockingDeque.push(element);
        }
        // poll() (Non-blocking)
        // Does not wait if the queue is empty.
        // Returns null immediately if no element is available.
        //
        // poll(long timeout, TimeUnit unit) (Blocking)
        // Waits for an element to become available (up to the given timeout).
        // Returns null if no element is available within the timeout.
        //
        // take() (Fully Blocking)
        // Waits indefinitely until an element is available.

        Deque<Map.Entry<Integer, String>> concurrentLinkedDeque = new ConcurrentLinkedDeque<>();
        for(Map.Entry<Integer, String> element: arrayList){
            concurrentLinkedDeque.push(element);
        }

        System.out.println("LinkedBlockingDeque: " + linkedBlockingDeque);
        System.out.println("ConcurrentLinkedDeque: " + concurrentLinkedDeque + "\n");

        // Queue / Dequeue
        //

        BlockingQueue<Map.Entry<Integer, String>> arrayBlockingQueue = new ArrayBlockingQueue<>(10);
        for(Map.Entry<Integer, String> element: arrayList){
            // Use add() only if you want to enforce strict insertion (i.e., throw an exception if the queue is full).
            arrayBlockingQueue.add(element);
        }


        BlockingQueue<Map.Entry<Integer, String>> priorityBlockingQueue = new PriorityBlockingQueue<>(
                10,
                Map.Entry.comparingByKey()
        );
        for(Map.Entry<Integer, String> element: arrayList){
            // Use add() only if you want to enforce strict insertion (i.e., throw an exception if the queue is full).
            priorityBlockingQueue.add(element);
        }

        System.out.println("ArrayBlockingQueue: " + arrayBlockingQueue);
        System.out.println("PriorityBlockingQueue: " + priorityBlockingQueue + "\n");

        // Set
        //
        Set<Map.Entry<Integer, String>> synchronizedHashSet = Collections.synchronizedSet(new HashSet<>());
        for(String[] element: input){
            synchronizedHashSet.add(Map.entry(Integer.parseInt(element[0]), element[1]));
        }

        Set<Map.Entry<Integer, String>> copyOnWriteArraySet = new CopyOnWriteArraySet<>();
        for(String[] element: input){
            copyOnWriteArraySet.add(Map.entry(Integer.parseInt(element[0]), element[1]));
        }

        Set<Map.Entry<Integer, String>> concurrentSkipListSet = new ConcurrentSkipListSet<>(Map.Entry.comparingByKey());
        for(String[] element: input){
            concurrentSkipListSet.add(Map.entry(Integer.parseInt(element[0]), element[1]));
        }

        System.out.println("Collections.synchronizedSet(HashSet): " + synchronizedHashSet);
        System.out.println("CopyOnWriteArraySet: " + copyOnWriteArraySet);
        System.out.println("ConcurrentSkipListSet: " + concurrentSkipListSet + "\n");

        // Map
        //

        Map<Integer, String> synchronizedHashMap = Collections.synchronizedMap(new HashMap<>());
        for(String[] element: input){
            synchronizedHashMap.put(Integer.parseInt(element[0]), element[1]);
        }

        Map<Integer, String> concurrentHashMap = new ConcurrentHashMap<>();
        for(String[] element: input){
            concurrentHashMap.put(Integer.parseInt(element[0]), element[1]);
        }

        Map<Integer, String> concurrentSkipListMap = new ConcurrentSkipListMap<>(Comparator.reverseOrder());
        for(String[] element: input){
            concurrentSkipListMap.put(Integer.parseInt(element[0]), element[1]);
        }

        System.out.println("Collections.synchronizedMap(HashMap): " + synchronizedHashMap);
        System.out.println("ConcurrentHashMap: " + concurrentHashMap);
        System.out.println("ConcurrentSkipListMap: " + concurrentSkipListMap + "\n");

    }
}
