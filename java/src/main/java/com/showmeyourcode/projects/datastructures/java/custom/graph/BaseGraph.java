package com.showmeyourcode.projects.datastructures.java.custom.graph;

import java.util.*;

public class BaseGraph {

    protected final GraphRepresentation representation;

    protected BaseGraph(GraphRepresentation representation) {
        this.representation = representation;
    }

    List<Integer> bfs(int startingVertex) {
        List<Integer> visited = new ArrayList<>();
        Queue<Integer> toVisit = new LinkedList<>();
        toVisit.offer(startingVertex);

        while (!toVisit.isEmpty()) {
            var vertex = toVisit.poll();

            if (!visited.contains(vertex)) {
                visited.add(vertex);
            }

            for (Integer neighbour : representation.getNeighbors(vertex)) {
                if (!visited.contains(neighbour)) {
                    toVisit.offer(neighbour);
                }
            }
        }

        return visited;
    }

    List<Integer> dfs(int startingVertex) {
        List<Integer> visited = new ArrayList<>();
        Deque<Integer> toVisit = new LinkedList<>();
        toVisit.addFirst(startingVertex);

        while (!toVisit.isEmpty()) {
            int vertex = toVisit.poll();

            if (!visited.contains(vertex)) {
                visited.add(vertex);
            }

            var neighbours = representation.getNeighbors(vertex);
            for (int i = neighbours.size() - 1; i >= 0; i--) {
                if (!visited.contains(neighbours.get(i))) {
                    toVisit.addFirst(neighbours.get(i));
                }
            }
        }

        return visited;
    }

    boolean hasCycleBFS(){
        int[] inDegrees = new int[representation.vertices()];
        Queue<Integer> queue = new LinkedList<>();

        // Calculate in-degrees of all vertices
        for (int i = 0; i < representation.vertices(); i++) {
            for (int neighbor : representation.getNeighbors(i)) {
                inDegrees[neighbor]++;
            }
        }

        // Add all vertices with in-degree 0 to the queue
        for (int i = 0; i < representation.vertices(); i++) {
            if (inDegrees[i] == 0) {
                queue.add(i);
            }
        }

        int processedVertices = 0;

        // Process vertices in the queue
        while (!queue.isEmpty()) {
            int current = queue.poll();
            processedVertices++;

            // Reduce the in-degree of all neighbors
            for (int neighbor : representation.getNeighbors(current)) {
                inDegrees[neighbor]--;
                if (inDegrees[neighbor] == 0) {
                    queue.add(neighbor);
                }
            }
        }

        // If the number of processed vertices is less than the total vertices, there is a cycle
        return processedVertices != representation.vertices();
    }

    boolean hasCycleDFS() {
        var visited = new boolean[representation.vertices()];
        var recursionStack = new boolean[representation.vertices()];

        for (int i = 0; i < representation.vertices(); i++) {
            if (!visited[i] && dfsRecursiveCycle(i, visited, recursionStack)) {
                return true;
            }
        }

        return false;
    }

    private boolean dfsRecursiveCycle(int vertex, boolean[] visited, boolean[] recursionStack) {
        visited[vertex] = true;
        recursionStack[vertex] = true;

        for (int neighbour : representation.getNeighbors(vertex)) {
            if (!visited[neighbour]) {
                if (dfsRecursiveCycle(neighbour, visited, recursionStack)) {
                    return true;
                }
            } else if (recursionStack[neighbour]) {
                return true;
            }
        }

        recursionStack[vertex] = false;
        return false;
    }

    List<List<Integer>> toList() {
        return representation.toList();
    }
}
