package com.showmeyourcode.projects.datastructures.java.custom.array;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DynamicArrayTest {

    private DynamicArray dynamicArray;

    private static int[][] provideArrays() {
        return new int[][]{
                {0, 1, 2},        // Simple case with 3 elements
                {5, 10, 15, 20},  // Larger array with 4 elements
                {1, 3, 7, 9, 12}, // Array with 5 elements
                {0},              // Array with a single element
                {}                // Empty array
        };
    }

    @BeforeEach
    public void setup() {
        dynamicArray = new DynamicArray(2);  // Initialize with a small capacity to test resizing
    }

    @ParameterizedTest
    @MethodSource("provideArrays")
    public void shouldInsertArrayAndReturnAllElements(int[] valuesToInsert) {
        // Insert all values from the array into the dynamic array
        for (int value : valuesToInsert) {
            dynamicArray.pushback(value);
        }

        // Check that the size matches the number of elements inserted
        assertEquals(valuesToInsert.length, dynamicArray.getSize(), "Size should match the number of inserted elements");

        // Check that each element is correctly inserted
        for (int i = 0; i < valuesToInsert.length; i++) {
            assertEquals(valuesToInsert[i], dynamicArray.get(i), "Element at index " + i + " should be " + valuesToInsert[i]);
        }
    }

    @Test
    void shouldSetElementAtGivenIndex() {
        dynamicArray.pushback(10);
        dynamicArray.set(0, 20);
        assertEquals(20, dynamicArray.get(0), "Expected 20 after set");
    }

    @Test
    void shouldResizeArrayWhenCapacityIsExceeded() {
        dynamicArray.pushback(1);
        dynamicArray.pushback(2);
        assertEquals(2, dynamicArray.getSize(), "Expected size to be 2");
        assertEquals(2, dynamicArray.getCapacity(), "Expected capacity to be 2 initially");

        // Push more elements to test resize
        dynamicArray.pushback(3);
        assertEquals(4, dynamicArray.getCapacity(), "Expected capacity to double after resizing");
        assertEquals(3, dynamicArray.getSize(), "Expected size to be 3 after pushing back 3 elements");
    }

    @Test
    void shouldPopBackLastElementCorrectly() {
        dynamicArray.pushback(1);
        dynamicArray.pushback(2);
        dynamicArray.pushback(3);

        assertEquals(3, dynamicArray.popback(), "Expected 3 as the last element");
        assertEquals(2, dynamicArray.popback(), "Expected 2 as the next last element");
        assertEquals(1, dynamicArray.popback(), "Expected 1 as the first element");
        assertEquals(-1, dynamicArray.popback(), "Expected -1 when popping from empty array");
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 10})
    void shouldReturnNegativeOneWhenGettingOutOfBounds(int index) {
        // Testing get for an index out of bounds
        assertEquals(-1, dynamicArray.get(index), "Expected -1 for out-of-bounds access");
    }

    @Test
    void shouldNotSetElementOutOfBounds() {
        dynamicArray.pushback(5);
        // Trying to set a value outside the array's current size
        dynamicArray.set(10, 100);
        assertEquals(5, dynamicArray.get(0), "Expected the original value because index 10 is out of bounds");
    }

    @Test
    void shouldReturnCorrectSize() {
        assertEquals(0, dynamicArray.getSize(), "Expected size to be 0 initially");
        dynamicArray.pushback(1);
        assertEquals(1, dynamicArray.getSize(), "Expected size to be 1 after pushing an element");
    }

    @Test
    void shouldReturnCorrectCapacity() {
        assertEquals(2, dynamicArray.getCapacity(), "Expected initial capacity to be 2");
        dynamicArray.pushback(1);
        dynamicArray.pushback(2);
        dynamicArray.pushback(3);  // Should trigger resizing
        assertEquals(4, dynamicArray.getCapacity(), "Expected capacity to be 4 after resize");
    }
}
