package com.showmeyourcode.projects.datastructures.java.custom.list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SinglyLinkedListTest {

    private SinglyLinkedList list;

    @BeforeEach
    public void setup() {
        list = new SinglyLinkedList();
    }

    @Test
    public void shouldReturnNegativeOneForEmptyListOnGet() {
        assertEquals(-1, list.get(0), "Get should return -1 for empty list");
    }

    @Test
    public void shouldReturnCorrectValueAfterInsertHead() {
        list.insertHead(10);
        assertEquals(10, list.get(0), "Head value should be correct after insertion");
    }

    @Test
    public void shouldInsertMultipleElementsAtHead() {
        list.insertHead(20);
        list.insertHead(10);
        list.insertHead(5);

        assertEquals(5, list.get(0), "First element should be 5");
        assertEquals(10, list.get(1), "Second element should be 10");
        assertEquals(20, list.get(2), "Third element should be 20");
    }

    @Test
    public void shouldReturnNegativeOneForOutOfBoundsOnGet() {
        list.insertHead(10);
        list.insertHead(20);
        list.insertHead(30);

        // Out of bounds
        assertEquals(-1, list.get(5), "Get should return -1 for invalid index");
    }

    @Test
    public void shouldInsertTailCorrectly() {
        list.insertTail(10);
        assertEquals(10, list.get(0), "Tail insertion should place the element at index 0");

        list.insertTail(20);
        assertEquals(20, list.get(1), "Tail insertion should place the second element at index 1");
    }

    @Test
    public void shouldInsertMultipleElementsAtTail() {
        list.insertTail(10);
        list.insertTail(20);
        list.insertTail(30);

        assertEquals(10, list.get(0), "First element should be 10");
        assertEquals(20, list.get(1), "Second element should be 20");
        assertEquals(30, list.get(2), "Third element should be 30");
    }

    @Test
    public void shouldRemoveHeadCorrectly() {
        list.insertHead(10);
        list.insertHead(20);
        list.insertHead(30);

        assertTrue(list.remove(0), "Should remove head element");
        assertEquals(20, list.get(0), "After removing head, new head should be 20");
        assertEquals(10, list.get(1), "Next element should still be in the list");

        assertTrue(list.remove(0), "Should remove new head element");
        assertEquals(10, list.get(0), "After removing, new head should be 10");
    }

    @Test
    public void shouldRemoveTailCorrectly() {
        list.insertTail(10);
        list.insertTail(20);
        list.insertTail(30);

        assertTrue(list.remove(2), "Should remove tail element at index 2");
        assertEquals(20, list.get(1), "Tail removal should leave the correct remaining elements");
    }

    @Test
    public void shouldReturnFalseWhenRemovingOutOfBounds() {
        assertFalse(list.remove(0), "Should return false for removal at invalid index");
    }

    @Test
    public void shouldReturnCorrectValues() {
        list.insertHead(5);
        list.insertTail(10);
        list.insertTail(15);

        assertEquals(5, list.get(0), "First element should be 5");
        assertEquals(10, list.get(1), "Second element should be 10");
        assertEquals(15, list.get(2), "Third element should be 15");

        // Check values array
        assertEquals(3, list.getValues().size(), "Values list should contain 3 elements");
        assertTrue(list.getValues().contains(5), "Values list should contain 5");
        assertTrue(list.getValues().contains(10), "Values list should contain 10");
        assertTrue(list.getValues().contains(15), "Values list should contain 15");
    }

    @Test
    public void shouldUpdateTailAfterRemoval() {
        list.insertHead(10);
        list.insertHead(20);
        list.insertTail(30);

        assertEquals(30, list.get(2), "Tail should initially be 30");
        list.remove(2); // Remove tail (30)
        assertEquals(10, list.get(1), "New tail should be 10 after removal of old tail");
    }

    @Test
    public void shouldReturnTrueWhenListIsEmpty() {
        assertEquals(-1, list.get(0), "Empty list should return -1 for get method");
    }

    @Test
    public void shouldHandleMultipleRemovals() {
        list.insertHead(10);
        list.insertTail(20);
        list.insertTail(30);

        // Remove head (index 0)
        assertTrue(list.remove(0), "Should successfully remove the first element (head)");
        assertEquals(20, list.get(0), "New head should be 20");

        // Remove tail (index 1, the last element)
        assertTrue(list.remove(1), "Should successfully remove the last element (tail)");
        assertEquals(20, list.get(0), "Only one element left, should be 20");

        // Remove the last remaining element
        assertTrue(list.remove(0), "Should successfully remove the last element");
        assertEquals(-1, list.get(0), "List should be empty after all removals");
    }
}
