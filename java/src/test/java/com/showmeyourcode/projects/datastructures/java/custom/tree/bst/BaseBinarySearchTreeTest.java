package com.showmeyourcode.projects.datastructures.java.custom.tree.bst;

import com.showmeyourcode.projects.datastructures.java.StringToIntListConverter;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

abstract class BaseBinarySearchTreeTest {

    abstract @NotNull TreeNode<Integer> makeTree(List<Integer> valuesToInsert);

    @ParameterizedTest
    @CsvSource({
            "'1,2,3,4,5,6,7,8,9,10,10',10",
            "'10,9,8,7,6,5,4,3,2,1,1',10",
            "'2,4,1,3,3',4",
            "'5,2,1,3,8,7,9,9',9",
            "'10,5,15,3,7,7',15",
            "'1,1', 1"
    })
    void shouldFindMaxValue(@ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert, int min) {
        var root = makeTree(valuesToInsert);

        assertEquals(min, root.findMax());
    }

    @ParameterizedTest
    @CsvSource({
            "'1,2,3,4,5,6,7,8,9,10,10',1",
            "'10,9,8,7,6,5,4,3,2,1,1',1",
            "'2,4,1,3,3',1",
            "'5,2,1,3,8,7,9,9',1",
            "'10,5,15,3,7,7',3",
            "'1,1', 1"
    })
    void shouldFindMinValue(@ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert, int min) {
        var root = makeTree(valuesToInsert);

        assertEquals(min, root.findMin());
    }

    @ParameterizedTest
    @CsvSource({
            "'1,2,3,4,5,6,7,8,9,10',9",
            "'10,9,8,7,6,5,4,3,2,1',9",
            "'2,4,1',1",
            "'5,2,1,3,8,7,9',2",
            "'10,5,15,3,7,1',3",
            "'1',0"
    })
    void shouldCalculateTreeHeight(@ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert, int expectedHeight) {
        var root = makeTree(valuesToInsert);

        assertEquals(expectedHeight, root.height());
    }

    @Test
    void shouldNotProduceErrorWhenRemovingNonExistentElement() {
        List<Integer> valuesToInsert = Arrays.asList(1, 2, 3, 4, 5);
        var root = makeTree(valuesToInsert);

        root = root.delete(10);
        for (int v : valuesToInsert) {
            assertTrue(root.find(v));
        }
        assertEquals(valuesToInsert.size(), root.countNodes());
    }

    @ParameterizedTest
    @CsvSource({
//            "'1,2,3,4,5,6,7,8,9,10',1",
//            "'10,9,8,7,6,5,4,3,2,1',1",
//            "'2,4,1',2",
//            "'5,2,1,3,8,7,9',4",
//            "'10,5,15,3,7',3",
//            "'1',1",
//            "'1,2,3,4,5,6,7,8,9,10,10,10',1",
//            "'10,9,8,7,6,5,4,3,2,1,1,1',1",
            "'2,4,1,1,1',2"
    })
    void shouldCorrectlyCountElements(@ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert, int leafNodesNo) {
        var root = makeTree(valuesToInsert);

        assertEquals(valuesToInsert.size(), root.countNodes());
        assertEquals(leafNodesNo, root.countLeafNodes());
    }

    @ParameterizedTest
    @CsvSource({
            "'1,2,3,4,5,6,7,8,9,10,10'",
            "'10,9,8,7,6,5,4,3,2,1,1'",
            "'2,4,1,3,3'",
            "'5,2,1,3,8,7,9,9'",
            "'10,5,15,3,7,7'",
            "'1,1'"
    })
    void shouldFindValue(@ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert) {
        var root = makeTree(valuesToInsert);

        for (int v : valuesToInsert) {
            assertTrue(root.find(v));
        }
    }

    @ParameterizedTest
    @CsvSource({
            "'1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10'",
            "'10,9,8,7,6,5,4,3,2,1'",
            "'2,4,1,3'",
            "'5,2,1,3,8,7,9,9'",
            "'10,5,15,3,7'",
            "'1,1'"
    })
    void shouldRemoveValue(@ConvertWith(StringToIntListConverter.class) List<Integer> treeNodes) {
        var root = makeTree(treeNodes);
        // remove all elements except the last one as it should remove completely the tree and produce a null value
        List<Integer> valuesToRemoveWithoutLastElement = treeNodes.subList(0, treeNodes.size() - 1);

        for (int j : valuesToRemoveWithoutLastElement) {
            var toRemove = j;
            assertTrue(root.find(toRemove));
            root = root.delete(toRemove);
        }

        // verify that removing the last element produces a null as the root
        int theLastElement = treeNodes.get(treeNodes.size() - 1);
        assertTrue(root.find(theLastElement));
        root = root.delete(theLastElement);
        assertNull(root);
    }

    @ParameterizedTest
    @CsvSource({
            "'1,2,3,4,5,6,7,8,9,10','1,2,3,4,5,6,7,8,9,10'",
            "'10,9,8,7,6,5,4,3,2,1','10,9,8,7,6,5,4,3,2,1'",
            "'2,4,1','2,1,4'",
            "'5,2,1,3,8,7,9','5,2,8,1,3,7,9'",
            "'10,5,15,3,7,1','10,5,15,3,7,1'",
            "'1','1'",
            "'10,5,15,3,7,12,18', '10,5,15,3,7,12,18'",
            "'10,20,30,40,50,60', '10,20,30,40,50,60'"
    })
    void shouldReturnOrderAccordinglyToBFS(
            @ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert,
            @ConvertWith(StringToIntListConverter.class) List<Integer> expectedOutput) {
        var root = makeTree(valuesToInsert);

        assertIterableEquals(
                expectedOutput,
                root.bfs()
        );
    }

    @ParameterizedTest
    @CsvSource({
            "'11,12,12,1','11,1,12,12'",
            "'10,5,15,5,5', '10,5,15,5,5'",
            "'10,8,6,4,2,2,2', '10,8,6,4,2,2,2'"
    })
    void shouldReturnOrderAccordinglyToBFSWithDuplicates(
            @ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert,
            @ConvertWith(StringToIntListConverter.class) List<Integer> expectedOutput) {
        var root = makeTree(valuesToInsert);

        assertIterableEquals(
                expectedOutput,
                root.bfs()
        );
    }

    @ParameterizedTest
    @CsvSource({
            "'1,2,3,4,5,6,7,8,9,10','1,2,3,4,5,6,7,8,9,10'",
            "'10,9,8,7,6,5,4,3,2,1','10,9,8,7,6,5,4,3,2,1'",
            "'2,4,1','2,1,4'",
            "'5,2,1,3,8,7,9','5,2,1,3,8,7,9'",
            "'10,5,15,3,7,1','10,5,3,1,7,15'",
            "'1','1'",
            "'1,2,3,4,5','1,2,3,4,5'",
            "'10,5,15,3,7,12,18', '10,5,3,7,15,12,18'",
            "'10,20,30,40,50,60', '10,20,30,40,50,60'",
            "'10,8,6,4,2', '10,8,6,4,2'"
    })
    void shouldReturnOrderAccordinglyToDFSPreorder(
            @ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert
            , @ConvertWith(StringToIntListConverter.class) List<Integer> expectedOutput) {
        var root = makeTree(valuesToInsert);

        assertIterableEquals(
                expectedOutput,
                root.dfsPreorder()
        );
    }

    @ParameterizedTest
    @CsvSource({
            "'1,2,3,4,5,6,7,8,9,10','1,2,3,4,5,6,7,8,9,10'",
            "'10,9,8,7,6,5,4,3,2,1','1,2,3,4,5,6,7,8,9,10'",
            "'2,4,1','1,2,4'",
            "'5,2,1,3,8,7,9','1,2,3,5,7,8,9'",
            "'10,5,15,3,7,1','1,3,5,7,10,15'",
            "'1','1'",
            "'1,2,3,4,5','1,2,3,4,5'",
            "'10,5,15,3,7,12,18', '3,5,7,10,12,15,18'",
            "'10,20,30,40,50,60', '10,20,30,40,50,60'",
            "'10,8,6,4,2', '2,4,6,8,10'"
    })
    void shouldReturnOrderAccordinglyToDFSInorder(@ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert,
                                                  @ConvertWith(StringToIntListConverter.class) List<Integer> expectedOutput) {
        var root = makeTree(valuesToInsert);

        assertIterableEquals(
                expectedOutput,
                root.dfsInorder()
        );
    }

    @ParameterizedTest
    @CsvSource({
            "'1,2,3,4,5,6,7,8,9,10','10,9,8,7,6,5,4,3,2,1'",
            "'10,9,8,7,6,5,4,3,2,1','1,2,3,4,5,6,7,8,9,10'",
            "'2,4,1','1,4,2'",
            "'5,2,1,3,8,7,9','1,3,2,7,9,8,5'",
            "'10,5,15,3,7,1','1,3,7,5,15,10'",
            "'1','1'",
            "'1,2,3,4,5','5,4,3,2,1'",
            "'10,5,15,3,7,12,18', '3,7,5,12,18,15,10'",
            "'10,20,30,40,50,60', '60,50,40,30,20,10'",
            "'10,8,6,4,2', '2,4,6,8,10'"
    })
    void shouldReturnOrderAccordinglyToDFSPostorder(@ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert,
                                                    @ConvertWith(StringToIntListConverter.class) List<Integer> expectedOutput) {
        var root = makeTree(valuesToInsert);

        assertIterableEquals(
                expectedOutput,
                root.dfsPostorder()
        );
    }

    @ParameterizedTest
    @CsvSource({
            // Identical trees
            "'10,5,15,3,7', '10,5,15,3,7', true",
            // Different structures
            "'10,5,15,3,7', '10,5,15', false",
            // Different values
            "'10,5,15,3,7', '10,6,15,3,7', false",
            // Same structure, different order of insertion
            "'10,5,15,3,7', '10,15,5,3,7', true",
            // Single node trees
            "'10', '10', true"
    })
    void shouldVerifyEqualsMethod(
            @ConvertWith(StringToIntListConverter.class) List<Integer> tree1Values,
            @ConvertWith(StringToIntListConverter.class) List<Integer> tree2Values,
            boolean expectedResult) {

        var tree1 = makeTree(tree1Values);
        var tree2 = makeTree(tree2Values);

        assertEquals(expectedResult, tree1.equals(tree2));
    }
}
