package com.showmeyourcode.projects.datastructures.java.exercise;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class ExercisesTest {

    @Test
    void shouldRunTheMainMethodWithoutErrors() {
        assertDoesNotThrow(() -> Exercises.main(new String[0]));
    }
}
