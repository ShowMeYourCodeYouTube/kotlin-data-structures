package com.showmeyourcode.projects.datastructures.java.custom.tree.rb;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RedBlackTreeTest {

    @Test
    void shouldInsertNodesAndMaintainRedBlackTreeProperties() {
        RedBlackTree<Integer> redBlackTree = new RedBlackTree<>();

        redBlackTree.insert(10);
        redBlackTree.insert(7);
        redBlackTree.insert(6);
        redBlackTree.insert(2);
        redBlackTree.insert(11);
        redBlackTree.insert(8);
        redBlackTree.insert(1);
        redBlackTree.insert(5);
        redBlackTree.insert(0);
        redBlackTree.insert(15);
        redBlackTree.insert(13);
        redBlackTree.insert(17);
        redBlackTree.insert(16);
        redBlackTree.insert(14);

        List<Integer> inOrderResult = redBlackTree.inOrderTraversal();
        assertEquals(List.of(0, 1, 2, 5, 6, 7, 8, 10, 11, 13, 14, 15, 16, 17), inOrderResult);
    }

    @Test
    void shouldThrowExceptionWhenDeletingNodesAsDeleteIsNotImplemented() {
        RedBlackTree<Integer> redBlackTree = new RedBlackTree<>();

        Executable executable = () -> redBlackTree.delete(1);
        assertThrows(UnsupportedOperationException.class, executable);
    }

    @Test
    void shouldReturnNodesInPreOrder() {
        RedBlackTree<Integer> redBlackTree = new RedBlackTree<>();

        redBlackTree.insert(10);
        redBlackTree.insert(5);
        redBlackTree.insert(15);
        redBlackTree.insert(3);
        redBlackTree.insert(7);

        List<Integer> preOrderResult = redBlackTree.preOrderTraversal();
        assertEquals(List.of(10, 5, 3, 7, 15), preOrderResult);
    }

    @Test
    void shouldReturnNodesInPostOrder() {
        RedBlackTree<Integer> redBlackTree = new RedBlackTree<>();

        redBlackTree.insert(10);
        redBlackTree.insert(5);
        redBlackTree.insert(15);
        redBlackTree.insert(3);
        redBlackTree.insert(7);

        List<Integer> postOrderResult = redBlackTree.postOrderTraversal();
        assertEquals(List.of(3, 7, 5, 15, 10), postOrderResult);
    }

    @Test
    void shouldReturnNodesInLevelOrder() {
        RedBlackTree<Integer> redBlackTree = new RedBlackTree<>();

        redBlackTree.insert(10);
        redBlackTree.insert(5);
        redBlackTree.insert(15);
        redBlackTree.insert(3);
        redBlackTree.insert(7);

        List<Integer> levelOrderResult = redBlackTree.levelOrderTraversal();
        assertEquals(List.of(10, 5, 15, 3, 7), levelOrderResult);
    }
}

