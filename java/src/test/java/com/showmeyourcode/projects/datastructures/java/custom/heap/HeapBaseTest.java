package com.showmeyourcode.projects.datastructures.java.custom.heap;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

abstract class HeapBaseTest {

    static Stream<Arguments> maxElement() {
        return Stream.of(
                Arguments.of(
                        Arrays.asList(2, 12, 3, 1, 123, 54, 444, 555, 1, 223),
                        555,
                        Arrays.asList(555, 444, 123, 12, 223, 3, 54, 1, 1, 2)
                ),
                Arguments.of(
                        Arrays.asList(1, 2, 3, 4, 5, 6),
                        6,
                        Arrays.asList(6, 4, 5, 1, 3, 2)
                ),
                Arguments.of(
                        Arrays.asList(1, 11, 2, 44, 98, 6, 4, 33678, 90, 11, 46, 123),
                        33678,
                        Arrays.asList(33678, 98, 123, 90, 46, 6, 4, 1, 44, 11, 11, 2)
                ),
                Arguments.of(
                        Arrays.asList(7, 4, 8, 7, 6, 9, 3, 234, 231, 5, 3, 2, 23),
                        234,
                        Arrays.asList(234, 231, 23, 9, 6, 8, 3, 4, 7, 5, 3, 2, 7)
                )
        );
    }

    static Stream<Arguments> minElement() {
        return Stream.of(
                Arguments.of(
                        Arrays.asList(2, 12, 3, 1, 123, 54, 444, 555, 1, 223), 1, Arrays.asList(1, 1, 3, 2, 123, 54, 444, 555, 12, 223)
                ),
                Arguments.of(
                        Arrays.asList(1, 2, 3, 4, 5, 6), 1, Arrays.asList(1, 2, 3, 4, 5, 6)
                ),
                Arguments.of(
                        Arrays.asList(3, 11, 4, 44, 98, 6, 4, 33678, 90, 1, 46, 123), 1, Arrays.asList(1, 3, 4, 44, 11, 6, 4, 33678, 90, 98, 46, 123)
                ),
                Arguments.of(
                        Arrays.asList(7, 4, 8, 7, 6, 9, 3, 234, 231, 5, 3, 2, 23), 2, Arrays.asList(2, 3, 3, 7, 5, 4, 8, 234, 231, 7, 6, 9, 23)
                )
        );
    }

    static Stream<Arguments> removeMinElement() {
        return Stream.of(
                Arguments.of(
                        Arrays.asList(2, 12, 3, 1, 123, 54, 444, 555, 1, 223), Arrays.asList(1, 1, 2, 3, 12)
                ),
                Arguments.of(
                        Arrays.asList(1, 2, 3, 4, 5, 6), Arrays.asList(1, 2, 3)
                ),
                Arguments.of(
                        Arrays.asList(3, 11, 4, 44, 98, 6, 4, 33678, 90, 1, 46, 123), Arrays.asList(1, 3, 4)
                )
        );
    }

    static Stream<Arguments> removeMaxElement() {
        return Stream.of(
                Arguments.of(
                        Arrays.asList(2, 12, 3, 1, 123, 54, 444, 555, 1, 223), Arrays.asList(555, 444, 223, 123, 54)
                ),
                Arguments.of(
                        Arrays.asList(1, 2, 3, 4, 5, 6), Arrays.asList(6, 5, 4)
                ),
                Arguments.of(
                        Arrays.asList(3, 11, 4, 44, 98, 6, 4, 33678, 90, 1, 46, 123), Arrays.asList(33678, 123, 98)
                )
        );
    }

    abstract Heap constructMaxHeap();

    abstract Heap constructMinHeap();

    @ParameterizedTest
    @MethodSource("maxElement")
    void shouldGetMaxElement(List<Integer> list, int expectedMax, List<Integer> expectedHeapAsArray) {
        var heap = constructMaxHeap();
        for (Integer e : list) {
            heap.insert(e);
        }

        assertEquals(expectedMax, heap.peek());
        assertEquals(list.size(), heap.size());
        assertEquals(expectedHeapAsArray, heap.toList());
    }

    @ParameterizedTest
    @MethodSource("minElement")
    void shouldGetMinElement(List<Integer> list, int expectedMin, List<Integer> expectedHeapAsArray) {
        var heap = constructMinHeap();
        for (Integer e : list) {
            heap.insert(e);
        }

        assertEquals(expectedMin, heap.peek());
        assertEquals(list.size(), heap.size());
        assertEquals(expectedHeapAsArray, heap.toList());
    }

    @ParameterizedTest
    @MethodSource("removeMinElement")
    void shouldRemoveMinElement(List<Integer> list, List<Integer> expectedTopElementsToRemove) {
        var heap = constructMinHeap();
        for (Integer e : list) {
            heap.insert(e);
        }

        AtomicInteger currentSize = new AtomicInteger(heap.size());
        expectedTopElementsToRemove.forEach(e -> {
            assertEquals(e, heap.pop());
            assertEquals(currentSize.decrementAndGet(), heap.size());
        });
    }

    @ParameterizedTest
    @MethodSource("removeMaxElement")
    void shouldRemoveMaxElement(List<Integer> list, List<Integer> expectedTopElementsToRemove) {
        var heap = constructMaxHeap();
        for (Integer e : list) {
            heap.insert(e);
        }

        AtomicInteger currentSize = new AtomicInteger(heap.size());
        expectedTopElementsToRemove.forEach(e -> {
            assertEquals(e, heap.pop());
            assertEquals(currentSize.decrementAndGet(), heap.size());
        });
    }

    @Test
    void shouldNotAllowToPopOrPeekOperationsOnEmptyHeap() {
        var emptyHeap = constructMaxHeap();


        assertThrows(NoSuchElementException.class, () -> {
            emptyHeap.peek();
        });
        assertThrows(NoSuchElementException.class, () -> {
            emptyHeap.pop();
        });
    }
}
