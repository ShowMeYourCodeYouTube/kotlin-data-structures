package com.showmeyourcode.projects.datastructures.java.custom.tree.bst;

import com.showmeyourcode.projects.datastructures.java.StringToIntListConverter;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DuplicatesTreeNodeTest extends BaseBinarySearchTreeTest {

    @ParameterizedTest
    @CsvSource({
            "'11,12,12,1','11,1,12,12'",
            "'10,5,15,5,5', '10,5,15,5,5'",
            "'10,8,6,4,2,2,2', '10,8,6,4,2,2,2'"
    })
    void shouldReturnOrderAccordinglyToBFSWithDuplicates(
            @ConvertWith(StringToIntListConverter.class) List<Integer> valuesToInsert,
            @ConvertWith(StringToIntListConverter.class) List<Integer> expectedOutput) {
        var root = makeTree(valuesToInsert);

        assertIterableEquals(
                expectedOutput,
                root.bfs()
        );
    }

    @Override
    protected @NotNull DuplicatesTreeNode<Integer> makeTree(List<Integer> valuesToInsert) {
        var root = new DuplicatesTreeNode<>(valuesToInsert.get(0));
        for (int i = 1; i < valuesToInsert.size(); i++) {
            root.insert(valuesToInsert.get(i));
        }
        return root;
    }
}
