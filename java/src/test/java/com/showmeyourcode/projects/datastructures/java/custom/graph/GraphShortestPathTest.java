package com.showmeyourcode.projects.datastructures.java.custom.graph;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static com.showmeyourcode.projects.datastructures.java.custom.graph.Common.createWeightedGraphWithAdjacencyList;
import static com.showmeyourcode.projects.datastructures.java.custom.graph.Common.createWeightedGraphWithAdjacencyMatrix;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GraphShortestPathTest {

    static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(Map.entry(1, 2.0), Map.entry(2, 4.0)),
                                Arrays.asList(Map.entry(2, 1.0), Map.entry(3, 7.0)),
                                Arrays.asList(Map.entry(4, 3.0)),
                                Arrays.asList(Map.entry(4, 1.0), Map.entry(5, 5.0)),
                                Arrays.asList(Map.entry(5, 2.0)),
                                Arrays.asList()
                        ),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0.0, 2.0, 3.0, 7.0, 6.0, 8.0)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(Map.entry(1, 2.0), Map.entry(3, 1.0)),
                                Arrays.asList(Map.entry(2, 3.0), Map.entry(3, 2.0), Map.entry(4, 4.0)),
                                Arrays.asList(Map.entry(4, 5.0)),
                                Arrays.asList(Map.entry(4, 1.0)),
                                Arrays.asList()
                        ),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0.0, 2.0, 5.0, 1.0, 2.0)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(Map.entry(1, 10.0)),
                                Arrays.asList(Map.entry(2, 5.0)),
                                Arrays.asList(Map.entry(3, 2.0)),
                                Arrays.asList(),
                                Arrays.asList(Map.entry(5, 3.0)),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0.0, 10.0, 15.0, 17.0, -1.0, -1.0)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(Map.entry(1, 1.0), Map.entry(2, 4.0)),
                                Arrays.asList(Map.entry(2, 2.0), Map.entry(3, 6.0)),
                                Arrays.asList(Map.entry(3, 3.0)),
                                Arrays.asList(Map.entry(4, 7.0)),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0.0, 1.0, 3.0, 6.0, 13.0)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(Map.entry(1, 5.0), Map.entry(2, 10.0)),
                                Arrays.asList(Map.entry(2, 2.0)),
                                Arrays.asList(Map.entry(3, 1.0)),
                                Arrays.asList(Map.entry(4, 7.0)),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0.0, 5.0, 7.0, 8.0, 15.0)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(Map.entry(1, 3.0), Map.entry(2, 6.0)),
                                Arrays.asList(Map.entry(2, 1.0)),
                                Arrays.asList(Map.entry(3, 4.0)),
                                Arrays.asList(Map.entry(4, 5.0)),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0.0, 3.0, 4.0, 8.0, 13.0)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(Map.entry(1, 2.0)),
                                Arrays.asList(Map.entry(2, 3.0)),
                                Arrays.asList(Map.entry(3, 1.0)),
                                Arrays.asList(),
                                Arrays.asList(Map.entry(4, 6.0)),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0.0, 2.0, 5.0, 6.0, -1.0, -1.0)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(Map.entry(1, 4.0)),
                                Arrays.asList(Map.entry(2, 1.0)),
                                Arrays.asList(Map.entry(3, 3.0)),
                                Arrays.asList(),
                                Arrays.asList(Map.entry(5, 2.0)),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0.0, 4.0, 5.0, 8.0, -1.0, -1.0)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(Map.entry(1, 7.0)),
                                Arrays.asList(Map.entry(2, 2.0)),
                                Arrays.asList(Map.entry(3, 6.0)),
                                Arrays.asList(),
                                Arrays.asList(Map.entry(5, 3.0)),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0.0, 7.0, 9.0, 15.0, -1.0, -1.0)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(Map.entry(1, 5.0), Map.entry(3, 2.0)),
                                Arrays.asList(Map.entry(2, 2.0)),
                                Arrays.asList(Map.entry(4, 1.0)),
                                Arrays.asList(Map.entry(4, 7.0)),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0.0, 5.0, 7.0, 2.0, 8.0)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("testCases")
    void shouldFindTheShortestPathUsingDijkstraAlgorithm(
            List<List<Map.Entry<Integer, Double>>> adjacencyList,
            GraphType graphType,
            int startVertex,
            List<Double> expected
    ) {
        var weightedGraphAdjacencyList = createWeightedGraphWithAdjacencyList(adjacencyList, graphType);
        var weightedGraphAdjacencyMatrix = createWeightedGraphWithAdjacencyMatrix(adjacencyList, graphType);

        assertEquals(expected, weightedGraphAdjacencyList.dijkstra(startVertex));
        assertEquals(expected, weightedGraphAdjacencyMatrix.dijkstra(startVertex));
    }
}

