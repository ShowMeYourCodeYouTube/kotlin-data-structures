package com.showmeyourcode.projects.datastructures.java.exercise.stacks;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BalancedParenthesesCheckExerciseTest {

    @ParameterizedTest(name = "Expression {index}: \"{0}\" should be balanced: {1}")
    @CsvSource({
            "(), true",
            "(a + b) * (c - d), true",
            "(a + b) * c - d), false",
            "()()(()()), true",
            "(()()(())), true",
            "(((()))), true",
            ")(, false",
            "(, false",
            "), false",
            "((,false",
            ")),false",
            "(), true",
            "(], false",
            "[), false",
            "[), false",
            "], true",
            "[, true",
            "[], true",
            "(], false",
            "()), false",
            "()(), true",
            "[), false",
            "(], false",
            "[), false",
            "([{}]), true",
            "(({[]})), true",
            "({[)]}), false",
            "({[]}), true",
            ",true",
            "((())), true",
            "(()(()(()))),true",
            "(((())))(, false"
    })
    void testBalancedParentheses(String expression, boolean expected) {
        assertEquals(expected, BalancedParenthesesCheckExercise.isBalanced(expression != null ? expression.toCharArray() : new char[0]));
    }

}
