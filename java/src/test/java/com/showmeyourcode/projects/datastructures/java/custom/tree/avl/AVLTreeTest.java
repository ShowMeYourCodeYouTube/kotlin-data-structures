package com.showmeyourcode.projects.datastructures.java.custom.tree.avl;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AVLTreeTest {

    @Test
    void shouldTraversalTheTreeUsingInOrderAlgorithm() {
        AVLTree<Integer> avlTree = new AVLTree<>();

        avlTree.insert(10);
        avlTree.insert(5);
        avlTree.insert(15);
        avlTree.insert(3);
        avlTree.insert(7);

        List<Integer> inOrderResult = avlTree.inOrderTraversal();
        assertEquals(List.of(3, 5, 7, 10, 15), inOrderResult);

        AVLTree<Integer> complexAvlTree = new AVLTree<>();

        complexAvlTree.insert(10);
        complexAvlTree.insert(7);
        complexAvlTree.insert(6);
        complexAvlTree.insert(2);
        complexAvlTree.insert(11);
        complexAvlTree.insert(8);
        complexAvlTree.insert(1);
        complexAvlTree.insert(5);
        complexAvlTree.insert(0);
        complexAvlTree.insert(15);
        complexAvlTree.insert(13);
        complexAvlTree.insert(17);
        complexAvlTree.insert(16);
        complexAvlTree.insert(14);

        List<Integer> inOrderComplexResult = complexAvlTree.inOrderTraversal();
        assertEquals(List.of(0, 1, 2, 5, 6, 7, 8, 10, 11, 13, 14, 15, 16, 17), inOrderComplexResult);
    }

    @Test
    void shouldTraversalTheTreeUsingInOrderAlgorithmWhenDeletingAndInsertingNodes() {
        AVLTree<Integer> avlTree = new AVLTree<>();

        avlTree.insert(10);
        avlTree.insert(5);
        avlTree.insert(15);
        avlTree.insert(3);
        avlTree.insert(7);
        avlTree.insert(1);
        avlTree.insert(2);
        avlTree.insert(0);

        List<Integer> inOrderResult = avlTree.inOrderTraversal();
        assertEquals(List.of(0, 1, 2, 3, 5, 7, 10, 15), inOrderResult);

        avlTree.delete(5);
        assertEquals(List.of(0, 1, 2, 3, 7, 10, 15), avlTree.inOrderTraversal());

        avlTree.delete(5);
        assertEquals(List.of(0, 1, 2, 3, 7, 10, 15), avlTree.inOrderTraversal());

        avlTree.delete(10);
        assertEquals(List.of(0, 1, 2, 3, 7, 15), avlTree.inOrderTraversal());

        avlTree.delete(1);
        assertEquals(List.of(0, 2, 3, 7, 15), avlTree.inOrderTraversal());

        avlTree.delete(2);
        assertEquals(List.of(0, 3, 7, 15), avlTree.inOrderTraversal());

        avlTree.delete(0);
        assertEquals(List.of(3, 7, 15), avlTree.inOrderTraversal());

        avlTree.delete(3);
        assertEquals(List.of(7, 15), avlTree.inOrderTraversal());

        avlTree.delete(15);
        assertEquals(List.of(7), avlTree.inOrderTraversal());
    }

    @Test
    void shouldTraversalTheTreeUsingPreOrderAlgorithm() {
        AVLTree<Integer> avlTree = new AVLTree<>();

        avlTree.insert(10);
        avlTree.insert(5);
        avlTree.insert(15);
        avlTree.insert(3);
        avlTree.insert(7);

        List<Integer> preOrderResult = avlTree.preOrderTraversal();
        assertEquals(List.of(10, 5, 3, 7, 15), preOrderResult);
    }

    @Test
    void shouldTraversalTheTreeUsingPostOrderAlgorithm() {
        AVLTree<Integer> avlTree = new AVLTree<>();

        avlTree.insert(10);
        avlTree.insert(5);
        avlTree.insert(15);
        avlTree.insert(3);
        avlTree.insert(7);

        List<Integer> postOrderResult = avlTree.postOrderTraversal();
        assertEquals(List.of(3, 7, 5, 15, 10), postOrderResult);
    }

    @Test
    void shouldTraversalTheTreeUsingLevelOrderAlgorithm() {
        AVLTree<Integer> avlTree = new AVLTree<>();

        avlTree.insert(10);
        avlTree.insert(5);
        avlTree.insert(15);
        avlTree.insert(3);
        avlTree.insert(7);

        List<Integer> levelOrderResult = avlTree.levelOrderTraversal();
        assertEquals(List.of(10, 5, 15, 3, 7), levelOrderResult);
    }
}
