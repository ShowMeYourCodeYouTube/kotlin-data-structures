package com.showmeyourcode.projects.datastructures.java.exercise.lists;

import com.showmeyourcode.projects.datastructures.java.StringToIntListConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;

import static com.showmeyourcode.projects.datastructures.java.exercise.lists.MergeSortedListsExercise.mergeAndSortLists;
import static com.showmeyourcode.projects.datastructures.java.exercise.lists.MergeSortedListsExercise.mergeSortedLists;

class MergeSortedListsExerciseTest {

    @ParameterizedTest
    @CsvSource({
            "'1,3,5,7,9', '2,4,6,8,10', '1,2,3,4,5,6,7,8,9,10'",
            "'1,3,5', '2,4,6,8,10', '1,2,3,4,5,6,8,10'",
            "'', '2,4,6,8,10', '2,4,6,8,10'",
            "'1,3,5,7,9', '', '1,3,5,7,9'",
            "'1,2,3', '4,5,6', '1,2,3,4,5,6'",
            "'5,4,3', '2,1,6', '1,2,3,4,5,6'",
            "'3,1,4', '2,5', '1,2,3,4,5'",
            "'9,7,5', '6,8', '5,6,7,8,9'",
            "'10,20,30', '25,15,5', '5,10,15,20,25,30'",
            "'10,5,15', '20,25,0', '0,5,10,15,20,25'",
            "'3,7,2', '9,4,1', '1,2,3,4,7,9'"
    })
    void testMergeAndSortLists(
            @ConvertWith(StringToIntListConverter.class) List<Integer> list1,
            @ConvertWith(StringToIntListConverter.class) List<Integer> list2,
            @ConvertWith(StringToIntListConverter.class) List<Integer> expected) {

        List<Integer> result = mergeAndSortLists(list1, list2);
        Assertions.assertEquals(expected, result);
    }

    @ParameterizedTest
    @CsvSource({
            "'1,3,5,7,9', '2,4,6,8,10', '1,2,3,4,5,6,7,8,9,10'",
            "'1,3,5', '2,4,6,8,10', '1,2,3,4,5,6,8,10'",
            "'', '2,4,6,8,10', '2,4,6,8,10'",
            "'1,3,5,7,9', '', '1,3,5,7,9'",
            "'1,2,3', '4,5,6', '1,2,3,4,5,6'"
    })
    void testMergeSortedLists(
            @ConvertWith(StringToIntListConverter.class) List<Integer> list1,
            @ConvertWith(StringToIntListConverter.class) List<Integer> list2,
            @ConvertWith(StringToIntListConverter.class) List<Integer> expected) {

        List<Integer> result = mergeSortedLists(list1, list2);
        Assertions.assertEquals(expected, result);
    }
}
