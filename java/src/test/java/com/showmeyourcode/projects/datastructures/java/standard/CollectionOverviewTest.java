package com.showmeyourcode.projects.datastructures.java.standard;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CollectionOverviewTest {

    @Test
    void shouldRunTheMainMethodWithoutExceptions(){
        Assertions.assertDoesNotThrow(()->{
            CollectionOverview.main(new String[0]);
        });
    }
}
