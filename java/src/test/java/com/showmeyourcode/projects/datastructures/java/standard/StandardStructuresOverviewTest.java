package com.showmeyourcode.projects.datastructures.java.standard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class StandardStructuresOverviewTest {

    @Test
    void shouldRunTheMainMethodWithoutErrors() {
        assertDoesNotThrow(() -> StandardStructuresOverview.main(new String[0]));
    }
}
