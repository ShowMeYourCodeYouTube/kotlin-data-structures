package com.showmeyourcode.projects.datastructures.java.custom.tree.tst;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TernaryTreeTest {

    @Test
    public void shouldInsertNodes() {
        TernaryTree<Integer> ternaryTree = new TernaryTree<>();

        ternaryTree.insert(10);
        ternaryTree.insert(5);
        ternaryTree.insert(15);
        ternaryTree.insert(3);
        ternaryTree.insert(7);

        List<Integer> inOrderResult = ternaryTree.inOrderTraversal();
        assertEquals(List.of(3, 5, 7, 10, 15), inOrderResult);
    }

    @Test
    public void shouldDeleteNode() {
        TernaryTree<Integer> ternaryTree = new TernaryTree<>();

        ternaryTree.insert(10);
        ternaryTree.insert(5);
        ternaryTree.insert(15);
        ternaryTree.insert(3);
        ternaryTree.insert(7);
        ternaryTree.insert(1);
        ternaryTree.insert(2);
        ternaryTree.insert(0);
        ternaryTree.insert(5);
        ternaryTree.insert(5);
        ternaryTree.insert(5);

        List<Integer> inOrderResult = ternaryTree.inOrderTraversal();
        assertEquals(List.of(0, 1, 2, 3, 5, 5, 5, 5, 7, 10, 15), inOrderResult);

        ternaryTree.delete(5);
        assertEquals(List.of(0, 1, 2, 3, 5, 5, 5, 7, 10, 15), ternaryTree.inOrderTraversal());

        ternaryTree.delete(5);
        assertEquals(List.of(0, 1, 2, 3, 5, 5, 7, 10, 15), ternaryTree.inOrderTraversal());

        ternaryTree.delete(5);
        assertEquals(List.of(0, 1, 2, 3, 5, 7, 10, 15), ternaryTree.inOrderTraversal());

        ternaryTree.delete(5);
        assertEquals(List.of(0, 1, 2, 3, 7, 10, 15), ternaryTree.inOrderTraversal());

        ternaryTree.delete(5);
        assertEquals(List.of(0, 1, 2, 3, 7, 10, 15), ternaryTree.inOrderTraversal());

        ternaryTree.delete(10);
        assertEquals(List.of(0, 1, 2, 3, 7, 15), ternaryTree.inOrderTraversal());

        ternaryTree.delete(0);
        assertEquals(List.of(1, 2, 3, 7, 15), ternaryTree.inOrderTraversal());
    }

    @Test
    public void shouldFindRelevantNodes() {
        TernaryTree<Integer> ternaryTree = new TernaryTree<>();

        ternaryTree.insert(10);
        ternaryTree.insert(5);
        ternaryTree.insert(15);
        ternaryTree.insert(3);
        ternaryTree.insert(7);
        ternaryTree.insert(1);
        ternaryTree.insert(2);
        ternaryTree.insert(0);
        ternaryTree.insert(5);
        ternaryTree.insert(5);
        ternaryTree.insert(5);

        assertFalse(ternaryTree.find(12345));
        assertFalse(ternaryTree.find(45));
        assertFalse(ternaryTree.find(-5));
        assertFalse(ternaryTree.find(66));
        assertFalse(ternaryTree.find(77));

        assertTrue(ternaryTree.find(0));
        assertTrue(ternaryTree.find(1));
        assertTrue(ternaryTree.find(2));
        assertTrue(ternaryTree.find(3));
        assertTrue(ternaryTree.find(5));
        assertTrue(ternaryTree.find(7));
        assertTrue(ternaryTree.find(10));
        assertTrue(ternaryTree.find(15));
    }

    @Test
    public void shouldReturnNodesInPreOrder() {
        TernaryTree<Integer> ternaryTree = new TernaryTree<>();

        ternaryTree.insert(10);
        ternaryTree.insert(5);
        ternaryTree.insert(15);
        ternaryTree.insert(3);
        ternaryTree.insert(7);

        List<Integer> preOrderResult = ternaryTree.preOrderTraversal();
        assertEquals(List.of(10, 5, 3, 7, 15), preOrderResult);
    }

    @Test
    public void shouldReturnNodesInPostOrder() {
        TernaryTree<Integer> ternaryTree = new TernaryTree<>();

        ternaryTree.insert(10);
        ternaryTree.insert(5);
        ternaryTree.insert(15);
        ternaryTree.insert(3);
        ternaryTree.insert(7);

        List<Integer> postOrderResult = ternaryTree.postOrderTraversal();
        assertEquals(List.of(3, 7, 5, 15, 10), postOrderResult);
    }

    @Test
    public void shouldReturnNodesInLevelOrder() {
        TernaryTree<Integer> ternaryTree = new TernaryTree<>();

        ternaryTree.insert(10);
        ternaryTree.insert(5);
        ternaryTree.insert(15);
        ternaryTree.insert(3);
        ternaryTree.insert(7);

        List<Integer> levelOrderResult = ternaryTree.levelOrderTraversal();
        assertEquals(List.of(10, 5, 15, 3, 7), levelOrderResult);
    }
}