# Data structure description

- [Array](#array)
- [Matrix](#matrix)
- [Multidimensional array](#multidimensional-array)
- [Stack](#stack)
- [Queue](#queue)
- [Linked list](#linked-list)
- [Skip list](#skip-list)
- [Set](#set)
- [Map](#map)
- [Hash table](#hash-table)
- [Tree](#tree)
  - [Binary tree](#binary-tree)
  - [Binary search tree (BTS)](#binary-search-tree-bts)
  - [Trie](#trie)
  - [Ternary tree](#ternary-tree)
  - [M-array tree (Generic tree)](#m-array-tree-generic-tree)
  - [Self-Balancing Binary Search Tree](#self-balancing-binary-search-tree)
    - [AVL tree](#avl-tree)
    - [Red–black tree](#redblack-tree)
    - [B-tree](#b-tree)
    - [B+ tree (vs B-tree)](#b-tree-vs-b-tree)
    - [Splay tree](#splay-tree)
- [Heap](#heap)
- [Graph](#graph)
  - [Graph Representations](#graph-representations)
  - [Graph traversal approaches](#graph-traversal-approaches)
  - [Minimum spanning tree](#minimum-spanning-tree)
  - [The shortest path in a graph](#the-shortest-path-in-a-graph)
  - [Detecting cycles in graphs](#detecting-cycles-in-graphs)
  - [Types of a graph](#types-of-a-graph)
  - [Graph algorithms](#graph-algorithms)

---

A **data type** is the most basic and the most common classification of data. It is this through which the compiler gets to know the form or the type of information that will be used throughout the code. So basically data type is a type of information transmitted between the programmer and the compiler where the programmer informs the compiler about what type of data is to be stored and also tells how much space it requires in the memory.

In short, the data type refers to the type of value a variable has

A **data structure** is the collection of different kinds of data (data types). It is a way of organizing the items in terms of memory, and also the way of accessing each item through some defined logic. Some examples of data structures are stacks, queues, linked lists, binary tree

Reference: https://www.geeksforgeeks.org/difference-between-data-type-and-data-structure/

## Array

An array is a linear data structure that collects elements of the same data type 
and stores them in contiguous and adjacent memory locations. 

Arrays work on an index system starting from 0 to (n-1), where n is the size of the array.

It's mutable with fixed size by default, which means we can perform both read and writes operations on elements of array.

![Array](./docs/array.png)

### Reference

- https://www.simplilearn.com/tutorials/data-structure-tutorial/arrays-in-data-structure

## Matrix

A matrix is a two-dimensional array that consists of rows and columns.
It is an arrangement of elements in horizontal or vertical lines of entries.

*It is possible for arrays to have multiple dimensions but the matrix is always a two dimensional array (array of arrays).*

![Matrix](./docs/matrix1.png)

![Matrix](./docs/matrix2.png)

### Reference

- https://www.simplilearn.com/tutorials/data-structure-tutorial/arrays-in-data-structure
- https://www.quora.com/What-is-the-difference-between-a-matrix-and-a-multidimensional-array

## Multidimensional array

A multi-dimensional array is an array with more than one level or dimension.
For example, a 2D array, or two-dimensional array, is an array of arrays, meaning it is a matrix of rows and columns (think of a table).
A 3D array adds another dimension, turning it into an array of arrays of arrays.

![Array](docs/array-three-dimensional.png)

### Reference

- https://www.mathworks.com/help/matlab/math/multidimensional-arrays.html
- https://study.com/academy/lesson/multi-dimensional-arrays-in-c-programming-definition-example.html

## Queue

A queue is also a linear data structure. The objects are removed and inserted from two different ends. It follows the Last In, First Out (LIFO) principle. It means that the last inserted element gets deleted at first. 

*A stack follows a LIFO (Last In First Out) order, whereas a queue follows a FIFO (First In First Out) order for storing the elements.*

![Queue](./docs/queue.png)

---

**Queue vs Dequeue**

A **queue** is designed to have elements inserted at the end of the queue, 
and elements removed from the beginning of the queue,   
whereas **dequeue (double-ended queue)** represents a queue 
where you can insert and remove elements from both ends of the queue.

### Reference

- https://byjus.com/gate/difference-stack-and-queue-data-structures/
- https://www.scaler.com/topics/difference-between-stack-and-queue/
- https://www.geeksforgeeks.org/difference-between-queue-and-deque-queue-vs-deque/
- https://stackoverflow.com/questions/38812225/queue-vs-dequeue-in-java

## Stack

A stack is an abstract data type that holds an ordered, linear sequence of items.  
In contrast to a queue, a stack is a last in, first out (LIFO) structure. 

A real-life example is a stack of plates: you can only take a plate from the top of the stack, 
and you can only add a plate to the top of the stack. 
If you want to reach a plate that is not on the top of the stack, you need to remove all of the plates that are above that one. 

In the same way, in a stack data structure, you can only access the element on the top of the stack. 
The element that was added last will be the one to be removed first. 
Therefore, to implement a stack, you need to maintain a pointer to the top of the stack (the last element to be added).

![Stack](./docs/stack.jpg)

### Reference

- https://isaaccomputerscience.org/concepts/dsa_datastruct_stack
- https://www.programiz.com/dsa/stack

## List

The list can be defined as an abstract data type in which 
the elements are stored in an ordered manner for easier and efficient retrieval of the elements.   
List Data Structure allows repetition that means a single piece of data can occur more than once in a list.

The list can be called Dynamic size arrays, which means their size increased as we go on adding data in them, and we need not to pre-define a static size for the list.

![List](docs/arraylist-implementation-in-java.png)

### Reference

- https://www.javatpoint.com/list-data-structure
- https://www.javatpoint.com/arraylist-implementation-in-java

## Linked list

A linked list stores a collection of items in a linear order. Each element, or node, in a linked list contains a data item, as well as a reference, or link, to the next item in the list.

*Kotlin does not provide a LinkedList implementation. However, you can still use LinkedList because of the Kotlin JVM interoperability with Java. This is because LinkedList is a data collection that is part of the JVM implementation. Ref: <https://blog.logrocket.com/arraylist-vs-linkedlist-kotlin-data-structure/>*

![Linked list](./docs/linked-list.png)

![List](docs/linkedlist-single-and-double.png)

**LinkedList vs ArrayList**

- ArrayList with ArrayDeque are preferable in many more use-cases than LinkedList. If you're not sure — just start with ArrayList.
- We might choose LinkedList when operations like data addition or deletion occur more frequently than reading the data. Conversely, use ArrayList when data reading scenarios are more common than adding or removing data. This is because ArrayList is better equipped for frequent data retrievals.
- The insertion, addition, and removal operations are faster in a LinkedList because there is no resizing of an array done in the background. When a new item is added somewhere in the middle of the list, only references in surrounding elements need to change.
- LinkedList is fast for appending or deleting large elements at the ends of a list, but slow to access a specific element. ArrayList is fast for accessing a specific element but can be slow to add to either end, and especially slow to delete in the middle. When we remove an item from the end of the list, all that is done internally is decrementing the size of the list internally. But removing an element which is located somewhere at the start of the ArrayList is VERY slow, because it will have to copy all the subsequent items one step back, so it is very slow.
- !!! Due to modern computer architecture, ArrayList will be significantly more efficient for nearly any possible use-case - and therefore LinkedList should be avoided except some very unique and extreme cases.
- IMPORTANT! LinkedList uses lots of small memory objects, and therefore impacts performance across the process. Lots of small objects are bad for cache-locality.

**Singly and Doubly Linked Lists**

A doubly linked list (DLL) is similar to a singly linked list except each node also has a reference (pointer) to the previous node.

- Advantage of double linked list: Can traverse in both directions
- Advantage of single linked list: Less housework to be done on update/insert/delete, less memory usage.

### Reference

- https://www.scaler.com/topics/arraylist-vs-linkedlist/
- https://www.baeldung.com/java-arraylist-vs-linkedlist-vs-hashmap
- https://stackoverflow.com/questions/34975220/why-is-removing-an-element-from-the-start-of-an-arraylist-slower-than-that-from
- https://stackoverflow.com/questions/322715/when-to-use-linkedlist-over-arraylist-in-java
- https://medium.com/@raphaelkhan8/singly-and-doubly-linked-lists-82c57ba8d7e4
- https://stackoverflow.com/questions/10708790/microsoft-asks-singly-list-or-doubly-list-what-are-the-pros-and-cons-of-using

## Skip list

A skip list is a probabilistic data structure. The skip list is used to store a sorted list of elements or data with a linked list.
It allows the process of the elements or data to view efficiently. 
In one single step, it skips several elements of the entire list, which is why it is known as a skip list.

The skip list is an extended version of the linked list. It allows the user to search, remove, and insert the element very quickly.

![Image](docs/skiplist.png)

Advantages of the Skip list
- If you want to insert a new node in the skip list, then it will insert the node very fast because there are no rotations in the skip list.
- The skip list is simple to implement as compared to the hash table and the binary search tree.
- It is very simple to find a node in the list because it stores the nodes in sorted form.
- The skip list algorithm can be modified very easily in a more specific structure, such as indexable skip lists, trees, or priority queues.

Disadvantages of the Skip list
- It requires more memory than the balanced tree.
- Reverse searching is not allowed.
- The skip list searches the node much slower than the linked list.

---

- Good YouTube explanation: https://www.youtube.com/watch?v=Dx7Hk8-8Kdw
- Visualization: https://cmps-people.ok.ubc.ca/ylucet/DS/SkipList.html

### References

- https://www.javatpoint.com/skip-list-in-data-structure
- https://www.baeldung.com/cs/skip-lists
- https://javarevisited.blogspot.com/2021/11/how-to-implement-skip-list-in-java.html

## Set

It's an unordered collection of objects in which duplicate values cannot be stored.

### Java

- HashSet
  - HashSet class is an inherent implementation of the hash table data structure. The objects that we insert into the HashSet do not guarantee to be inserted in the same order. The objects are inserted based on their hashcode. This class also allows the insertion of NULL elements.
- EnumSet 
  - EnumSet is one of the specialized implementations of the Set interface for use with the enumeration type. It is a high-performance set implementation, much faster than HashSet. All of the elements in an enum set must come from a single enumeration type that is specified when the set is created either explicitly or implicitly. Let’s see how to create a set object using this class.
- LinkedHashSet
  - LinkedHashSet is an ordered version of HashSet that maintains a doubly-linked List across all elements. When the iteration order is needed to be maintained this class is used. When iterating through a HashSet the order is unpredictable, while a LinkedHashSet lets us iterate through the elements in the order in which they were inserted.
- TreeSet
  - TreeSet is basically an implementation of a self-balancing binary search tree like a Red-Black Tree. Therefore operations like add, remove, and search takes O(log(N)) time. The reason is that in a self-balancing tree, it is made sure that the height of the tree is always O(log(N)) for all the operations. Therefore, this is considered as one of the most efficient data structures in order to store the huge sorted data and perform operations on it. However, operations like printing N elements in the sorted order take O(N) time.
  - For HashSet, LinkedHashSet, and EnumSet, the add(), remove() and contains() operations cost constant O(1) time thanks to the internal HashMap implementation. Likewise, the TreeSet has O(log(n)) time complexity.

![Tree Set](./docs/tree-set.jpg)

### Reference

- https://www.geeksforgeeks.org/set-in-java/
- https://www.digitalocean.com/community/tutorials/java-set
- https://www.geeksforgeeks.org/navigableset-java-examples/
- https://www.geeksforgeeks.org/treeset-in-java-with-examples/
- https://www.baeldung.com/java-collections-complexity

## Map

Map data structure (also known as a dictionary, associative array, or hash map) 
is defined as a data structure that stores a collection of key-value pairs, where each key is associated with a single value.

- HashMap
  - This class uses a technique called Hashing. Hashing is a technique of converting a large String to a small String that represents the same String. A shorter value helps in indexing and faster searches.
  - HashMap works on hashing algorithm and uses hashCode() and equals() method on key for get and put operations.
- LinkedHashMap
  - LinkedHashMap is just like HashMap with the additional feature of maintaining an order of elements inserted into it. HashMap provided the advantage of quick insertion, search, and deletion but it never maintained the track and order of insertion which the LinkedHashMap provides where the elements can be accessed in their insertion order.
- TreeMap
  - The TreeMap in Java is used to implement the Map interface and NavigableMap along with the Abstract Class. The map is sorted according to the natural ordering of its keys, or by a Comparator provided at map creation time, depending on which constructor is used. This proves to be an efficient way of sorting and storing the key-value pairs. The storing order maintained by the treemap must be consistent with equals just like any other sorted map, irrespective of the explicit comparators.

![Linked Hash Map](./docs/linked-hash-map.png)

### Reference

- https://www.geeksforgeeks.org/introduction-to-map-data-structure-and-algorithm-tutorials/
- https://docs.oracle.com/javase/tutorial/collections/interfaces/map.html
- https://www.geeksforgeeks.org/map-interface-java-examples/
- https://www.baeldung.com/kotlin/maps
- https://www.designgurus.io/blog/what-is-java-map-class-and-its-uses
- https://www.digitalocean.com/community/tutorials/java-hashmap

## Hash table

A hash table stores a collection of items in an associative array that plots keys to values.   
A hash table uses a hash function to convert an index into an array of buckets that contain the desired data item.

![Hash table](./docs/hash-table.png)

---

**HashTable vs HashMap**

- Hashtable is synchronized, whereas HashMap is not. 
  - To make HashMap synchronized you can use `Collections.synchronizedMap()`
  - The naive approach to thread-safety in Hashtable ("synchronizing every method should take care of any concurrency problems!") makes it very much worse for threaded applications. You're better off externally synchronizing a HashMap (and thinking about the consequences), or using a ConcurrentMap implementation (and exploiting its extended API for concurrency).
- Hashtable does not allow null keys or values. HashMap allows one null key and any number of null values.
- One of HashMap's subclasses is LinkedHashMap, so in the event that you'd want predictable iteration order (which is insertion order by default), you could easily swap out the HashMap for a LinkedHashMap. This wouldn't be as easy if you were using Hashtable.

### Reference

- https://www.techtarget.com/searchdatamanagement/definition/data-structure
- https://stackoverflow.com/questions/40471/what-are-the-differences-between-a-hashmap-and-a-hashtable-in-java

## Tree

A tree is a collection of nodes connected by edges (branches), and it represents a hierarchical graphic form.

The tree data structure is a type of graph. A tree has a root node (top node) that will have a relationship with its child nodes. The path that connects the root node to the child nodes is called an edge. The leaf node is the node that doesn’t have any children and is not the root node.

The number of edges from the leaf node to the particular node in the longest path is known as the height of that node.  
Another keyword is the depth of a tree which means the count of nodes from a specific node to the root node.

![Tree](docs/minHeightTree.png)

![Tree](docs/tree-nomenclature.png)

---

Tree traversal involves searching a tree data structure one node at a time, 
performing functions like checking the node for data or updating the node. 

There are two common classifications for tree traversal algorithms: 
- **depth-first search (DFS)**
  - Depth-first search starts with the root node and first visits all nodes on one branch before backtracking.
- **breadth-first search (BFS)**
  - Breadth-first search starts from the root node and visits all nodes at its current depth before moving to the next depth in the tree.

![Tree](docs/tree-traversal.gif)

Types of traversal algorithms:
- there are three types of depth-first traversal (DFS):
  - **in-order**: visits the current node after visiting all nodes inside the left subtree, but before visiting any node within the right subtree,
  - **pre-order**: visits the current node before visiting any nodes inside left or right subtrees,
  - **post-order**: visits the current node after visiting all the nodes of left and right subtrees,
- the **level order** traversal is called BFS (Breadth First Search): 
  - visits nodes level-by-level and in left-to-right fashion at the same level.

![Tree](docs/tree-traversal-algorithms.png)

- https://builtin.com/software-engineering-perspectives/tree-traversal
- https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/

### Binary tree

A binary Tree is defined as a Tree data structure with at most 2 children. 
Since each element in a binary tree can have only 2 children, we typically name them the left and right child.

![Tree](docs/binary.png)

**Types of Binary Tree**

Binary Tree consists of following types based on the  number of children:
- Full Binary Tree
  - Each parent node or child node has either exactly two child node or no child node.
- Degenerate Binary Tree
  - Tree having single child either on left or right.

On the basis of completion of levels, the binary tree can be divided into following types:
- Complete Binary Tree
  - It is similar to full binary tree with few major differences 
    - All levels must be completely filled. 
    - All leaf nodes must lean towards left. 
    - The last leaf node may or may not have a right sibling.
- Perfect Binary Tree
  - Each parent node or internal node has exactly two child node and the leaf nodes should be at same level
- Balanced Binary Tree
  - The difference between the left and the right sub-tree for each node is either 0 or 1.

![Tree](docs/binary-tree-types.png)

Reference: https://python.plainenglish.io/data-structure-in-python-binary-tree-7b30795e1d34

### Binary search tree (BTS)

![Tree](docs/tree-bst-21.png)

Binary Search Tree is a node-based binary tree data structure which has the following properties:
- The left subtree of a node contains only nodes with keys lesser than the node’s key.
- The right subtree of a node contains only nodes with keys greater than the node’s key.
- The left and right subtree each must also be a binary search tree.

### Trie

Trie is a type of search tree. It's a famous data structure used to store and process data, especially strings.
The word trie comes from retrieval as a trie  can retrieve all the words with a given prefix.

Each node can have multiple children (as many as the alphabet size, e.g., 26 for English letters).

![Trie](docs/tree-trie.png)

[Reference](https://www.enjoyalgorithms.com/blog/introduction-to-trie-data-structure)

### Ternary tree

A Ternary Tree is a tree data structure in which each node has at most three child nodes, 
usually distinguished as `left`, `mid` and `right`. It's space-efficient for applications that require trie-like behavior with fewer nodes.

There is no strict constraint on how the nodes are ordered or how values are compared in a general ternary tree.
It can be used for various purposes, but it doesn’t impose any specific order or relationship between the values of nodes.

![Ternary tree](docs/tree-ternary-tree.png)

#### Ternary Search Tree (TST)

A ternary search tree (TST) is a special type of ternary tree that organizes data based on a specific ordering rule,
often used for string searching and dictionary-like applications.

Unlike trie(standard) data structure where each node contains 26 pointers for its children,
each node in a ternary search tree contains only 3 pointers:
1. The left pointer points to the node whose value is less than the value in the current node.
2. The equal pointer points to the node whose value is equal to the value in the current node.
3. The right pointer points to the node whose value is greater than the value in the current node.

### M-array tree (Generic Tree)

Generic trees are a collection of nodes where each node is a data structure that 
consists of records and a list of references to its children(duplicate references are not allowed). 
Unlike the linked list, each node stores the address of multiple nodes. 

*It's also called n-array/n-ary tree.*

![Tree](docs/tree-nary-tree.png)

Every node stores the addresses of its children and the very first node’s address will be stored in a separate pointer called root.
1. Many children at every node.
2. The number of nodes for each node is not known in advance.

### Self-Balancing Binary Search Tree

Self-Balancing Binary Search Trees are height-balanced binary search trees that 
automatically keep the height as small as possible when insertion and deletion operations are performed on the tree.
The most common examples of self-balancing binary search trees are: AVL Tree, Red Black Tree and Splay Tree.

Reference: https://www.geeksforgeeks.org/self-balancing-binary-search-trees/

#### AVL tree

AVL tree (named after inventors Adelson-Velsky and Landis) is a self-balancing Binary Search Tree (BST) where 
the difference between heights of left and right subtrees for any node cannot be more than one.

![Tree](docs/tree-avl.png)

##### Reference

- https://www.geeksforgeeks.org/types-of-trees-in-data-structures/
- https://www.geeksforgeeks.org/deletion-in-an-avl-tree/

#### Red–black tree

When it comes to searching and sorting data, one of the most fundamental data structures is the binary search tree. 
However, the performance of a binary search tree is highly dependent on its shape, and in the worst case, 
it can degenerate into a linear structure with a time complexity of O(n). 
This is where Red Black Trees come in, they are a type of balanced binary search tree 
that use a specific set of rules to ensure that the tree is always balanced. 
This balance guarantees that the time complexity for operations 
such as insertion, deletion, and searching is always O(log n), regardless of the initial shape of the tree.

![Tree](docs/tree-red-black-tree.png)

---

The Red-Black tree satisfies all the properties of binary search tree in addition to that it satisfies following additional properties:
1. Root property: The root is black.
2. External property: Every leaf (Leaf is a NULL child of a node) is black in Red-Black tree.
3. Internal property: The children of a red node are black. Hence possible parent of red node is a black node.
4. Depth property: All the leaves have the same black depth.
5. Path property: Every simple path from root to descendant leaf node contains same number of black nodes.

The result of all these above-mentioned properties is that the Red-Black tree is roughly balanced.

Rules That Every Red-Black Tree Follows:
1. Every node has a color either red or black.
2. The root of the tree is always black.
3. There are no two adjacent red nodes (A red node cannot have a red parent or red child).
  1. Red-Black tree is a binary search tree in which every node is colored with either red or black.
4. Every path from a node (including root) to any of its descendants NULL nodes has the same number of black nodes.
5. Every leaf (e.i. NULL node) must be colored BLACK.

---

Why Red-Black Trees?  

Most of the BST operations (e.g., search, max, min, insert, delete.. etc) take O(h) time where h is the height of the BST. The cost of these operations may become O(n) for a skewed Binary tree. If we make sure that the height of the tree remains O(log n) after every insertion and deletion, then we can guarantee an upper bound of O(log n) for all these operations. The height of a Red-Black tree is always O(log n) where n is the number of nodes in the tree.

The AVL trees are more balanced compared to Red-Black Trees, but they may cause more rotations during insertion and deletion. So if your application involves frequent insertions and deletions, then Red-Black trees should be preferred. And if the insertions and deletions are less frequent and search is a more frequent operation, then AVL tree should be preferred over the Red-Black Tree.

---

![Tree](docs/tree-red-black-tree.gif)

[Visualization reference](https://github.com/maelvls/rbtree-gif)

**An outstanding program to visualize adding and removing nodes from the tree**: https://www.cs.usfca.edu/~galles/visualization/RedBlack.html  
You can use it to better understand how it works and verify your implementations.

*More examples of adding values to a tree:* https://faculty.cs.niu.edu/~freedman/340/340notes/340redblk.htm

##### Reference

- https://www.geeksforgeeks.org/types-of-trees-in-data-structures/
- https://www.geeksforgeeks.org/introduction-to-red-black-tree/

#### B-tree

B-tree is a self-balancing M-way tree in which each node can contain more than one key and can have more than two children.
It's perfectly balanced (every leaf node is at the same depth) and every node, except perhaps the root, is at least half-full, i.e. contains M/2 or more values (of course, it cannot contain more than M-1 values).

![Tree](docs/tree-b-tree-indexing.jpg)

The following graphic depicts a B tree of order 4:

![Tree](docs/word-image232.png)

The need for B-tree arose with the rise in the need for lesser time in accessing physical storage media like a hard disk. 
The secondary storage devices are slower with a larger capacity. 
There was a need for such types of data structures that minimize the disk access.

Other data structures such as a binary search tree, avl tree, red-black tree, etc can store only one key in one node. 
If you have to store a large number of keys, then the height of such trees becomes very large, and the access time increases.

All of the features of a M-way tree are present in a B tree of order m. Additionally, it has the following features:
- In a B-Tree, each node has a maximum of m children.
- Each internal node can contain at most m - 1 keys.
- Except for the root and leaf nodes, each node in a B-Tree has at least m/2 children.
- There must be at least two root nodes.
- The level of all the leaf nodes should be the same.

**B-trees can be used for:**
- databases indexing (improve searching)
  - B-Tree indexes (B stands for balanced), are the most common index type in a relational database and are used for a variety of common query performance enhancing tasks.
  - https://docs.aws.amazon.com/dms/latest/oracle-to-aurora-postgresql-migration-playbook/chap-oracle-aurora-pg.tables.btree.html
- multilevel indexing
- storage systems (file systems storage / blocks of data (secondary storage media))
  - reduces the number of reads made on the disk

**Searching Complexity on B Tree:**
- Worst case Time complexity: Θ(log n)
- Average case Time complexity: Θ(log n)
- Best case Time complexity: Θ(log n)
- Average case Space complexity: Θ(n)
- Worst case Space complexity: Θ(n)

---

![Tree](docs/tree-b-tree-insertion.png)

Reference: https://www.programiz.com/dsa/insertion-into-a-b-tree

![Tree](docs/tree-b-tree_insertion.jpg)

Reference: https://iq.opengenus.org/b-tree-searching-insertion/

![Tree](docs/tree-b-tree-delete.png)

[Visualization reference](https://cuuduongthancong.com/~galles/visualization/BTree.html)

**An outstanding program to visualize adding and removing nodes from the tree**: https://cuuduongthancong.com/~galles/visualization/BTree.html

##### Reference

- https://builtin.com/data-science/b-tree-index
- https://www.programiz.com/dsa/b-tree
- https://www.guru99.com/b-tree-example.html
- https://www.geeksforgeeks.org/delete-operation-in-b-tree/
- https://www.programiz.com/dsa/insertion-into-a-b-tree
- https://byjus.com/gate/b-tree-notes/
- https://webdocs.cs.ualberta.ca/~holte/T26/b-trees.html

#### B+ tree (vs B-tree)

B+ tree eliminates the drawback B-tree used for indexing by storing data pointers only at the leaf nodes of the tree. 
Thus, the structure of leaf nodes of a B+ tree is quite different from the structure of internal nodes of the B tree. 
It may be noted here that, since data pointers are present only at the leaf nodes, the leaf nodes must necessarily store all the key values along with their corresponding data pointers to the disk file block, to access them. 
Moreover, the leaf nodes are linked to providing ordered access to the records. 
The leaf nodes, therefore form the first level of the index, with the internal nodes forming the other levels of a multilevel index. 
Some of the key values of the leaf nodes also appear in the internal nodes, to simply act as a medium to control the searching of a record.

![Tree](docs/tree-b-tree-plus.jpg)

![Tree](docs/tree-b-tree-plus.png)

##### Reference

- https://www.geeksforgeeks.org/difference-between-b-tree-and-b-tree/
- https://stackoverflow.com/questions/870218/what-are-the-differences-between-b-trees-and-b-trees

#### Splay tree

Splay Tree in data structures is a type of binary search tree that uses a splaying operation on the tree so the most frequently used elements can come closer to the root. Splay tree is the fastest type of binary search tree, which is used in a variety of practical applications such as GCC compilers.

- The basic idea behind splay trees is to bring the most recently accessed or inserted element to the root of the tree by performing a sequence of tree rotations, called splaying. Splaying is a process of restructuring the tree by making the most recently accessed or inserted element the new root and gradually moving the remaining nodes closer to the root.
- Splay trees are highly efficient in practice due to their self-adjusting nature, which reduces the overall access time for frequently accessed elements. This makes them a good choice for applications that require fast and dynamic data structures, such as caching systems, data compression, and network routing algorithms.
- However, the main disadvantage of splay trees is that they do not guarantee a balanced tree structure, which may lead to performance degradation in worst-case scenarios. Also, splay trees are not suitable for applications that require guaranteed worst-case performance, such as real-time systems or safety-critical systems.

For searching, we will perform the binary search method. Let’s say we want to search for element 9. As 9 is less than 11, we will come to the left of the root node. After performing a search operation, we need to do one thing called splaying. This means, that after splaying, the element on which we are operating should come to the root. Elements would come to the root after performing some rearrangements of elements or rotations in the tree.

---

Operations in a splay tree:
- Insertion: To insert a new element into the tree, start by performing a regular binary search tree insertion. Then, apply rotations to bring the newly inserted element to the root of the tree.
- Deletion: To delete an element from the tree, first locate it using a binary search tree search. Then, if the element has no children, simply remove it. If it has one child, promote that child to its position in the tree. If it has two children, find the successor of the element (the smallest element in its right subtree), swap its key with the element to be deleted, and delete the successor instead.
- Search: To search for an element in the tree, start by performing a binary search tree search. If the element is found, apply rotations to bring it to the root of the tree. If it is not found, apply rotations to the last node visited in the search, which becomes the new root.
- Rotation: The rotations used in a splay tree are either a Zig or a Zig-Zig rotation. A Zig rotation is used to bring a node to the root, while a Zig-Zig rotation is used to balance the tree after multiple accesses to elements in the same subtree.


Rotations in Splay Tree:
- Zig Rotation
- Zag Rotation
- Zig – Zig Rotation
- Zag – Zag Rotation
- Zig – Zag Rotation
- Zag – Zig Rotation

1) Zig Rotation:
   The Zig Rotation in splay trees operates in a manner similar to the single right rotation in AVL Tree rotations. This rotation results in nodes moving one position to the right from their current location. For example, consider the following scenario:

![Rotation](docs/zig-rotation.png)

2) Zag Rotation:
   The Zag Rotation in splay trees operates in a similar fashion to the single left rotation in AVL Tree rotations. During this rotation, nodes shift one position to the left from their current location. For instance, consider the following illustration:

![Rotation](docs/zag-rotation.png)

3) Zig-Zig Rotation:
   The Zig-Zig Rotation in splay trees is a double zig rotation. This rotation results in nodes shifting two positions to the right from their current location. Take a look at the following example for a better understanding:

![Rotation](docs/zig-zig-rotation.png)

4) Zag-Zag Rotation:
   In splay trees, the Zag-Zag Rotation is a double zag rotation. This rotation causes nodes to move two positions to the left from their present position. For example:

![Rotation](docs/zag-zag-rotation.png)

5) Zig-Zag Rotation:
   The Zig-Zag Rotation in splay trees is a combination of a zig rotation followed by a zag rotation. As a result of this rotation, nodes shift one position to the right and then one position to the left from their current location. The following illustration provides a visual representation of this concept:

![Rotation](docs/zig-zag-rotation2.png)

6) Zag-Zig Rotation:
   The Zag-Zig Rotation in splay trees is a series of zag rotations followed by a zig rotation. This results in nodes moving one position to the left, followed by a shift one position to the right from their current location. The following illustration offers a visual representation of this concept:

![Rotation](docs/zag-zig-rotation.png)

Reference: https://www.geeksforgeeks.org/introduction-to-splay-tree-data-structure/

---

*What is the difference between AVL trees and splay trees?* Both splay trees and AVL trees are binary search trees with excellent performance guarantees, but they differ in how they achieve those guarantee that performance. In an AVL tree, the shape of the tree is constrained at all times such that the tree shape is balanced, meaning that the height of the tree never exceeds O(log n). This shape is maintained on insertions and deletions, and does not change during lookups. Splay trees, on the other hand, maintain efficient by reshaping the tree in response to lookups on it. That way, frequently-accessed elements move up toward the top of the tree and have better lookup times. The shape of splay trees is not constrained, and varies based on what lookups are performed.

*On what basis do we select these trees?* There is no hard-and-fast rule about this. However, one key difference between the structures is that AVL trees guarantee fast lookup (O(log n)) on each operation, while splay trees can only guarantee that any sequence of n operations takes at most O(n log n) time. This means that if you need real-time lookups, the AVL tree is likely to be better. However, splay trees tend to be much faster on average, so if you want to minimize the total runtime of tree lookups, the splay tree is likely to be better. Notice that the worst-case time complexity of operations like search, delete, and insert on a binary search tree is O(n).


**An outstanding program to visualize adding and removing nodes from the tree**: https://www.cs.usfca.edu/~galles/visualization/SplayTree.html   
You can use it to better understand how it works and verify your implementations.

##### Reference

- https://www.geeksforgeeks.org/introduction-to-splay-tree-data-structure/
- https://stackoverflow.com/questions/7467079/difference-between-avl-trees-and-splay-trees
- https://www.scaler.com/topics/data-structures/splay-tree/

### Reference

- https://javachallengers.com/tree-data-structure-with-java/
- https://towardsdatascience.com/5-types-of-binary-tree-with-cool-illustrations-9b335c430254
- https://www.geeksforgeeks.org/roots-tree-gives-minimum-height/
- https://www.simplilearn.com/tutorials/data-structure-tutorial/trees-in-data-structure
- https://www.lavivienpost.com/data-structures-and-java-collections/
- https://zhang-xiao-mu.blog/2019/07/20/tree-traversal-in-order-level-order/
- https://www.geeksforgeeks.org/types-of-trees-in-data-structures/
- https://www.geeksforgeeks.org/introduction-to-tree-data-structure-and-algorithm-tutorials/

## Heap

![Heap](docs/heap.png)

A Heap is a special tree-based data structure in which the tree is a complete binary tree.
Comparing to a binary tree it has an extra condition: that the value in each node of the tree is smaller than or equal to the value in the children of that node (if it’s a min-heap; you could also have a heap where all values are larger or equal to the values in the children).

> Min-heap and max-heap are both priority queues, it depends on how their order of priority is defined. The order of items is based on value. Ref: https://itnext.io/heap-priority-queue-identify-pattern-aaedda7b3f6b

A binary tree is said to be a complete binary tree if all its levels, except possibly the last level, have the maximum number of possible nodes, and all the nodes in the last level appear as far left as possible.
- In a complete binary tree, a node in the last level can have only one child.
- In a complete binary tree, the node should be filled from the left to right.
- Complete binary trees are mainly used in heap-based data structures.
- A complete binary tree is also called almost complete binary tree.
- A complete binary tree must have the entire leaves node in the exact same depth.

![img](docs/tree-perfect-complete-binary-tree.png)

Ref: https://www.geeksforgeeks.org/difference-between-full-and-complete-binary-tree/

Operations of Heap Data Structure:
- Heapify: a process of creating a heap from an array.
- Insertion: process to insert an element in existing heap time complexity O(log N).
- Deletion: deleting the top element of the heap or the highest priority element, and then organizing the heap and returning the element with time complexity O(log N).
- Peek: to check or find the first (or can say the top) element of the heap.

**Types of Heap Data Structure**

Heaps can be of two types:
- Max-Heap: In a Max-Heap the key present at the root node must be greatest among the keys present at all of it’s children. The same property must be recursively true for all sub-trees in that Binary Tree.
- Min-Heap: In a Min-Heap the key present at the root node must be minimum among the keys present at all of it’s children. The same property must be recursively true for all sub-trees in that Binary Tree.

![Heap](docs/heap-min-max-heap1.png)

### Reference

- https://www.geeksforgeeks.org/heap-data-structure/
- https://www.softwaretestinghelp.com/heap-data-structure-in-java/
- https://en.wikipedia.org/wiki/Heap_%28data_structure%29

## Graph

A Graph is a non-linear data structure consisting of vertices and edges. The vertices are sometimes also referred to as nodes and the edges are lines or arcs that connect any two nodes in the graph. More formally a Graph is composed of a set of vertices(V) and a set of edges(E). The graph is denoted by G(E, V).

![img](docs/graph-undirected.png)

Components of a Graph:
- Vertices: Vertices are the fundamental units of the graph. Sometimes, vertices are also known as vertex or nodes. Every node/vertex can be labeled or unlabelled.
- Edges: Edges are drawn or used to connect two nodes of the graph. It can be ordered pair of nodes in a directed graph. Edges can connect any two nodes in any possible way. There are no rules. Sometimes, edges are also known as arcs. Every edge can be labeled/unlabelled.

In graph theory, the degree (or valency) of a vertex of a graph is the number of edges that are incident to the vertex; in a multigraph, a loop contributes 2 to a vertex's degree, for the two ends of the edge. Ref: https://en.wikipedia.org/wiki/Degree_(graph_theory)

![img](docs/graph-example1.png)


Graphs are used to solve many real-life problems. Graphs are used to represent networks. The networks may include paths in a city or telephone network or circuit network. Graphs are also used in social networks like linkedIn, Facebook. For example, in Facebook, each person is represented with a vertex(or node). Each node is a structure and contains information like person id, name, gender, locale etc.

![Graph](docs/graph-example2.png)

Types of graphs:
- undirected/directed graph
- unweighted/weighted graph

![Graph](docs/graph-types.png)

![Graph](docs/graph-properties-graph-algorithms-2.png)  

Reference: https://neo4j.com/blog/graph-algorithms-neo4j-graph-algorithm-concepts/

### Graph Representations

- Adjacency Matrix
  - An adjacency matrix is a square matrix with dimensions equivalent to the number of vertices in the graph. 
  - The elements of the matrix typically have values 0 or 1. 
  - A value of 1 indicates adjacency between the vertices in the row and column and a value of 0 otherwise.
  - This representation is fairly easier to implement and efficient to query as well. However, it’s less efficient with respect to space occupied.

![Graph](docs/graph-adjacency-matrix.jpg)

- Adjacency List
  - An adjacency list is nothing but an array of lists. The size of the array is equivalent to the number of vertices in the graph. 
  - The list at a specific index of the array represents the adjacent vertices of the vertex represented by that array index.
  - This representation is comparatively difficult to create and less efficient to query. However, it offers better space efficiency.

![Graph](docs/graph-adjacency-list.jpg)

---

What is better, adjacency lists or adjacency matrices for graph problems?

Adjacency Matrix
- Uses O(n^2) memory
- It is fast to lookup and check for presence or absence of a specific edge between any two nodes O(1)
- It is slow to iterate over all edges
- It is slow to add/delete a node; a complex operation O(n^2)
- It is fast to add a new edge O(1)

Adjacency List
- Memory usage depends more on the number of edges (and less on the number of nodes), which might save a lot of memory if the adjacency matrix is sparse
- Finding the presence or absence of specific edge between any two nodes is slightly slower than with the matrix O(k); where k is the number of neighbors nodes
- It is fast to iterate over all edges because you can access any node neighbors directly
- It is fast to add/delete a node; easier than the matrix representation
- It fast to add a new edge O(1)


References: 
- https://www.geeksforgeeks.org/comparison-between-adjacency-list-and-adjacency-matrix-representation-of-graph/
- https://stackoverflow.com/questions/2218322/what-is-better-adjacency-lists-or-adjacency-matrices-for-graph-problems-in-c

### Graph traversal approaches

- **BFS, Breadth-First Search or Breadth First Traversal**, is a vertex-based technique for finding the shortest path in the graph. It uses a Queue data structure that follows first in first out. In BFS, one vertex is selected at a time when it is visited and marked then its adjacent are visited and stored in the queue. It is slower than DFS. BFS do not use weights

```
function bfs( graph, node )
  queue = new Queue()
  queue.enqueue( node )

  while (queue.notEmpty())
    currentNode = queue.dequeue()

    if (!currentNode.visited)
      currentNode.visited = true

      while ( child = graph.nextUnvisitedChildOf( node ) )
        queue.enqueue( child )
```

- **DFS, Depth First Search or Depth First Traversal**, is an edge-based technique. It uses the Stack data structure and performs two stages, first visited vertices are pushed into the stack, and second if there are no vertices then visited vertices are popped. The algorithm is searching from right to left. DFS do not use weights

```text
function dfs( graph, node )
  stack = new Stack()
  search(node)

  function search( node )
    if ( !node )
      return

    if ( !node.visited )
      stack.push( node )
      node.visited = true

    if ( graph.nodeHasUnvisitedChildren( node ) )
      search( graph.nextUnvisitedChildOf( node ) )
    else
      search( stack.pop() )
```

![Graph](docs/graph-bfs-dfs.png)

Good visualization:
- https://www.cs.usfca.edu/~galles/visualization/DFS.html
- https://www.cs.usfca.edu/~galles/visualization/BFS.html

References: 
- https://www.geeksforgeeks.org/difference-between-bfs-and-dfs/
- https://medium.com/@jwbtmf/dfs-vs-bfs-algorithms-for-graph-database-5948f0fd2057
- https://medium.com/@crusso_22624/bfs-vs-dfs-graph-traversals-comparing-times-80729f100bf
- https://workshape.github.io/visual-graph-algorithms/
  - pseudocode
  - visualization

### Minimum spanning tree

***A minimum spanning tree (MST)** or minimum weight spanning tree is a subset of the edges of a connected, edge-weighted undirected graph that connects all the vertices together*, without any cycles and with the minimum possible total edge weight. [Wikipedia reference](https://en.wikipedia.org/wiki/Minimum_spanning_tree)

![Graph](docs/tree-minimum-spanning-tree.png)

- Prim’s Minimum Spanning Tree (MST)
  - It falls under a class of algorithms called [greedy algorithms](https://www.programiz.com/dsa/greedy-algorithm) that find the local optimum in the hopes of finding a global optimum. 
    - We start from one vertex and keep adding edges with the lowest weight until we reach our goal. 
    - The steps for implementing Prim's algorithm are as follows:
      - Initialize the minimum spanning tree with a vertex chosen at random. 
      - Find all the edges that connect the tree to new vertices, find the minimum and add it to the tree 
      - Keep repeating step 2 until we get a minimum spanning tree
  - https://www.programiz.com/dsa/prim-algorithm

```
PrimMST(Graph G):
    Initialize an empty list MST to store the edges of the Minimum Spanning Tree
    Create a priority queue PQ to store edges, sorted by their weights (use a min-heap)
    Initialize a boolean array visited[] to keep track of visited vertices (initially all false)
    
    Choose an arbitrary starting vertex (e.g., startVertex)
    Set visited[startVertex] = true
    For each edge (startVertex, v) in the adjacency list of startVertex:
        Insert (startVertex, v, weight) into the priority queue PQ
    
    While PQ is not empty:
        Extract the edge (u, v, weight) with the smallest weight from PQ
        
        If v is not visited:
            Add edge (u, v) to the MST
            Set visited[v] = true
            
            For each edge (v, w, weight) in the adjacency list of v:
                If w is not visited:
                    Insert (v, w, weight) into PQ
    
    Return the MST (a list of edges)
```

- Kruskal’s Minimum Spanning Tree Algorithm
  - It falls under a class of algorithms called [greedy algorithms](https://www.programiz.com/dsa/greedy-algorithm) that find the local optimum in the hopes of finding a global optimum. 
  - We start from the edges with the lowest weight and keep adding edges until we reach our goal. 
  - The steps for implementing Kruskal's algorithm are as follows:
    - Sort all the edges from low weight to high 
    - Take the edge with the lowest weight and add it to the spanning tree. If adding the edge created a cycle, then reject this edge. 
    - Keep adding edges until we reach all vertices.
  - https://www.programiz.com/dsa/kruskal-algorithm

```
KruskalMST(Graph G):
    Initialize an empty list MST to store the edges of the Minimum Spanning Tree
    Sort all edges of the graph by their weights in ascending order

    Create a disjoint-set (union-find) data structure to keep track of connected components
    For each vertex in the graph:
        Create a disjoint-set for the vertex
    
    For each edge (u, v, weight) in the sorted list of edges:
        If u and v belong to different components (check with union-find):
            Add the edge (u, v, weight) to the MST
            Union the sets containing u and v (merge their sets in the union-find structure)
    
    Return the MST (a list of edges)
```

![Graph](docs/graph-prim-mst.png)

![Graph](docs/graph-kruskal-mst.png)

Reference: https://stackoverflow.com/questions/1195872/when-should-i-use-kruskal-as-opposed-to-prim-and-vice-versa

---

Prim’s algorithm is typically preferred for dense graphs, leveraging its efficient priority queue-based approach, while Kruskal’s algorithm excels in handling sparse graphs with its edge-sorting and union-find techniques. [Reference](https://www.geeksforgeeks.org/difference-between-prims-and-kruskals-algorithm-for-mst/)

Kruskal time complexity worst case is O(E log E),this because we need to sort the edges. Prim time complexity worst case is O(E log V) with priority queue or even better, O(E+V log V) with Fibonacci Heap. We should use Kruskal when the graph is sparse, i.e.small number of edges,like E=O(V),when the edges are already sorted or if we can sort them in linear time. We should use Prim when the graph is dense, i.e number of edges is high ,like E=O(V²). [Reference](https://stackoverflow.com/questions/1195872/when-should-i-use-kruskal-as-opposed-to-prim-and-vice-versa)

### The shortest path in a graph

#### Dijkstra's Algorithm

Dijkstra's algorithm is an algorithm for finding the shortest paths between nodes in a weighted graph,
which may represent, for example, road networks. [Reference](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm)

Pseudocode:
```
Dijkstra(Graph, source):
    Initialize distances for each node:
        for each node in Graph:
            distance[node] = ∞
        distance[source] = 0
    
    Create a priority queue (min-heap) to hold nodes and their distances:
        priority_queue = []
        priority_queue.push(source, distance[source])
    
    While priority_queue is not empty:
        current_node = priority_queue.pop_min()
        
        If current_node is already visited:
            continue
        
        For each neighbor of current_node:
            new_distance = distance[current_node] + weight(current_node, neighbor)
            
            If new_distance < distance[neighbor]:
                distance[neighbor] = new_distance
                priority_queue.push(neighbor, distance[neighbor])
        
        Mark current_node as visited
    
    Return distance array which holds the shortest distance from source to each node
```

*The `prev` array contains pointers to previous-hop nodes on the shortest path from source to the given vertex (equivalently, it is the next-hop on the path from the given vertex to the source)

Reference: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

>*Does minimum spanning tree of a graph give the shortest distance between any two specified nodes?*
No, the minimum spanning tree (MST) of a graph does not necessarily give the shortest distance between any two specified nodes in the graph.
The minimum spanning tree of a graph is a tree that connects all the vertices of the graph with the minimum total edge weight possible, without forming any cycles. While the minimum spanning tree ensures that all vertices are connected with the minimum total edge weight, it does not guarantee the shortest path between any two specific nodes in the graph.
If you want to find the shortest distance between two specific nodes in a graph, you should use algorithms like Dijkstra's algorithm or the Bellman-Ford algorithm. These algorithms are designed specifically to find the shortest path between two nodes in a graph by considering the weights of the edges along the path.
Reference: https://www.quora.com/Does-minimum-spanning-tree-of-a-graph-give-shortest-distance-between-any-two-specified-nodes


---

Consider the following graph:

```text
       2       3
    (A)---(B)---(C)
     |    / \    |
   1 |   2   4   | 5
     | /      \  |
    (D)-------(E)
           1
```

Let's apply Dijkstra's algorithm starting from node `A`:

1. Initialization:
   - dist[A] = 0
   - dist[B] = ∞
   - dist[C] = ∞
   - dist[D] = ∞
   - dist[E] = ∞
   - Priority queue: [A (0)]
2. Processing Nodes:
   - Process A: Update dist[D] = 1, dist[B] = 2
   - Priority queue: [D (1), B (2)]
   - Process D: Update dist[E] = 2
   - Priority queue: [B (2), E (2)]
   - Process B: Update dist[C] = 5
   - Priority queue: [E (2), C (5)]
   - Process E: No updates needed
   - Priority queue: [C (5)]
   - Process C: No updates needed
   - Priority queue: []
3. Termination:
   - The priority queue is empty, and the shortest distances are:
     - dist[A] = 0
     - dist[B] = 2
     - dist[C] = 5
     - dist[D] = 1
     - dist[E] = 2

---

```text
       5   
    (A)---(B)
     |    / 
   1 |   6 
     | / 
    (C)-------(D)
           1
```

If you were to apply Dijkstra’s algorithm to find the shortest path from A to all other nodes, here’s how it would work:

- Initialization:
Set the distance to A as 0 (dist(A) = 0), and set the distances to all other nodes as infinity (dist(B) = ∞, dist(C) = ∞, dist(D) = ∞).
The unvisited nodes are {A, B, C, D}.
- Step 1: Start from A
  - Visit A, and update the distances to its neighbors:
  - A → B: dist(B) = min(∞, dist(A) + 5) = 5
  - A → C: dist(C) = min(∞, dist(A) + 1) = 1
  - Now the distances are:
    - dist(A) = 0, dist(B) = 5, dist(C) = 1, dist(D) = ∞
  - Move to the unvisited node with the smallest tentative distance, which is C (distance 1).
- Step 2: Visit C
  - From C, update the distances to its neighbors:
    - C → B: dist(B) = min(5, dist(C) + 6) = 5 (no update, since 5 is already the smallest distance to B)
    - C → D: dist(D) = min(∞, dist(C) + 1) = 2
  - Now the distances are:
    - dist(A) = 0, dist(B) = 5, dist(C) = 1, dist(D) = 2 
  - Move to the unvisited node with the smallest tentative distance, which is D (distance 2).
- Step 3: Visit D
  - From D, update the distance to its neighbor B:
    - D → B: dist(B) = min(5, dist(D) + 1) = 5 (no update, since 5 is already the smallest distance to B)
  - Now the distances are:
    - dist(A) = 0, dist(B) = 5, dist(C) = 1, dist(D) = 2 
  - Move to the unvisited node with the smallest tentative distance, which is B (distance 5).
- Step 4: Visit B
  - From B, there’s no further update needed for its neighbors (as the distances are already the shortest).
- The final distances are:
  - dist(A) = 0, dist(B) = 5, dist(C) = 1, dist(D) = 2

Final Shortest Distances:
- Shortest path from A to A: 0 (trivial)
- Shortest path from A to B: 5 (A → B)
- Shortest path from A to C: 1 (A → C)
- Shortest path from A to D: 2 (A → C → D)

#### Bellman–Ford Algorithm

It is similar to Dijkstra's algorithm but it can work with graphs in which edges can have negative weights.

```text
function bellmanFord(G, S)
  for each vertex V in G
    distance[V] <- infinite
      previous[V] <- NULL
  distance[S] <- 0

  for each vertex V in G				
    for each edge (U,V) in G
      tempDistance <- distance[U] + edge_weight(U, V)
      if tempDistance < distance[V]
        distance[V] <- tempDistance
        previous[V] <- U

  for each edge (U,V) in G
    If distance[U] + edge_weight(U, V) < distance[V}
      Error: Negative Cycle Exists

  return distance[], previous[]
```

https://www.programiz.com/dsa/bellman-ford-algorithm

#### Floyd Warshall Algorithm

Floyd-Warshall Algorithm is an algorithm for finding the shortest path between all the pairs of vertices in a weighted graph. This algorithm works for both the directed and undirected weighted graphs. But, it does not work for the graphs with negative cycles (where the sum of the edges in a cycle is negative).

```text
n = no of vertices
A = matrix of dimension n*n
for k = 1 to n
    for i = 1 to n
        for j = 1 to n
            Ak[i, j] = min (Ak-1[i, j], Ak-1[i, k] + Ak-1[k, j])
return A
```

https://www.programiz.com/dsa/floyd-warshall-algorithm

### Detecting cycles in graphs

Both Breadth-First Search (BFS) and Depth-First Search (DFS) can be used to detect cycles in a graph.

For problems like topological sorting or cycle detection, DFS is more appropriate. Space Complexity: BFS has higher space complexity than DFS, as it stores all the vertices of a level in the queue. [Reference](https://codedamn.com/news/algorithms/dgraph-algorithms-bfs-vs-dfs)

DFS:
```text
function hasCycleDirected(graph):
    visited = set()
    recStack = set()
    
    for each vertex v in graph:
        if v is not in visited:
            if dfsCycleDirected(v, visited, recStack):
                return true
    return false

function dfsCycleDirected(vertex, visited, recStack):
    visited.add(vertex)
    recStack.add(vertex)
    
    for each neighbor in graph.neighbors(vertex):
        if neighbor is not in visited:
            if dfsCycleDirected(neighbor, visited, recStack):
                return true
        elif neighbor in recStack:
            return true
            
    recStack.remove(vertex)
    return false
```


It is true that the recursion stack alone is sufficient; however, keeping the visited array around is a good performance optimization.

Given that the graph is directed, consider the graph with the edges A->B, A->C, B->D, C->D. In other words, there are 2 ways to reach node D (you can go A->B->D or A->C->D). There are additional nodes and additional edges that branch off from node D (e.g. nodes E, F, G, H, etc).

So the algorithm traverses A->B->D, then continues traversing all the other nodes (E, F, G, H, etc), but sees that there is not a cycle. The algorithm eventually returns to the top-most call (from node A), and then tries A->C->D.

At this point, a naive algorithm with only a recursion stack will then re-traverse starting from node D. But a good algorithm using the visited array will know that there are no cycles from node D, since we've already inspected that node before.

[Reference](https://www.reddit.com/r/learnprogramming/comments/15wqetj/how_to_find_cycles_in_a_directed_graph_using_dfs/)

---

To detect a cycle in a directed graph using Breadth-First Search (BFS), 
you can use Kahn's algorithm for topological sorting. 
This algorithm leverages the concept of in-degrees (the number of edges pointing to a vertex) to determine if there is a cycle.

BFS:
```text
FUNCTION hasCycleBFS(graph):
    vertices = graph.numberOfVertices()
    inDegrees = ARRAY of size vertices initialized to 0
    queue = EMPTY QUEUE
    processedVertices = 0

    // Step 1: Calculate in-degrees of all vertices
    FOR vertex FROM 0 TO vertices - 1:
        FOR each neighbor IN graph.getNeighbors(vertex):
            inDegrees[neighbor]++

    // Step 2: Add all vertices with in-degree 0 to the queue
    FOR vertex FROM 0 TO vertices - 1:
        IF inDegrees[vertex] == 0:
            queue.enqueue(vertex)

    // Step 3: Process vertices in BFS manner
    WHILE queue is NOT EMPTY:
        current = queue.dequeue()
        processedVertices++

        // Reduce the in-degree of all neighbors
        FOR each neighbor IN graph.getNeighbors(current):
            inDegrees[neighbor]--
            IF inDegrees[neighbor] == 0:
                queue.enqueue(neighbor)

    // Step 4: Check if all vertices were processed
    IF processedVertices == vertices:
        RETURN False // No cycle
    ELSE:
        RETURN True  // Cycle detected
```

- Time Complexity: O(V + E):
  - Calculating in-degrees takes O(E). 
  - BFS traversal takes O(V + E).
- Space Complexity: O(V):

Space for the in-degree array and the queue.

### Types of a graph

- Finite Graph
- Infinite Graph
- Trivial Graph
- Simple Graph
- Multi Graph
- Null Graph
- Complete Graph
- Pseudo Graph
- Regular Graph
- Weighted Graph
- Directed Graph
- Undirected Graph
- Connected Graph
- Disconnected Graph
- Cyclic Graph
- Acyclic Graph

References:
- https://www.javatpoint.com/types-of-graph-in-data-structure
- https://www.simplilearn.com/tutorials/data-structure-tutorial/graphs-in-data-structure

### Graph algorithms

- cycles in a graph
  - BFS
  - DFS
- the shortest path in a graph
  - Dijkstra's Algorithm
  - Bellman–Ford Algorithm
  - Floyd Warshall Algorithm
- minimum spanning tree
  - Prim’s Minimum Spanning Tree (MST)
  - Kruskal’s Minimum Spanning Tree Algorithm
- strongly connected
- topological sorting
- graph colouring
- maximum flow
- matching
- more algorithms can be found [here](https://www.geeksforgeeks.org/graph-data-structure-and-algorithms/)

### References

- https://www.baeldung.com/java-graphs
- https://www.geeksforgeeks.org/graph-data-structure-and-algorithms/
- https://medium.com/tebs-lab/types-of-graphs-7f3891303ea8
- https://www.softwaretestinghelp.com/java-graph-tutorial/
